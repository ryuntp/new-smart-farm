
import axios from 'axios';


let headers = {};


const axiosInstance = axios.create({
    baseURL: 'http://acethainodev2-env.eba-a6yxvpca.us-east-2.elasticbeanstalk.com/',
    headers,
});


axiosInstance.interceptors.request.use(

    async (config) => {
        const token = await localStorage.getItem("token");
        if (token) {
            config.headers = {
                "authorization": token
            };
            // config.headers.ContentType = `multipart/form-data`;
            //${token}`
        }

        return config;
    },
    (error) => {
        return Promise.reject(error);
    },
);

axios.interceptors.request.use(request => {
    // console.log('Starting Request', JSON.stringify(request, null, 2))
    return request
})

axiosInstance.interceptors.response.use((response) => {
    // console.log("response :: ", response);
    return response
}, async function (error) {
    const originalRequest = error.config;

    // const refresh_token = await AsyncStorage.getItem('refresh_token');

    // if ((error.response.status === 401 || error.response.status === 402) && !originalRequest._retry) {

    // }

    return Promise.reject(error);


});

export default axiosInstance;
