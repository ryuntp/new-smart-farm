export const handleSelectPounds = (newPound, setPound, setNewPoundError) => {
    let newPoundList = [];
    let firstVal;
    let lastVal;

    if (!newPound) return;

    const regexPound = /^[0-9|{,}| -]*$/gi;
    if (!regexPound.test(newPound.trim())) {
      return setNewPoundError(true);
    }
    if (newPound.includes(",")) {
      newPound.split(",").forEach((pound) => {
        if (pound.split("-").length === 1) {
          setNewPoundError(false);
          newPoundList.push(pound);
        } else if (pound.includes("-")) {
          setNewPoundError(false);
          firstVal = parseInt(pound.split("-")[0]);
          lastVal = parseInt(pound.split("-")[1]);
          if (!firstVal || !lastVal) {
            return setNewPoundError(true);
          }
          if (firstVal >= lastVal) {
            return setNewPoundError(true);
          }
          for (firstVal; firstVal <= lastVal; firstVal++) {
            newPoundList.push(firstVal.toString());
          }
        }
      });
    } else {
      if (newPound.split("-").length === 1) {
        setNewPoundError(false);
        newPoundList.push(newPound);
      } else if (newPound.includes("-")) {
        setNewPoundError(false);
        firstVal = parseInt(newPound.split("-")[0]);
        lastVal = parseInt(newPound.split("-")[1]);
        if (!firstVal || !lastVal) {
          return setNewPoundError(true);
        }
        if (firstVal >= lastVal) {
          return setNewPoundError(true);
        }
        for (firstVal; firstVal <= lastVal; firstVal++) {
          newPoundList.push(firstVal.toString());
        }
      }
    }
    
    console.log("newPoundList => ", newPoundList);
    return setPound(newPoundList);
  };