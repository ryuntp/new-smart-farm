import * as React from "react";
import useCollapse from "react-collapsed";

export default function Collapse({ isActive, render_children }) {
    const [isExpanded, setExpanded] = React.useState(isActive);
    const { getToggleProps, getCollapseProps } = useCollapse({
        isExpanded
    });

    React.useEffect(() => {
        console.log("render_children LL ",render_children);
        setExpanded(isActive);
    }, [isActive, setExpanded]);

    return (
        <>

            <div {...getCollapseProps({ style: { backgroundColor: "#fff" } })}>
            {render_children}
                {/* <h2 style={{ margin: 0, padding: 10 }}>
               
                    Start editing to see some magic happen!
                </h2> */}
            </div>
        </>
    );
}


