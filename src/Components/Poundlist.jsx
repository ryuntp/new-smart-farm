
import React, { useContext, useEffect, useState } from "react";
import { Dropdown } from "react-bootstrap";
import { handleSelectPounds } from "../util/handleInput";
import CalendarsworkerStore from '../Stores/CalendarsworkerStore'
import _ from 'lodash'
import icon_midelete from "../Assets/img/mi-delete.png";
import Switch from "react-switch";
import { Input, } from "reactstrap";

import icon_downarrow from '../Assets/img/dropdown-down.png'
import { observer } from "mobx-react";
import UIstore from "../Stores/UIstore";
import moment from "moment";
import Rootstore from "../Stores/Rootstore";


export const Poundlist = observer((props) => {

    const uistore = useContext(UIstore);
    const rootstore = useContext(Rootstore);
    const calendarsworkerStore = useContext(CalendarsworkerStore);
    const [newPoundError, setNewPoundError] = useState(false);
    const [disable, setdisable] = useState(false)
    const [poundlistSplitlot, setpoundlistSplitlot] = useState(props.poundlistSplitlot)
    useEffect(() => {
        rootstore.Setdatacardinfo(props.cardinfo)
    }, []);

    const calPoundList = (arr) => {
        let ans = [];
        let firstval;
        for (let [i, num] of arr.entries()) {
            if ((Number(num) + 1) === Number(arr[i + 1])) {
                if (!firstval) firstval = num;
            } else {
                if (firstval) {
                    if (firstval !== num) {
                        ans.push(`${firstval}-${num}`);
                    }
                } else {
                    ans.push(num.toString());
                }

                firstval = undefined;
            }

        }

        return ans;
    };

    const CustomToggle = React.forwardRef(
        (props, ref) => (
            <div
                className={`d-flex flex-row justify-content-between mt-3 ${!disable ? 'drop-downwork-blue' : 'drop-downwork-gray'}`}
                ref={ref}
                onClick={e => {
                    if (!disable) {
                        e.preventDefault();

                        props.onClick(e);
                    } else {
                        e.preventDefault();
                    }

                }}
            >
                {props.children}
                <img src={icon_downarrow} alt="" className='icon-mini' />
            </div>
        )
    );


    const CustomMenu = React.forwardRef(
        (props, ref) => {
            return (
                <div
                    ref={ref}
                    style={props.style}
                    className={[props.className]}
                    aria-labelledby={props.labeledBy}
                    onClick={() => {

                    }}
                >
                    {props.children}
                </div>
            );
        }
    );

    const RenderDropdownDisable = () => {
        return <div className="w-45">
            <Dropdown
                onSelect={(e) => {
                }}>
                <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components">
                    <span className="sarabun_L_300 text_black">

                    </span>
                </Dropdown.Toggle>
                <Dropdown.Menu as={CustomMenu}>
                    {poundlistSplitlot.map(item => {
                        return (
                            <Dropdown.Item key={item.value} eventKey={item.value}>
                                {item.label}
                            </Dropdown.Item>
                        );
                    })}
                </Dropdown.Menu>
            </Dropdown>
        </div>
    }

    const RenderDropdown = (item, index) => {
        return <div className="w-45">
            <Dropdown

                onSelect={(e) => {
                    item.insectarium_name = e
                }}>
                <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components">
                    <span className="sarabun_L_300 text_black">
                        {item.insectarium_name}
                    </span>
                </Dropdown.Toggle>
                <Dropdown.Menu disabled as={CustomMenu}>
                    {poundlistSplitlot.map(item => {
                        return (
                            <Dropdown.Item key={item.value} eventKey={item.value}>
                                {item.label}
                            </Dropdown.Item>
                        );
                    })}
                </Dropdown.Menu>
            </Dropdown>
        </div>
    }




    return (
        <div className="p-3 pt-2">
            <div className="d-flex flex-row w-100 align-items-center justify-content-between">
                <span className="prompt_XL ">{props.title}</span>
                {props.checked === false ? null : <div className="d-flex flex-row  align-items-center ">
                    <Input
                        type="checkbox"
                        className="checkboxes-xl mr-2"
                        defaultChecked={false}
                        onChange={(e) => {
                            setdisable(!disable)
                            var datainfo = []
                            for (const key in props.poundlistForcheck) {
                                if (Object.hasOwnProperty.call(props.poundlistForcheck, key)) {
                                    const element = props.poundlistForcheck[key];
                                   console.log(element);
                                    datainfo.push({
                                        insectarium_name: element.insectarium_name,
                                        pound_name: element.pound_name,
                                        Error_pound: false,
                                    })
                                }
                            }
                            rootstore.Setdatacardinfo(datainfo)

                        }}
                    />
                    <span className="prompt_L text_dimgray">
                        ที่เหลือทั้งหมด
                    </span>
                </div>}

            </div>
            {disable ? <div className="d-flex flex-row align-items-end justify-content-between mb-2">
                {RenderDropdownDisable()}
                <div className=" w-45">
                    <input
                        disabled={true}
                        type={'text'}
                        className={`w-100 input-pond-gray`} />
                </div>
            </div> : rootstore.datacardinfo.map((item, index) => {
                const setvalue = (data) => {
                    item.pound_name = data
                }
                const Checkpoundremain = (e) => {
                    var remainingPound = props.poundlistForcheck
                    handleSelectPounds(
                        e.target.value,
                        setvalue,
                        setNewPoundError
                    )
                    var index_remaining = remainingPound.findIndex((ele) => item.insectarium_name === ele.insectarium_name)
                    var remaining_pound_name = remainingPound[index_remaining]
                    if (index_remaining === -1) {
                        return
                    }
                    for (const key in item.pound_name) {
                        if (Object.hasOwnProperty.call(item.pound_name, key)) {
                            const elementPound_name = item.pound_name[key];
                            var findpound = remaining_pound_name.pound_name.findIndex((ele) => Number(ele) === Number(elementPound_name))

                            if (findpound === -1) {
                                item.Error_pound = true
                            } else {
                                item.Error_pound = false
                            }
                        }
                    }
                }
                return <div className="d-flex flex-row align-items-end justify-content-between mb-2">
                    {RenderDropdown(item, index)}
                    <div className=" w-45">
                        <div className="position-relative d-flex flex-row justify-content-center align-item-center" >
                            <input
                                disabled={disable}
                                value={calPoundList(item.pound_name)}
                                type={'text'}
                                onChange={(e) => {
                                    Checkpoundremain(e)
                                }}
                                className={`w-100 ${!disable ? 'input-pond' : 'input-pond-gray'}`} />

                            {index === 0 ? null : <div
                                onClick={() => {
                                    rootstore.removedatacardinfo(index)
                                }}
                                className="d-flex flex-row justify-content-center align-items-center">
                                <img src={icon_midelete} className="icon-minismall ml-3" />
                            </div>}
                            {item.Error_pound === true ? <span
                                className="position-absolute prompt_M"
                                style={{ bottom: '-20px' }}>
                                ไม่มีบ่อที่เลือก
                            </span> : null}
                        </div>
                    </div>
                </div>
            })}
            {
                !disable ? <div onClick={() => {
                    var cardinfo = {
                        insectarium_name: "",
                        pound_name: [],
                        Error_pound: false,

                    }
                    rootstore.Adddatacardinfo(cardinfo)
                }} className="d-flex flex-row align-item-center mt-4">
                    <div className="add-circle-darkturquoise mr-3 " />

                    <span className="prompt_XL">ระบุงานที่ทำเพิ่ม</span>
                </div> : null
            }

        </div>
    );
})


// const HomeDash = () => {

// };

export default Poundlist;
