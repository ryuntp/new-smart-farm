import React, { useState, useEffect } from "react";

import icon_leftarrow from "../../Assets/img/dropdown-left-white.png";
import icon_stagenotify from "../../Assets/img/b-stage-notify-urgent.png";

import icon_generalhistory from "../../Assets/img/icon-general-history.png";
import icon_rightarrow from "../../Assets/img/dropdown-right-white.png";

import _ from "lodash";
import { observer } from "mobx-react-lite";
import UIstore from "../../Stores/UIstore";
import { useContext } from "react";
import Appmodal from "./Appmodal";
import { Dropdown } from "react-bootstrap";
import { Input } from "reactstrap";
import EscapeStore from "../../Stores/EscapeStore";
import icon_midelete from "../../Assets/img/mi-delete.png";
import icon_downarrow from "../../Assets/img/dropdown-down.png";
import { handleSelectPounds } from "../../util/handleInput";
import axiosInstance from "../../Lib/AxiosInterceptor";
import Poundlist from "../../Components/Poundlist";
import Rootstore from "../../Stores/Rootstore";
import { toJS } from "mobx";

const EscapeModal = observer((props) => {
  const [value, setValue] = useState("");
  const [note, setnote] = useState("");
  const [pound, setPound] = useState([]);
  const [newPoundError, setNewPoundError] = useState(false);
  const [isError, setIsError] = useState(false);
  const [isUpdate, SetisUpdate] = useState(false);
  const [poundlistSplitlot, setpoundlistSplitlot] = useState([
    {
      label: "ทุกวัน",
      value: "ทุกวัน",
    },
  ]);
  const rootstore = useContext(Rootstore);

  const handleCountPound = () => {
    var curPound = toJS(rootstore.datacardinfo);
    var sum = 0;
    for (var pound of curPound) {
      sum += pound.pound_name.length;
    }

    return sum;
  };

  const handleChange = () => {
    var poundlist = JSON.parse(localStorage.getItem("insectariums")) || [];

    var insectarium_name_list = [
      {
        label: "ไม่มีข้อมูล",
        value: "ไม่มีข้อมูล",
      },
    ];
    for (const key in poundlist) {
      if (Object.hasOwnProperty.call(poundlist, key)) {
        const element = poundlist[key];
        var dateElement = {
          label: element.insectarium_name,
          value: element.insectarium_name,
        };

        insectarium_name_list.push(dateElement);
      }
    }
    setpoundlistSplitlot(insectarium_name_list);
  };

  const [Breed_Arr, setBreed_arr] = useState([
    {
      label: "New York",
      value: "newYork",
    },
    {
      label: "Dublin",
      value: "dublin",
    },
    {
      label: "Istanbul",
      value: "istanbul",
    },
    {
      label: "California",
      value: "colifornia",
    },
    {
      label: "Izmir",
      value: "izmir",
    },
    {
      label: "Oslo",
      value: "oslo",
    },
  ]);

  const onSubmit = async () => {
    let initPoundList = toJS(rootstore.datacardinfo);
    console.log("initPoundList ", initPoundList);

    if (newPoundError) return setIsError(true);

    setIsError(false);

    var packDatasend = {
      poundlist: [],
      name: "พบจิ้งหรีดหลุด",
      worker_id: localStorage.getItem("id"),
      type: 1,
      farm_id: localStorage.getItem("farm_id"),
      status: 0,
      note: note,
    };

    for (const key in initPoundList) {
      if (initPoundList.hasOwnProperty.call(initPoundList, key)) {
        const element = initPoundList[key];
        console.log("element ", element);
        if (element.Error_pound === true) {
          setIsError(true);
          return;
        }
        delete element.Error_pound;
        packDatasend.poundlist.push(element);
      }
    }

    console.log("packDatasend ", packDatasend);
    try {
      const createNoti = await axiosInstance.post(
        `/admin/createnoti/createproblem/`,
        packDatasend
      );

      console.log("createNoti ", createNoti);
      if (createNoti.status === 200) {
        uistore.SetaddEscapeModal(!uistore.addEscapeModal);
        setPound([]);
        setnote("");
      }
    } catch (err) {
      console.error("err jaa", err.message);
    }
  };

  useEffect(() => {
    console.log(
      "localStorage ",
      JSON.parse(localStorage.getItem("insectariums"))
    );
    const fetch = async () => {
      //   await getNotifications();
      handleChange();
    };

    fetch();
  }, [pound, newPoundError, note]);

  const uistore = useContext(UIstore);
  const escapestore = useContext(EscapeStore);

  return (
    <div>
      <Appmodal visible={uistore.EscapeModal} nocontainer={false}>
        <div className="problem-modal">
          <div className="head-content">
            <div
              onClick={() => {
                uistore.SetEscapemodal(!uistore.EscapeModal);
              }}
              className="d-flex flex-row justify-content-center align-items-center"
              style={{
                position: "absolute",
                top: 20,
                left: 15,
              }}
            >
              <img src={icon_leftarrow} alt="" className="icon-mini" />
              <span className="sarabun_L_100 text_white">BACK</span>
            </div>

            <div className="text-content">
              <span className="prompt_XXL text_white">พบจิ้งหรีดหลุด</span>

              <div className="history-tag mt-2 d-flex flex-row justify-content-center align-items-center">
                <img src={icon_generalhistory} alt="" className="icon-mini" />

                <span className="prompt_M text_orange">ดูประวัติ</span>
              </div>
            </div>
          </div>

          <div className="d-flex justify-content-center align-items-center d-flex flex-column mt-5">
            <img src={icon_stagenotify} alt="" className="icon-ExLarge" />

            <span className="prompt_XXL text_silver ">ยังไม่ได้ระบุ</span>
            <span className="mt-2 sarabun_L_300 text_silver">
              กรุณาระบุรายละเอียด
            </span>

            <div
              onClick={() => {
                uistore.SetaddEscapeModal(!uistore.addEscapeModal);
              }}
              className="btn-confirm mt-4"
            >
              <span className="prompt_XL text_white ">+ ระบุเลย</span>
            </div>
          </div>
        </div>
      </Appmodal>

      {/* add problem */}

      <Appmodal
        visible={uistore.addEscapeModal}
        toggle_click={() => {
          uistore.SetaddEscapeModal(!uistore.addEscapeModal);
        }}
        nocontainer={true}
        className={"d-flex justify-content-center align-items-center"}
      >
        <div
          className="d-flex flex-column justify-content-between"
          style={{
            width: "100%",
            minHeight: 600,
            backgroundColor: "#fff",
            padding: 17,
          }}
        >
          <div className="d-flex flex-column">
            <span className="prompt_XXL text_blueteal">รายละเอียด</span>

            {/* <div
              onClick={() => {
                uistore.SetaddEnemyListModal(!uistore.addEnemyListModal);
              }}
              className="d-flex flex-row align-items-center justify-content-between py-3 pb-4"
            >
              <span className="text_black prompt_XL">ศัตรูจิ้งหรีดที่พบ</span>
              {problemstore.enemy === "" ? (
                <div className="add-circle-darkturquoise">
                  <span className="text_white"></span>
                </div>
              ) : (
                <div className="d-flex flex-row align-items-center">
                  <span className=" text_black prompt_L mr-3 ">
                    {problemstore.enemy}
                  </span>
                  <div className="add-circle-darkturquoise">
                    <span className="text_white"></span>
                  </div>
                </div>
              )}
            </div> */}
            <div className="position-relative mt-1 ">
              <div className=" line-solid not-absolute" />
            </div>
            {/* <span className="mt-3 mb-3 text_black prompt_XL">
              บ่อที่พบจิ้งหรีดหลุด
            </span> */}
            <div className="d-flex flex-row w-100 justify-content-between">
              <Poundlist
                poundlistSplitlot={poundlistSplitlot}
                title={"บ่อที่พบจิ้งหรีดหลุด"}
                isUpdate={isUpdate}
                poundlistForcheck={JSON.parse(
                  localStorage.getItem("insectariums")
                )}
                cardinfo={[
                  {
                    insectarium_name: "",
                    pound_name: [],
                    Error_pound: false,
                  },
                ]}
                provider={rootstore}
                checked={false}
              />
            </div>
          </div>

          <div>
            <div className="position-relative mt-1 ">
              <div className=" line-dash not-absolute" />
            </div>

            <div
              onClick={() => {
                uistore.SetaddNoteEscapeModal(!uistore.addEscapeNoteModal);
              }}
              className="d-flex flex-row align-items-center justify-content-between pt-3 pb-1"
            >
              <span className="text_black prompt_XL">หมายเหตุ</span>
              {escapestore.note === "" ? (
                <div className="add-circle-darkturquoise">
                  <span className="text_white"></span>
                </div>
              ) : (
                <div className="d-flex flex-row align-items-center">
                  <span className=" text_black prompt_L mr-3 ">
                    {escapestore.note}
                  </span>
                  <div className="add-circle-darkturquoise">
                    <span className="text_white"></span>
                  </div>
                </div>
              )}
            </div>
            <div className="ml-3">
              <span className="text_black">{note ? note : null}</span>
            </div>

            <div
              onClick={async () => {
                // uistore.SetduplicateEscapemodal(!uistore.duplicateEscapemodal);
                await onSubmit();
              }}
              className="btn-confirm-100 mt-2 d-flex flex-row align-items-center justify-content-between"
            >
              <div className="d-flex flex-row align-items-center">
                <span className="sarabun_M_300 text_white ">Total :</span>
                <span className="prompt_XXL text_white mx-2">
                  {handleCountPound() || "-"}
                </span>
                <span className="sarabun_M_300 text_white ">บ่อ</span>
              </div>

              <div className="btn-minisubmit d-flex justify-content-center align-items-center">
                <img src={icon_rightarrow} alt="" className="icon-mini" />
              </div>
            </div>
          </div>
        </div>
      </Appmodal>
      {/* add enemy */}
      {/* <Appmodal
        visible={uistore.addEnemyListModal}
        toggle_click={() => {
          uistore.SetaddEnemyListModal(!uistore.addEnemyListModal);
        }}
        nocontainer={true}
        className={"d-flex justify-content-center align-items-center"}
      >
        <div
          className="d-flex flex-column "
          style={{
            width: "100%",
            minHeight: 600,
            backgroundColor: "#fff",
          }}
        >
          <div className="note-headmodal d-flex align-items-center w-100">
            <div
              onClick={() => {
                uistore.SetaddEnemyListModal(!uistore.addEnemyListModal);
              }}
              className="d-flex flex-row justify-content-center align-items-center"
            >
              <img src={icon_leftarrow} alt="" className="icon-mini" />
              <span className="sarabun_L_100 text_black">BACKss</span>
            </div>
          </div>

          <div className="px-4 mt-4">
            <div className="d-flex flex-row justify-content-between align-items-center">
              <span className="prompt_XL text_black">ศัตรูจิ้งหรีดที่พบ</span>
            </div>
          </div>

          <div className="mx-4 mt-4">
            <FormGroup>
              <Row className="my-3">
                <Label>
                  <Input
                    type="checkbox"
                    className="checkboxes"
                    id="0"
                    onClick=""
                  />
                  <span className="form-check-sign ml-2 prompt_L">
                    เลือกทั้งหมด
                  </span>
                </Label>
              </Row>
              <div className="position-relative mt-1 ">
                <div className=" line-solid not-absolute" />
              </div>
              <Row className="my-3">
                <Label check>
                  <Input
                    type="checkbox"
                    className="checkboxes"
                    id="1"
                    onClick=""
                  />
                  <span className="form-check-sign ml-2 prompt_L">มด</span>
                </Label>
              </Row>
              <div className="position-relative mt-1 ">
                <div className=" line-solid not-absolute" />
              </div>
              <Row className="my-3">
                <Label check>
                  <Input
                    type="checkbox"
                    className="checkboxes"
                    id="2"
                    onClick=""
                  />
                  <span className="form-check-sign ml-2 prompt_L">แมลงทับ</span>
                </Label>
              </Row>
              <div className="position-relative mt-1 ">
                <div className=" line-solid not-absolute" />
              </div>
              <Row className="my-3">
                <Label check>
                  <Input
                    type="checkbox"
                    className="checkboxes"
                    id="3"
                    onClick=""
                  />
                  <span className="form-check-sign ml-2 prompt_L">ไร</span>
                </Label>
              </Row>
              <div className="position-relative mt-1 ">
                <div className=" line-solid not-absolute" />
              </div>
              <Row className="my-3">
                <Label check>
                  <Input
                    type="checkbox"
                    className="checkboxes"
                    id="4"
                    onClick=""
                  />
                  <span className="form-check-sign ml-2 prompt_L">อื่นๆ</span>
                </Label>
              </Row>
            </FormGroup>{" "}
          </div>

          <div className="d-flex flex-row mx-4 justify-content-between mt-4">
            <div
              onClick={() => {
                uistore.SetaddEnemyListModal(!uistore.addEnemyListModal);
              }}
              className="btn-minicancle d-flex justify-content-center align-items-center"
            >
              <span className="prompt_XL text_dodgerblue">ยกเลิก</span>
            </div>
            <div
              onClick={() => {
                escapestore.setNote(note);
                uistore.SetaddEnemyListModal(!uistore.addEnemyListModal);
              }}
              className="btn-miniconfirm"
            >
              <span className="prompt_XL text_white">ยืนยัน</span>
            </div>
          </div>
        </div>
      </Appmodal> */}
      {/* add note */}
      <Appmodal
        visible={uistore.addEscapeNoteModal}
        toggle_click={() => {
          uistore.SetaddNoteEscapeModal(!uistore.addEscapeNoteModal);
        }}
        nocontainer={true}
        className={"d-flex justify-content-center align-items-center"}
      >
        <div
          className="d-flex flex-column "
          style={{
            width: "100%",
            minHeight: 600,
            backgroundColor: "#fff",
          }}
        >
          <div className="note-headmodal d-flex align-items-center w-100">
            <div
              onClick={() => {
                uistore.SetaddNoteEscapeModal(!uistore.addEscapeNoteModal);
              }}
              className="d-flex flex-row justify-content-center align-items-center"
            >
              <img src={icon_leftarrow} alt="" className="icon-mini" />
              <span className="sarabun_L_100 text_black">BACK</span>
            </div>
          </div>

          <div className="px-4 mt-4">
            <div className="d-flex flex-row justify-content-between align-items-center">
              <span className="prompt_XL text_black">หมายเหตุ</span>

              <div
                onClick={() => {
                  setnote(null);
                  uistore.SetaddNoteEscapeModal(!uistore.addEscapeNoteModal);
                }}
              >
                <img src={icon_midelete} className="icon-minismall mr-3" />
                <span className="prompt_M_200 text-decoration-underline text_tomato">
                  ลบ
                </span>
              </div>
            </div>
          </div>

          <div className="mx-4 mt-4">
            <textarea
              value={note}
              onChange={(e) => {
                // console.log("e",);
                setnote(e.target.value);
              }}
              className="input-area-addnote"
              id="w3review"
              name="w3review"
              rows="4"
              cols="50"
            ></textarea>
          </div>

          <div className="d-flex flex-row mx-4 justify-content-between mt-4">
            <div
              onClick={() => {
                uistore.SetaddNoteEscapeModal(!uistore.addEscapeNoteModal);
              }}
              className="btn-minicancle d-flex justify-content-center align-items-center"
            >
              <span className="prompt_XL text_dodgerblue">ยกเลิก</span>
            </div>
            <div
              onClick={() => {
                // escapestore.setNote(note);
                uistore.SetaddNoteEscapeModal(!uistore.addEscapeNoteModal);
              }}
              className="btn-miniconfirm d-flex justify-content-center align-items-center"
            >
              <span className="prompt_XL text_white">ยืนยัน</span>
            </div>
          </div>
        </div>
      </Appmodal>
      {/* Check duplicate data */}
      <Appmodal
        visible={uistore.duplicateEscapemodal}
        toggle_click={() => {
          uistore.SetduplicateEscapemodal(!uistore.duplicateEscapemodal);
        }}
        nocontainer={true}
        className={"d-flex justify-content-center align-items-center"}
      >
        <div
          className="d-flex flex-column align-items-center"
          style={{
            width: "100%",
            minHeight: 600,
            backgroundColor: "#fff",
          }}
        >
          <div className="head-duplicate-problemmodal" />
          <div className="px-4 d-flex flex-column">
            <span
              className="prompt_XXL text_orangefatbg mt-4 text-center"
              style={{
                lineHeight: 1.13,
              }}
            >
              บ่อเลี้ยงที่คุณแจ้งบางบ่อ มีคนแจ้งปัญหาแล้ว
            </span>

            <span className="sarabun_L_300 text_dimgray my-3 mb-4 text-center">
              เราจะนำเลขบ่อที่คุณแจ้งซ้ำ
              ออกจากรายการแจ้งเตือนของคุณให้อัตนโนมัติ
            </span>

            <div className="card-mention-orange d-flex flex-column">
              <span className="prompt_XL text_black mb-2">รายการซ้ำ</span>

              <div className="d-flex flex-row align-items-center">
                <span className="sarabun_M text_black">โรงเลี้ยง 01 : </span>
                <span className="sarabun_M_300 text_black ml-1">
                  {" "}
                  101-103,109
                </span>
              </div>
            </div>

            <div
              onClick={() => {
                uistore.SetduplicateEscapemodal(!uistore.duplicateEscapemodal);
              }}
              className="btn-confirm-100 d-flex justify-content-center align-items-center"
              style={{
                marginTop: -10,
              }}
            >
              <span className="prompt_XL text_white">
                ยืนยัน...เอาบ่อซ้ำออก
              </span>
            </div>
          </div>
        </div>
      </Appmodal>
    </div>
  );
});

export default EscapeModal;
