import React, { useState, useEffect } from "react";

import icon_leftarrowwhite from '../../Assets/img/dropdown-left-white.png'

import icon_leftarrow from '../../Assets/img/dropdown-left.png'
import icon_downarrow from '../../Assets/img/dropdown-down.png'
import icon_rightarrow from '../../Assets/img/dropdown-right-white.png'
import icon_stagebeed from '../../Assets/img/b-stage-notify-beed.png'
import icon_midelete from '../../Assets/img/mi-delete.png'

import icon_generalhistory from '../../Assets/img/icon-general-history.png'


import _ from 'lodash';
import { observer } from 'mobx-react-lite'
import UIstore from "../../Stores/UIstore";
import Breedstore from "../../Stores/Breedstore";
import { useContext } from "react";
import Appmodal from "./Appmodal";
import { Dropdown, FormControl } from 'react-bootstrap';

const Breedmodal = observer((props) => {
    const [dropdownOpen, setdropdownOpen] = useState(false)
    const [value, setValue] = useState("");

    const [note, setnote] = useState("");

    const [Breed_Arr, setBreed_arr] = useState([
        {
            label: 'New York',
            value: 'newYork',
        },
        {
            label: 'Dublin',
            value: 'dublin',
        },
        {
            label: 'Istanbul',
            value: 'istanbul',
        },
        {
            label: 'California',
            value: 'colifornia',
        },
        {
            label: 'Izmir',
            value: 'izmir',
        },
        {
            label: 'Oslo',
            value: 'oslo',
        },
    ])
    const CustomToggle = React.forwardRef(
        (props, ref) => (
            <div
                className='drop-down-blue d-flex flex-row justify-content-between'
                ref={ref}
                onClick={e => {
                    e.preventDefault();

                    props.onClick(e);
                }}
            >
                {props.children}
                <img src={icon_downarrow} alt="" className='icon-mini' />
            </div>
        )
    );
    const CustomMenu = React.forwardRef(
        (props, ref) => {
            return (
                <div
                    ref={ref}
                    style={props.style}
                    className={props.className}
                    aria-labelledby={props.labeledBy}
                    onClick={() => {
                        console.log(":props ;:", props.key);
                    }}
                >
                    {props.children}
                </div>
            );
        }
    );

    const uistore = useContext(UIstore)
    const breedstore = useContext(Breedstore)

    return (
        <div>
            {/* MAIN modal */}
            <Appmodal
                visible={uistore.Breedmodal}
                nocontainer={false}
            >
                <div className='breed-modal'>
                    <div className='head-content'>
                        <div
                            onClick={() => {
                                uistore.SetBreedmodal(!uistore.Breedmodal)
                            }}
                            className="d-flex flex-row justify-content-center align-items-center"
                            style={{
                                position: 'absolute',
                                top: 20,
                                left: 15
                            }}>
                            <img src={icon_leftarrowwhite} alt='' className='icon-mini' />
                            <span className='sarabun_L_100 text_white'>
                                BACK
                            </span>
                        </div>

                        <div className='text-content'>
                            <span className='prompt_XXL text_white'>
                                พร้อมผสมพันธ์
                            </span>

                            <div className='history-tag mt-2 d-flex flex-row justify-content-center align-items-center'>
                                <img src={icon_generalhistory} alt="" className='icon-mini' />

                                <span className='prompt_M text_orange'>ดูประวัติ</span>
                            </div>
                        </div>


                    </div>

                    <div className="d-flex justify-content-center align-items-center d-flex flex-column mt-5">
                        <img src={icon_stagebeed} alt="" className='icon-ExLarge' />

                        <span className='prompt_XXL text_silver '>
                            ยังไม่ได้ระบุ
                        </span>
                        <span className='mt-2 sarabun_L_300 text_silver'>
                            กรุณาระบุบ่อเลี้ยงที่พบสัญญาน
                        </span>

                        <div
                            onClick={() => {
                                uistore.SetaddBreedmodal(!uistore.addBreedmodal)
                            }}
                            className='btn-confirm mt-4'>
                            <span className='prompt_XL text_white '>+ ระบุเลย</span>
                        </div>


                    </div>


                </div>
            </Appmodal>

            {/* add Breeding */}

            <Appmodal
                visible={uistore.addBreedmodal}

                toggle_click={() => {
                    uistore.SetaddBreedmodal(!uistore.addBreedmodal)
                }}
                nocontainer={true}
                className={'d-flex justify-content-center align-items-center'}
            >
                <div
                    className='d-flex flex-column justify-content-between'
                    style={{
                        width: '100%',
                        minHeight: 600,
                        backgroundColor: '#fff',
                        padding: 17
                    }}>

                    <div className='d-flex flex-column'>
                        <span className='prompt_XXL text_blueteal'>
                            บ่อที่พร้อมผสมพันธ์
                        </span>

                        <span className='prompt_XL text_black my-3'>
                            บ่อที่พร้อมผสมพันธ์
                        </span>


                        <div className='d-flex flex-row w-100 justify-content-between'>
                            <Dropdown onSelect={(e) => {
                                console.log("e", e);
                                setValue(e)
                            }}>
                                <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components">
                                    <span className="sarabun_L_300 text_black">
                                        {value}
                                    </span>
                                </Dropdown.Toggle>
                                <Dropdown.Menu as={CustomMenu}>
                                    {Breed_Arr.map(item => {
                                        return (
                                            <Dropdown.Item key={item.label} eventKey={item.label}>
                                                {item.value}
                                            </Dropdown.Item>
                                        );
                                    })}
                                </Dropdown.Menu>
                            </Dropdown>

                            <div>
                                <input className='input-breed w-100' />
                            </div>
                            {/* <div className='drop-down-blue'>
                              
                            </div> */}

                        </div>


                    </div>

                    <div >
                        <div className='position-relative mt-1 '>

                            <div className=' line-dash not-absolute' />

                        </div>

                        <div
                            onClick={() => {
                                uistore.SetaddnoteBreedmodal(!uistore.addnoteBreedmodal)
                            }}
                            className='d-flex flex-row align-items-center justify-content-between py-3 pb-4'>
                            <span className="text_black prompt_XL">หมายเหตุ</span>
                            {
                                breedstore.notebreed === '' ? <div className='add-circle-darkturquoise'>
                                    <span className="text_white"></span>
                                </div> :
                                    <div className='d-flex flex-row align-items-center'>
                                        <span className=" text_black prompt_L mr-3 ">{breedstore.notebreed}</span>
                                        <div className='add-circle-darkturquoise'>
                                            <span className="text_white"></span>
                                        </div>
                                    </div>
                            }
                        </div>

                        <div
                            onClick={() => {
                                uistore.SetduplicateBreedmodal(!uistore.duplicateBreedmodal)
                            }}
                            className='btn-confirm-100 mt-2 d-flex flex-row align-items-center justify-content-between'>

                            <div className='d-flex flex-row align-items-center'>
                                <span className="sarabun_M_300 text_white ">Total :</span>
                                <span className="prompt_XXL text_white mx-2"> 10</span>
                                <span className="sarabun_M_300 text_white ">บ่อ</span>
                            </div>

                            <div className='btn-minisubmit d-flex justify-content-center align-items-center'>
                                <img src={icon_rightarrow} alt="" className='icon-mini' />
                            </div>


                        </div>
                    </div>


                </div>
            </Appmodal>


            {/* add note */}
            <Appmodal
                visible={uistore.addnoteBreedmodal}

                toggle_click={() => {
                    uistore.SetaddnoteBreedmodal(!uistore.addnoteBreedmodal)
                }}
                nocontainer={true}
                className={'d-flex justify-content-center align-items-center'}
            >


                <div
                    className='d-flex flex-column '
                    style={{
                        width: '100%',
                        minHeight: 600,
                        backgroundColor: '#fff',

                    }}>

                    <div className='note-headmodal d-flex align-items-center w-100'>
                        <div
                            onClick={() => {
                                uistore.SetaddnoteBreedmodal(!uistore.addnoteBreedmodal)
                            }}
                            className="d-flex flex-row justify-content-center align-items-center"
                        >
                            <img src={icon_leftarrow} alt='' className='icon-mini' />
                            <span className='sarabun_L_100 text_black'>
                                BACK
                            </span>
                        </div>
                    </div>

                    <div className="px-4 mt-4">

                        <div className='d-flex flex-row justify-content-between align-items-center'>

                            <span className='prompt_XL text_black'>หมายเหตุ</span>

                            <div>
                                <img src={icon_midelete} className='icon-minismall mr-3' />
                                <span className='prompt_M_200 text-decoration-underline text_tomato'>ลบ</span>
                            </div>

                        </div>

                    </div>


                    <div className='mx-4 mt-4'>
                        <textarea
                            // /breedstore.notebreed
                            value={note}
                            onChange={(e) => {
                                // console.log("e",);
                                setnote(e.target.value)
                            }}
                            className='input-area-addnote'
                            id="w3review"
                            name="w3review"
                            rows="4"
                            cols="50"
                        ></textarea>
                    </div>

                    <div className='d-flex flex-row mx-4 justify-content-between mt-4'>
                        <div
                            onClick={() => {
                                uistore.SetaddnoteBreedmodal(!uistore.addnoteBreedmodal)
                            }}
                            className='btn-minicancle d-flex justify-content-center align-items-center'>
                            <span className="prompt_XL text_dodgerblue">ยกเลิก</span>
                        </div>
                        <div
                            onClick={() => {
                                breedstore.Setnotebreed(note)
                                uistore.SetaddnoteBreedmodal(!uistore.addnoteBreedmodal)
                            }}
                            className='btn-miniconfirm d-flex justify-content-center align-items-center'>
                            <span className="prompt_XL text_white">ยืนยัน</span>
                        </div>
                    </div>



                </div>
            </Appmodal>

            {/* Check duplicate data */}
            <Appmodal
                visible={uistore.duplicateBreedmodal}
                toggle_click={() => {
                    uistore.SetduplicateBreedmodal(!uistore.duplicateBreedmodal)
                }}

                nocontainer={true}
                className={'d-flex justify-content-center align-items-center'}
            >


                <div
                    className='d-flex flex-column align-items-center'
                    style={{
                        width: '100%',
                        minHeight: 600,
                        backgroundColor: '#fff',

                    }}>
                    <div className="head-duplicate-breedmodal" />
                    <div className='px-4 d-flex flex-column'>



                        <span className="prompt_XXL text_blueteal mt-4 text-center" style={{
                            lineHeight: 1.13
                        }}>
                            บ่อเลี้ยงที่คุณแจ้งบางบ่อ
                            มีคนแจ้งสัญญาณแล้ว
                        </span>

                        <span className='sarabun_L_300 text_dimgray my-3 mb-4 text-center'>
                            เราจะนำเลขบ่อที่คุณแจ้งซ้ำ ออกจากรายการแจ้งเตือนของคุณให้อัตนโนมัติ
                        </span>


                        <div className='cardcolor-lightcyan d-flex flex-column'>
                            <span className='prompt_XL text_black mb-2'>รายการซ้ำ</span>

                            <div className='d-flex flex-row align-items-center'>
                                <span className="sarabun_M text_black">โรงเลี้ยง 01 : </span>
                                <span className="sarabun_M_300 text_black ml-1"> 101-103,109</span>
                            </div>

                        </div>


                        <div
                            onClick={() => {
                                uistore.SetduplicateBreedmodal(!uistore.duplicateBreedmodal)
                            }}
                            className='btn-confirm-100 d-flex justify-content-center align-items-center' style={{
                                marginTop: -10
                            }}>

                            <span className='prompt_XL text_white'>
                                ยืนยัน...เอาบ่อซ้ำออก
                            </span>
                        </div>


                    </div>






                </div>
            </Appmodal>
        </div>
    )
})

export default Breedmodal