import React, { useState, useEffect } from "react";

import { Input, } from "reactstrap";
import _ from 'lodash';
import icon_search from '../../Assets/img/ico-search.png'
import { observer } from 'mobx-react-lite'
import UIstore from "../../Stores/UIstore";
import Breedstore from "../../Stores/Breedstore";
import { useContext } from "react";
import Appmodal from "./Appmodal";
import { Dropdown, FormControl } from 'react-bootstrap';
import CalendarsworkerStore from "../../Stores/CalendarsworkerStore";

const Workersearchmodal = observer((props) => {

    const uistore = useContext(UIstore)
    const calendarsworkerStore = useContext(CalendarsworkerStore);

  

    const renderModalWorkerlist = () => {
        if (props.Type === 'detail') {
            return <div
                className='d-flex flex-column  position-relative'
                style={{
                    width: '100%',
                    minHeight: '75vh',
                    backgroundColor: '#fff',
                    padding: '21px 18px'
                }}>

                <div className='d-flex flex-column'>

                    <div className='position-relative d-flex flex-row align-items-center mb-4'>
                        <img src={icon_search} className='icon-minismall position-absolute ml-3' />
                        <input type="text" onChange={handleinput} className='input-search-worker' />
                    </div>
                    <div className='list-worker'>
                        {calendarsworkerStore.workersearchdata.map((item, index) => {
                            return <div key={index}>
                                <div className='d-flex flex-row justify-content-between align-items-center'>
                                    <div className='d-flex flex-row justify-content-start align-items-center'>
                                        <Input
                                            type="checkbox"
                                            className="checkboxes-xl mr-4"
                                            id="0"
                                            onClick={() => {
                                                props.selectedWorker(item)
                                            }}
                                        />
                                        <span className='ml-1 prompt_XL text-black'>{item.name}</span>
                                    </div>
                                    <div className='img-person-default'>
                                    </div>
                                </div>
                                <hr />
                            </div>
                        })}
                    </div>
                </div>
                <div className='fixed-bottom'>
                    <div className='d-flex flex-row w-100 px-3 mt-3 mb-4 '>
                        <div onClick={() => {
                            calendarsworkerStore.Setworkerselectmodal(false)
                            props.submitWorker()
                        }} className='btn-confirm-100 d-flex justify-content-center align-items-center mr-3'>
                            <span className='prompt_XL text_white'>
                                ยืนยัน
                            </span>
                        </div>
                        <div onClick={() => {
                            calendarsworkerStore.Setworkerselectmodal(false)
                            calendarsworkerStore.removeallworkerSelectdata()
                        }} className='btn-cancel-100 d-flex justify-content-center align-items-center ml-3'>
                            <span className='prompt_XL text_dodgerblue'>
                                ยกเลิก
                            </span>
                        </div>

                    </div>
                </div>
            </div>
        }
        else {
            return <div
                className='d-flex flex-column  position-relative'
                style={{
                    width: '100%',
                    minHeight: '75vh',
                    backgroundColor: '#fff',
                    padding: '21px 18px'
                }}>

                <div className='d-flex flex-column'>

                    <div className='position-relative d-flex flex-row align-items-center mb-4'>
                        <img src={icon_search} className='icon-minismall position-absolute ml-3' />
                        <input type="text" onChange={handleinput} className='input-search-worker' />
                    </div>
                    <div className='list-worker'>
                        {calendarsworkerStore.workersearchdata.map((item, index) => {
                            return <div key={index}>
                                <div className='d-flex flex-row justify-content-between align-items-center'>
                                    <div className='d-flex flex-row justify-content-start align-items-center'>
                                        <Input
                                            type="checkbox"
                                            className="checkboxes-xl mr-4"
                                            id="0"
                                            onClick={() => {
                                                calendarsworkerStore.addworkerSelectdatacalendar(item)
                                            }}
                                        />
                                        <span className='ml-1 prompt_XL text-black'>{item.name}</span>
                                    </div>
                                    <div className='img-person-default'>
                                    </div>
                                </div>
                                <hr />
                            </div>
                        })}
                    </div>
                </div>
                <div className='fixed-bottom'>
                    <div className='d-flex flex-row w-100 px-3 mt-3 mb-4 '>
                        <div onClick={() => {
                            calendarsworkerStore.Setworkerselectmodal(false)
                            props.submitWorker()
                        }} className='btn-confirm-100 d-flex justify-content-center align-items-center mr-3'>
                            <span className='prompt_XL text_white'>
                                ยืนยัน
                            </span>
                        </div>
                        <div onClick={() => {
                            calendarsworkerStore.Setworkerselectmodal(false)
                            calendarsworkerStore.removeallworkerSelectdatacalendar()
                        }} className='btn-cancel-100 d-flex justify-content-center align-items-center ml-3'>
                            <span className='prompt_XL text_dodgerblue'>
                                ยกเลิก
                            </span>
                        </div>

                    </div>
                </div>
            </div>
        }

    }

    const handleinput = (e) => {
        calendarsworkerStore.SearchWorker(e.target.value)
        // workerlist
    }

    return (
        <div>
            <Appmodal
                visible={calendarsworkerStore.workerselectmodal}
                nocontainer={true}
                className={'d-flex justify-content-center align-items-center'}
            >
                {renderModalWorkerlist()}
            </Appmodal>

        </div>
    )
})

export default Workersearchmodal