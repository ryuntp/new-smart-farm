import React, { useState, useEffect } from "react";
import {
    Modal, ModalBody
} from "reactstrap";

import _ from 'lodash';

const Appmodal = (props) => {
    const { onRef, onBackButton, onconfirm, visible, animationType, children, nocontainer,toggle_click,className } = props
    useEffect(() => {
        if (onRef) {
            onRef(this);
        }
    });


    const onBackButtonClick = () => {
        if (onBackButton) {
            onBackButton()
        }
    }


    const renderAppmodal = () => {
        // if (!poundsNoti && !farmNoti && !hatchNoti && !boxNoti) { return }
        // const allNotis = poundsNoti.concat(farmNoti, hatchNoti, boxNoti)

        return <Modal
            isOpen={visible}
            toggle={toggle_click}
            className={className}
        >
            <div
                
                className={`${(nocontainer == false) ? "" : "modal-container"}`}
            >

                <div
                    className={`${(nocontainer == false) ? "" : "modal-subcontainer"}`}>
                    {children}
                </div>

            </div>
        </Modal>
    }

    return renderAppmodal()
}

export default Appmodal


// const Appmodal = () => {
//     return <div>Header</div>
//   };
//   export default Appmodal;