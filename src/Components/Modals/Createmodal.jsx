import React, { useState, useEffect } from "react";

import icon_noti_invent from "../../Assets/img/icon-noti-invent.png";
import icon_noti_escape from "../../Assets/img/icon-noti-escape.png";
import icon_noti_infect from "../../Assets/img/icon-noti-infect.png";
import icon_general_bento from "../../Assets/img/icon-general-bento.png";

import _ from "lodash";
import { observer } from "mobx-react-lite";
import UIstore from "../../Stores/UIstore";
import { useContext } from "react";
import Appmodal from "../Modals/Appmodal";
import Problemmodal from "../Modals/Problemmodal";
import EscapeModal from "../Modals/EscapeModal";
import InfectedModal from "../Modals/InfectedModal";
import { useHistory } from "react-router-dom";

const Createmodal = observer((props) => {
  const uistore = useContext(UIstore);
  const history = useHistory();
  return (
    <div>
      <Appmodal
        visible={uistore.Createmodal}
        toggle_click={() => {
          uistore.SetCreatemodal(!uistore.Createmodal);
        }}
        className={"d-flex justify-content-center align-items-center"}
      >
        <div className="w-100">
          <span className="prompt_XL text_white">สร้างงาน</span>
          <div
            className="card-creatework mt-3 d-flex flex-row justify-content-start align-items-center"
            onClick={() => {
              history.push(`/admin/Creatework`);
            }}
          >
            <div className="d-flex flex-column">
              <span className="prompt_XXL text_white">สร้างงาน</span>
              <span className="sarabun_L text_blueteal mt-3">มอบหมายงาน</span>
            </div>
          </div>

          <div className="mt-4">
            <span className="prompt_XL text_white ">แจ้งสัญญาณ</span>
          </div>
          <div
            onClick={() => {
              uistore.SetBreedmodal(!uistore.Breedmodal);
            }}
            className="card-alertwork mt-3 d-flex flex-row justify-content-start align-items-center"
          >
            <div className="d-flex flex-column">
              <span className="prompt_XXL text_white">พร้อมผสมพันธ์</span>
              <span className="sarabun_L text_greenteal mt-3">
                พบสัญญาณการออกไข่
              </span>
            </div>
          </div>

          <div className="mt-4">
            <span className="prompt_XL text_white ">แจ้งปัญหา</span>
          </div>

          <div
            onClick={() => {
              uistore.SetProblemmodal(!uistore.Problemmodal);
            }}
            className="card-problem-head d-flex flex-row align-items-center justify-content-between mt-3"
          >
            <div>
              <img
                src={icon_noti_invent}
                alt=""
                className="icon-default mr-3"
              />
              <span className="prompt_XL text_black">พบศัตรูจิ้งหรีด</span>
            </div>
            <div className="add-circle-orange d-flex align-items-center justify-content-center mr-3"></div>
          </div>

          <div
            onClick={() => {
              uistore.SetEscapemodal(!uistore.EscapeModal);
            }}
            className="card-problem-body d-flex flex-row align-items-center justify-content-between mt-1"
          >
            <div>
              <img
                src={icon_noti_escape}
                alt=""
                className="icon-default mr-3"
              />
              <span className="prompt_XL text_black">จิ้งหรีดหลุด</span>
            </div>
            <div className="add-circle-orange d-flex align-items-center justify-content-center mr-3"></div>
          </div>

          <div
            onClick={() => {
              uistore.SetInfectedmodal(!uistore.InfectedModal);
            }}
            className="card-problem-body d-flex flex-row align-items-center justify-content-between mt-1"
          >
            <div>
              <img
                src={icon_noti_infect}
                alt=""
                className="icon-default mr-3"
              />
              <span className="prompt_XL text_black">จิ้งหรีดติดเชื้อ</span>
            </div>
            <div className="add-circle-orange d-flex align-items-center justify-content-center mr-3"></div>
          </div>

          {/* <div className="card-problem-footer d-flex flex-row align-items-center justify-content-between mt-1">
            <div>
              <img
                src={icon_general_bento}
                alt=""
                className="icon-default mr-3"
              />
              <span className="prompt_XL text_black">อื่นๆ</span>
            </div>
            <div className="add-circle-orange d-flex align-items-center justify-content-center mr-3"></div>
          </div> */}
        </div>
      </Appmodal>
      <Problemmodal />
      <EscapeModal />
      <InfectedModal />
    </div>
  );
});

export default Createmodal;
