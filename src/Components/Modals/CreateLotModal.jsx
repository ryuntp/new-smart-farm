import React, { forwardRef, useState, useEffect } from "react";

import icon_leftarrow from "../../Assets/img/dropdown-left.png";
import icon_stagenotify from "../../Assets/img/b-stage-notify-urgent.png";
import icon_calendar from "../../Assets/img/icon-nav-calendar-blue.png";
import icon_rightarrow from "../../Assets/img/dropdown-right-white.png";
import sading from "../../Assets/img/bug-sading-01.png";
import thongdum from "../../Assets/img/bug-thongdum-04.png";
import thongdaeng from "../../Assets/img/bug-thongdaeng-01.png";
import custom from "../../Assets/img/bug-custom-01.png";
import customBreed from "../../Assets/img/custom-breed-bg.png";

import _ from "lodash";
import { observer } from "mobx-react-lite";
import UIstore from "../../Stores/UIstore";
import { useContext } from "react";
import Appmodal from "./Appmodal";
import { Dropdown } from "react-bootstrap";
import EscapeStore from "../../Stores/EscapeStore";
import icon_midelete from "../../Assets/img/mi-delete.png";
import icon_downarrow from "../../Assets/img/dropdown-down.png";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import axiosInstance from "../../Lib/AxiosInterceptor";
import Poundlist from "../../Components/Poundlist";
import Rootstore from "../../Stores/Rootstore";
import { toJS } from "mobx";

import { Input } from "reactstrap";
const CreateLotModal = observer((props) => {
  const [value, setValue] = useState("");
  const [breed, setBreed] = useState("");
  const [showpound, setShowPound] = useState(false);
  const [farmEgg, setFarmEgg] = useState(false);
  const [outsourceEgg, setOutsourceEgg] = useState(false);
  const [note, setnote] = useState("");
  const [date, setDate] = useState(new Date());
  const [parentLot, setParentLot] = useState("");
  const [outsource, setOutsource] = useState("");
  const [poundOption, setPoundOption] = useState("");
  const [newPoundError, setNewPoundError] = useState(false);
  const [isError, setIsError] = useState(false);
  const [isUpdate, SetisUpdate] = useState(false);
  const [poundlistSplitlot, setpoundlistSplitlot] = useState([
    {
      label: "ทุกวัน",
      value: "ทุกวัน",
    },
  ]);
  const rootstore = useContext(Rootstore);

  const handleCountPound = () => {
    var curPound = toJS(rootstore.datacardinfo);
    var sum = 0;
    for (var pound of curPound) {
      sum += pound.pound_name.length;
    }

    return sum;
  };

  const handleChange = () => {
    var poundlist = JSON.parse(localStorage.getItem("insectariums")) || [];

    var insectarium_name_list = [
      {
        label: "ไม่มีข้อมูล",
        value: "ไม่มีข้อมูล",
      },
    ];
    for (const key in poundlist) {
      if (Object.hasOwnProperty.call(poundlist, key)) {
        const element = poundlist[key];
        var dateElement = {
          label: element.insectarium_name,
          value: element.insectarium_name,
        };

        insectarium_name_list.push(dateElement);
      }
    }
    setpoundlistSplitlot(insectarium_name_list);
  };

  const [Breed_Arr, setBreed_arr] = useState([
    {
      label: "New York",
      value: "newYork",
    },
    {
      label: "Dublin",
      value: "dublin",
    },
    {
      label: "Istanbul",
      value: "istanbul",
    },
    {
      label: "California",
      value: "colifornia",
    },
    {
      label: "Izmir",
      value: "izmir",
    },
    {
      label: "Oslo",
      value: "oslo",
    },
  ]);
  const uistore = useContext(UIstore);
  const escapestore = useContext(EscapeStore);

  useEffect(() => {
    console.log(
      "localStorage ",
      JSON.parse(localStorage.getItem("insectariums"))
    );
    const fetch = async () => {
      //   await getNotifications();
      handleChange();
    };

    fetch();
  }, [note, parentLot, poundOption, newPoundError]);

  const CustomToggle = React.forwardRef((props, ref) => (
    <div
      className="drop-down-blue d-flex flex-row justify-content-between"
      ref={ref}
      onClick={(e) => {
        e.preventDefault();

        props.onClick(e);
      }}
    >
      {props.children}
      <img src={icon_downarrow} alt="" className="icon-mini" />
    </div>
  ));
  const CustomToggleFarmEgg = React.forwardRef((props, ref) => (
    <div
      className="drop-down-blue-max-w d-flex flex-row justify-content-between"
      ref={ref}
      onClick={(e) => {
        e.preventDefault();

        props.onClick(e);
      }}
    >
      {props.children}
      <img src={icon_downarrow} alt="" className="icon-mini" />
    </div>
  ));

  const handleEggSource = (value, type) => {
    switch (type) {
      case "infarm":
        setParentLot(value);
        setOutsource("");
        break;
      case "outsource":
        setParentLot("");
        setOutsource(value);
      default:
        break;
    }
  };

  const handleSubmit = async () => {
    let initPoundList = toJS(rootstore.datacardinfo);
    console.log("initPoundList ", initPoundList);

    if (newPoundError) return setIsError(true);

    setIsError(false);

    var packDatasend = {
      option: parseInt(poundOption),
      parentlot_id: parentLot || outsource,
      crickettype_id:
        uistore.breedName === "sading"
          ? 1
          : uistore.breedName === "thongdum"
          ? 2
          : uistore.breedName === "thongdaeng"
          ? 3
          : null,
      note,
      poundlist: [],
    };

    if (parseInt(poundOption) === 1) {
      for (const key in initPoundList) {
        if (initPoundList.hasOwnProperty.call(initPoundList, key)) {
          const element = initPoundList[key];
          console.log("element ", element);
          if (element.Error_pound === true) {
            setIsError(true);
            return;
          }
          delete element.Error_pound;
          packDatasend.poundlist.push(element);
        }
      }
    }

    console.log("packDatasend ", packDatasend);
    try {
      const createLot = await axiosInstance.post(
        `/manager/lot/createlot`,
        packDatasend
      );

      console.log("createLot ", createLot);
      if (createLot.status === 200) {
        setPoundOption("");
        setParentLot("");
        setOutsource("");
        setnote("");

        uistore.setCreateLotModal(!uistore.createLotModal);
      }
    } catch (err) {
      console.error("err jaa", err.message);
    }
  };

  const CustomToggleOutEgg = React.forwardRef((props, ref) => (
    <div
      className="drop-down-blue-max-w d-flex flex-row justify-content-between"
      ref={ref}
      onClick={(e) => {
        e.preventDefault();

        props.onClick(e);
      }}
    >
      {props.children}
      <img src={icon_downarrow} alt="" className="icon-mini" />
    </div>
  ));
  const CustomMenu = React.forwardRef((props, ref) => {
    return (
      <div
        ref={ref}
        style={props.style}
        className={props.className}
        aria-labelledby={props.labeledBy}
        onClick={() => {
          console.log(":props ;:", props.key);
        }}
      >
        {props.children}
      </div>
    );
  });
  const CustomInput = forwardRef(({ value, onClick }, ref) => (
    <div
      className="drop-downwork-blue d-flex flex-row align-items-center justify-content-between"
      onClick={onClick}
      ref={ref}
    >
      <span>{value}</span>

      <img src={icon_calendar} alt="" className="icon-mini" />
    </div>
  ));

  return (
    <div>
      <Appmodal visible={uistore.createLotModal} nocontainer={false}>
        <div className="create-lot-modal">
          <div className="head-content">
            <div
              onClick={() => {
                uistore.setCreateLotModal(!uistore.createLotModal);
              }}
              className="d-flex flex-row justify-content-center align-items-center"
              style={{
                position: "absolute",
                top: 20,
                left: 15,
              }}
            >
              <img src={icon_leftarrow} alt="" className="icon-mini" />
              <span className="sarabun_L_100 text_white">BACK</span>
            </div>

            <div className="text-content">
              <span className="prompt_XXL text_white">Create Lot</span>

              <div className="d-flex flex-row">
                <span className="prompt_L text_bluebit">สร้างล็อตใหม่</span>
              </div>
            </div>

            <div
              className="Rectangle-Copy-11"
              onClick={() => {
                uistore.setChooseBreedModal(!uistore.chooseBreedModal);
              }}
            >
              <div className="d-flex flex-row">
                <div className="col-10">
                  <div>
                    <img
                      src={
                        uistore.breedName === "sading"
                          ? sading
                          : uistore.breedName === "thongdaeng"
                          ? thongdaeng
                          : uistore.breedName === "thongdum"
                          ? thongdum
                          : sading
                      }
                      alt=""
                      className="cricket-dropdown"
                    />
                  </div>
                  <div className="dd-txt">
                    <div className="ml-2">
                      <span className="prompt_XL">
                        {uistore.breedName === "sading"
                          ? "สะดิ้ง"
                          : uistore.breedName === "thongdaeng"
                          ? "ทองแดง"
                          : uistore.breedName === "thongdum"
                          ? "ทองดำ"
                          : "สะดิ้ง"}
                      </span>
                    </div>
                    <div className="ml-2">
                      <span className="prompt_L text_dimgray">
                        {uistore?.breedName?.toUpperCase() || "SADING"}
                      </span>
                    </div>
                  </div>
                </div>
                <div class="border-cric"></div>
                <div>
                  <img
                    src={icon_downarrow}
                    alt=""
                    className="icon-mini downarrow-cric"
                  />
                </div>
              </div>
            </div>
          </div>

          <div className="d-flex justify-content-between mt-6 mx-3 mb-3">
            <span className="prompt_L">ไข่ในฟาร์ม :</span>
            <span className="prompt_L text_dimgray">
              {parentLot || outsource || "ยังไม่ได้ระบุ"}
            </span>
          </div>
          <div className="spc"></div>
          <div className="justify-content-between mx-3 mt-3">
            <span className="prompt_XL">โรงเลี้ยง&บ่อที่ลงไข่</span>
            <div className="d-flex flex-row mt-3">
              <div className="w-100">
                <div class="row mb-4">
                  <input
                    className="col-1 ml-4 align-self-center input-radio"
                    type="radio"
                    value={0}
                    name="pounds"
                    onChange={(e) => {
                      setShowPound(false);
                      setPoundOption(e.target.value);
                      console.log(e.target.value);
                    }}
                  />{" "}
                  <span className="col-10 prompt_L text_dimgray">
                    บ่อที่ว่างทั้งหมด
                  </span>
                </div>
                <div class="row mb-4">
                  <input
                    className="col-1 ml-4 align-self-center input-radio"
                    type="radio"
                    value={1}
                    name="pounds"
                    onChange={(e) => {
                      setShowPound(true);
                      setPoundOption(e.target.value);
                      console.log(e.target.value);
                    }}
                  />{" "}
                  <span className="col-10 prompt_L text_dimgray">ระบุเอง</span>
                </div>
                {showpound === true ? (
                  <Poundlist
                    poundlistSplitlot={poundlistSplitlot}
                    title={""}
                    isUpdate={isUpdate}
                    poundlistForcheck={JSON.parse(
                      localStorage.getItem("insectariums")
                    )}
                    cardinfo={[
                      {
                        insectarium_name: "",
                        pound_name: [],
                        Error_pound: false,
                      },
                    ]}
                    provider={rootstore}
                    checked={false}
                  />
                ) : null}
              </div>
            </div>
          </div>
          <div className="spc"></div>
          {/* <div className="d-flex mx-3 mt-3 mb-3">
            <div className="col-6">
              <span className="prompt_XL">วันที่เริ่มต้น&เวลา</span>
            </div>
          </div> */}
          {/* <div className="d-flex mx-3 mb-3">
            <div className="col-12">
              <div>
                <div className="d-flex flex-row">
                  <div className="w-100 mx-3">
                    <DatePicker
                      selected={new Date(date)}
                      timeFormat="HH:mm"
                      dateFormat="Pp"
                      timeIntervals={15}
                      showTimeSelect
                      onChange={(date) => {
                        setDate(date.toISOString());
                      }}
                      customInput={<CustomInput />}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div> */}
          <div className="spc"></div>
          <div className="d-flex mx-3 mt-3 mb-2 justify-content-between">
            <span className="prompt_XL">หมายเหตุ</span>
            <div
              className="add-circle-darkturquoise"
              onClick={() => {
                uistore.SetAddnoteCreateLotModal(
                  !uistore.addnoteCreateLotmodal
                );
              }}
            >
              <span className="text_white"></span>
            </div>
          </div>
          <div className="ml-3">
            <span className="text_black">{note ? note : null}</span>
          </div>
          <div className="d-flex flex-row mx-4 justify-content-between mt-4">
            <div
              onClick={() => {
                uistore.setCreateLotModal(!uistore.createLotModal);
              }}
              className="btn-minicancle d-flex justify-content-center align-items-center mr-1"
            >
              <span className="prompt_XL text_dodgerblue">กลับ</span>
            </div>
            <div
              onClick={async () => {
                await handleSubmit();
              }}
              className="d-flex justify-content-center align-items-center btn-miniconfirm ml-1"
            >
              <span className="prompt_XL text_white">ยืนยัน</span>
            </div>
          </div>
        </div>
      </Appmodal>

      {/* add problem */}

      <Appmodal
        visible={uistore.chooseBreedModal}
        toggle_click={() => {
          uistore.setChooseBreedModal(!uistore.chooseBreedModal);
        }}
        nocontainer={true}
        className={"d-flex justify-content-center align-items-center"}
      >
        <div
          className="d-flex flex-column justify-content-between"
          style={{
            width: "100%",
            minHeight: 600,
            backgroundColor: "#fff",
            padding: 17,
          }}
        >
          <div className="d-flex flex-column">
            <span className="prompt_XL text_lightblueheader">STEP 1</span>
            <div className="position-relative mt-1 "></div>
            <span className="mt-3 mb-3 text_blueteal prompt_XXL">
              เลือกพันธ์ที่ต้องการ
            </span>

            <div
              className="d-flex flex-row tile-bug-select"
              onClick={() => {
                uistore.setStep2Modal(!uistore.step2Modal, "sading");
              }}
            >
              <div className="col-12 d-flex">
                <img src={sading} alt="" className="cric-img" />
                <div className="dd-txt align-self-center">
                  <div className="ml-2">
                    <span className="prompt_XL">สะดิ้ง</span>
                  </div>
                  <div className="ml-2">
                    <span className="prompt_L text_dimgray">SADING</span>
                  </div>
                </div>
              </div>
            </div>
            <div
              className="d-flex flex-row tile-bug-selects"
              onClick={() => {
                uistore.setStep2Modal(!uistore.step2Modal, "thongdum");
              }}
            >
              <div className="col-12 d-flex">
                <img src={thongdum} alt="" className="cric-img" />
                <div className="dd-txt align-self-center">
                  <div className="ml-2">
                    <span className="prompt_XL">ทองดำ</span>
                  </div>
                  <div className="ml-2">
                    <span className="prompt_L text_dimgray">THONGDUM</span>
                  </div>
                </div>
              </div>
            </div>
            <div
              className="d-flex flex-row tile-bug-selects"
              onClick={() => {
                uistore.setStep2Modal(!uistore.step2Modal, "thongdaeng");
              }}
            >
              <div className="col-12 d-flex">
                <img src={thongdaeng} alt="" className="cric-img" />
                <div className="dd-txt align-self-center">
                  <div className="ml-2">
                    <span className="prompt_XL">ทองแดง</span>
                  </div>
                  <div className="ml-2">
                    <span className="prompt_L text_dimgray">THONGDAENG</span>
                  </div>
                </div>
              </div>
            </div>
            {/* <div
              className="d-flex flex-row tile-bug-selects"
              // onClick={() => {
              //     uistore.setStep2Modal(!uistore.step2Modal, "Sading");
              //   }}
            >
              <div className="col-12 d-flex">
                <img src={custom} alt="" className="cric-img" />
                <div className="dd-txt align-self-center">
                  <div className="ml-2">
                    <span className="prompt_XL">ทองเหลือง</span>
                  </div>
                  <div className="ml-2">
                    <span className="prompt_L text_dimgray">CUSTOM</span>
                  </div>
                </div>
              </div>
            </div> */}
          </div>

          {/* <div>
            <div
              onClick={() => {
                uistore.setAddNewBreedModal(!uistore.addNewBreedModal);
              }}
              className="d-flex flex-row align-items-center justify-content-center py-3 pb-4"
            >
              <span className="text_black prompt_XL">
                เพิ่มแมลงสายพันธ์อื่น
              </span>
              <div className="ml-2 add-circle-darkturquoise">
                <span className="text_white"></span>
              </div>
            </div>
          </div> */}
        </div>
      </Appmodal>
      <Appmodal
        visible={uistore.step2Modal}
        toggle_click={() => {
          uistore.setStep2Modal(!uistore.step2Modal);
        }}
        nocontainer={true}
        className={"d-flex justify-content-center align-items-center"}
      >
        <div
          className="d-flex flex-column justify-content-between"
          style={{
            width: "100%",
            minHeight: 600,
            backgroundColor: "#fff",
            padding: 17,
          }}
        >
          <div className="d-flex flex-column">
            <span className="prompt_XL text_lightblueheader">STEP 2</span>
            <div className="position-relative mt-1"></div>
            <span className="mt-3 mb-3 text_blueteal prompt_XXL">
              ล็อตไข่ที่ต้องการลง
            </span>

            <div className="d-flex flex-row">
              <div className="w-100">
                <div class="row mb-4">
                  <input
                    className="col-2 align-self-center input-radio"
                    type="radio"
                    value="farm"
                    name="eggtype"
                    onChange={(e) => {
                      setFarmEgg(true);
                      setOutsourceEgg(false);
                      console.log(e.target.value);
                    }}
                  />{" "}
                  <span className="col-10 prompt_L text_dimgray">
                    ไข่ในฟาร์ม
                  </span>
                </div>
                {farmEgg === true ? (
                  <div className="mx-3 mt-3 mb-3">
                    <Dropdown
                      onSelect={(e) => {
                        console.log("e", e);
                        handleEggSource(e, "infarm");
                      }}
                    >
                      <Dropdown.Toggle
                        as={CustomToggleFarmEgg}
                        id="dropdown-custom-components"
                      >
                        <span className="sarabun_L_300 text_black">
                          {parentLot || "โปรดระบุ"}
                        </span>
                      </Dropdown.Toggle>
                      <Dropdown.Menu as={CustomMenu}>
                        {Breed_Arr.map((item) => {
                          return (
                            <Dropdown.Item
                              key={item.label}
                              eventKey={item.label}
                            >
                              {item.value}
                            </Dropdown.Item>
                          );
                        })}
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                ) : null}
                <div class="row mb-4">
                  <input
                    className="col-2 align-self-center input-radio"
                    type="radio"
                    value="outsource"
                    name="eggtype"
                    onChange={(e) => {
                      setFarmEgg(false);
                      setOutsourceEgg(true);
                      console.log(e.target.value);
                    }}
                  />{" "}
                  <span className="col-10 prompt_L text_dimgray">
                    ซื้อไข่จากนอกฟาร์ม
                  </span>
                </div>
                {outsourceEgg === true ? (
                  <div className="mx-3 mt-3 mb-3">
                    <Input
                      className="input-breed w-100"
                      type="text"
                      placeholder="โปรดระบุ"
                      onChange={(e) =>
                        handleEggSource(e.target.value, "outsource")
                      }
                    />
                  </div>
                ) : null}
              </div>
            </div>
          </div>

          <div className="d-flex flex-row mx-4 justify-content-between mt-4">
            <div
              onClick={() => {
                uistore.setStep2Modal(!uistore.step2Modal);
              }}
              className="btn-minicancle d-flex justify-content-center align-items-center mr-1"
            >
              <span className="prompt_XL text_dodgerblue">กลับ</span>
            </div>
            <div
              onClick={() => {
                uistore.setStep2Modal(!uistore.step2Modal);
                uistore.setChooseBreedModal(!uistore.chooseBreedModal);
                setFarmEgg(false);
                setOutsourceEgg(false);
              }}
              className="btn-miniconfirm ml-1 d-flex justify-content-center align-items-center"
            >
              <span className="prompt_XL text_white">ยืนยัน</span>
            </div>
          </div>
        </div>
      </Appmodal>
      <Appmodal
        visible={uistore.addNewBreedModal}
        toggle_click={() => {
          uistore.setAddNewBreedModal(!uistore.addNewBreedModal);
        }}
        nocontainer={true}
        className={"d-flex justify-content-center align-items-center"}
      >
        <div
          className="d-flex flex-column "
          style={{
            width: "100%",
            minHeight: 600,
            backgroundColor: "#fff",
          }}
        >
          <div className="note-headmodal d-flex align-items-center w-100">
            <div
              onClick={() => {
                uistore.setAddNewBreedModal(!uistore.addNewBreedModal);
              }}
              className="d-flex flex-row justify-content-center align-items-center"
            >
              <img src={icon_leftarrow} alt="" className="icon-mini" />
              <span className="sarabun_L_100 text_black">BACK</span>
            </div>
          </div>
          <div className="h-100 w-100">
            <img src={customBreed} alt="" className="custom-breed-bg" />
          </div>

          <div className="px-4 mt-4">
            <div className="d-flex flex-row justify-content-between align-items-center">
              <span className="prompt_XL text_black">ชื่อสายพันธ์</span>
            </div>
          </div>

          <div className="mx-3 mt-3">
            <Input
              //   name="breed"
              className="input-breed-name"
              value={breed}
              placeholder="โปรดระบุชื่อสายพันธ์"
              type="text"
              onChange={(e) => setBreed(e.target.value)}
            />
          </div>

          <div className="btn-bottom">
            <div
              onClick={() => {
                escapestore.setNote(breed);
                uistore.SetaddNoteEscapeModal(!uistore.addEscapeNoteModal);
              }}
              className="btn-bigconfirm"
            >
              <span className="prompt_XL text_white">+ สร้างเลย</span>
            </div>
          </div>
        </div>
      </Appmodal>
      {/* Check duplicate data */}
      <Appmodal
        visible={uistore.duplicateEscapemodal}
        toggle_click={() => {
          uistore.SetduplicateEscapemodal(!uistore.duplicateEscapemodal);
        }}
        nocontainer={true}
        className={"d-flex justify-content-center align-items-center"}
      >
        <div
          className="d-flex flex-column align-items-center"
          style={{
            width: "100%",
            minHeight: 600,
            backgroundColor: "#fff",
          }}
        >
          <div className="head-duplicate-problemmodal" />
          <div className="px-4 d-flex flex-column">
            <span
              className="prompt_XXL text_orangefatbg mt-4 text-center"
              style={{
                lineHeight: 1.13,
              }}
            >
              บ่อเลี้ยงที่คุณแจ้งบางบ่อ มีคนแจ้งปัญหาแล้ว
            </span>

            <span className="sarabun_L_300 text_dimgray my-3 mb-4 text-center">
              เราจะนำเลขบ่อที่คุณแจ้งซ้ำ
              ออกจากรายการแจ้งเตือนของคุณให้อัตนโนมัติ
            </span>

            <div className="card-mention-orange d-flex flex-column">
              <span className="prompt_XL text_black mb-2">รายการซ้ำ</span>

              <div className="d-flex flex-row align-items-center">
                <span className="sarabun_M text_black">โรงเลี้ยง 01 : </span>
                <span className="sarabun_M_300 text_black ml-1">
                  {" "}
                  101-103,109
                </span>
              </div>
            </div>

            <div
              onClick={() => {
                uistore.SetduplicateEscapemodal(!uistore.duplicateEscapemodal);
              }}
              className="btn-confirm-100 d-flex justify-content-center align-items-center"
              style={{
                marginTop: -10,
              }}
            >
              <span className="prompt_XL text_white">
                ยืนยัน...เอาบ่อซ้ำออก
              </span>
            </div>
          </div>
        </div>
      </Appmodal>
      {/* add note */}
      <Appmodal
        visible={uistore.addnoteCreateLotmodal}
        toggle_click={() => {
          uistore.SetAddnoteCreateLotModal(!uistore.addnoteCreateLotmodal);
        }}
        nocontainer={true}
        className={"d-flex justify-content-center align-items-center"}
      >
        <div
          className="d-flex flex-column "
          style={{
            width: "100%",
            minHeight: 600,
            backgroundColor: "#fff",
          }}
        >
          <div className="note-headmodal d-flex align-items-center w-100">
            <div
              onClick={() => {
                uistore.SetAddnoteCreateLotModal(
                  !uistore.addnoteCreateLotmodal
                );
              }}
              className="d-flex flex-row justify-content-center align-items-center"
            >
              <img src={icon_leftarrow} alt="" className="icon-mini" />
              <span className="sarabun_L_100 text_black">BACK</span>
            </div>
          </div>

          <div className="px-4 mt-4">
            <div className="d-flex flex-row justify-content-between align-items-center">
              <span className="prompt_XL text_black">หมายเหตุ</span>

              <div
                onClick={() => {
                  setnote(null);
                  uistore.SetAddnoteCreateLotModal(
                    !uistore.addnoteCreateLotmodal
                  );
                }}
              >
                <img src={icon_midelete} className="icon-minismall mr-3" />
                <span className="prompt_M_200 text-decoration-underline text_tomato">
                  ลบ
                </span>
              </div>
            </div>
          </div>

          <div className="mx-4 mt-4">
            <textarea
              // /breedstore.notebreed
              value={note}
              onChange={(e) => {
                // console.log("e",);
                setnote(e.target.value);
              }}
              className="input-area-addnote"
              id="w3review"
              name="w3review"
              rows="4"
              cols="50"
            ></textarea>
          </div>

          <div className="d-flex flex-row mx-4 justify-content-between mt-4">
            <div
              onClick={() => {
                setnote(null);
                uistore.SetAddnoteCreateLotModal(
                  !uistore.addnoteCreateLotmodal
                );
              }}
              className="btn-minicancle d-flex justify-content-center align-items-center"
            >
              <span className="prompt_XL text_dodgerblue">ยกเลิก</span>
            </div>
            <div
              onClick={() => {
                uistore.SetAddnoteCreateLotModal(
                  !uistore.addnoteCreateLotmodal
                );
              }}
              className="btn-miniconfirm d-flex justify-content-center align-items-center"
            >
              <span className="prompt_XL text_white">ยืนยัน</span>
            </div>
          </div>
        </div>
      </Appmodal>
    </div>
  );
});

export default CreateLotModal;
