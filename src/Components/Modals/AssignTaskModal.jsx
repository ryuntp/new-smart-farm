import React, { useState, useEffect } from "react";

import icon_leftarrow from "../../Assets/img/dropdown-left.png";
import icon_stagenotify from "../../Assets/img/b-stage-notify-urgent.png";

import icon_generalhistory from "../../Assets/img/icon-general-history.png";
import icon_rightarrow from "../../Assets/img/dropdown-right.png";

import _ from "lodash";
import { observer } from "mobx-react-lite";
import UIstore from "../../Stores/UIstore";
import { useContext } from "react";
import Appmodal from "./Appmodal";
import { Dropdown } from "react-bootstrap";

import EscapeStore from "../../Stores/EscapeStore";
import icon_downarrow from "../../Assets/img/dropdown-down.png";
import CreateLotModal from "../Modals/CreateLotModal";

const assignTaskModal = observer((props) => {
  const [value, setValue] = useState("");
  const [note, setnote] = useState("");

  const [Breed_Arr, setBreed_arr] = useState([
    {
      label: "New York",
      value: "newYork",
    },
    {
      label: "Dublin",
      value: "dublin",
    },
    {
      label: "Istanbul",
      value: "istanbul",
    },
    {
      label: "California",
      value: "colifornia",
    },
    {
      label: "Izmir",
      value: "izmir",
    },
    {
      label: "Oslo",
      value: "oslo",
    },
  ]);

  const CustomToggle = React.forwardRef((props, ref) => (
    <div
      className="drop-down-blue d-flex flex-row justify-content-between"
      ref={ref}
      onClick={(e) => {
        e.preventDefault();

        props.onClick(e);
      }}
    >
      {props.children}
      <img src={icon_downarrow} alt="" className="icon-mini" />
    </div>
  ));
  const CustomMenu = React.forwardRef((props, ref) => {
    return (
      <div
        ref={ref}
        style={props.style}
        className={props.className}
        aria-labelledby={props.labeledBy}
        onClick={() => {
          console.log(":props ;:", props.key);
        }}
      >
        {props.children}
      </div>
    );
  });
  const uistore = useContext(UIstore);
  const escapestore = useContext(EscapeStore);

  return (
    <div>
      <Appmodal visible={uistore.assignTaskModal} nocontainer={false}>
        <div className="assign-task-modal">
          <div className="head-content">
            <div
              onClick={() => {
                uistore.setAssignTaskModal(!uistore.assignTaskModal);
              }}
              className="d-flex flex-row justify-content-center align-items-center"
              style={{
                position: "absolute",
                top: 20,
                left: 15,
              }}
            >
              <img src={icon_leftarrow} alt="" className="icon-mini" />
              <span className="sarabun_L_100 text_dimgray">BACK</span>
            </div>

            <div className="text-content">
              <span className="prompt_XXL text_blueteal">มอบหมายงานใหม่</span>

              <div className="mt-2 justify-content-center align-items-center">
                <span className="prompt_XL text_dimgray">
                  กรุณาเลือกรูปแบบงาน
                </span>
              </div>
              <div className="justify-content-center align-items-center">
                <span className="prompt_XL text_dimgray">
                  ที่ต้องการมอบหมาย
                </span>
              </div>
            </div>
          </div>

          <div className="d-flex justify-content-center align-items-center d-flex flex-column mt-2">
            <div
              onClick={() => {
                uistore.SetBreedmodal(!uistore.Breedmodal);
              }}
              className="card-assign mt-3 d-flex flex-row justify-content-end align-items-center"
            >
              <div className="d-flex flex-column mr-5">
                <span className="prompt_XL text_black">งานทั่วไป</span>
                <span className="sarabun_M text_bluebit">
                  งานเพาะเลี้ยง/ประจำวัน
                </span>
              </div>
            </div>
          </div>
          <div className="d-flex justify-content-center align-items-center d-flex flex-column mt-3">
            <div
              onClick={() => {
                uistore.setCreateLotModal(!uistore.createLotModal);
              }}
              className="card-create-lot mt-3 d-flex flex-row justify-content-end align-items-center"
            >
              <div className="d-flex flex-column mr-5">
                <span className="prompt_XL text_black">ลงลูกจิ้งหรีด</span>
                <span className="sarabun_M text_bluebit">
                  ลงลูกจิ้งหรีด/สร้างล็อตใหม่
                </span>
              </div>
            </div>
          </div>
        </div>
      </Appmodal>

      {/* create lot modal */}
      <CreateLotModal />
    </div>
  );
});

export default assignTaskModal;
