import { observer } from "mobx-react";
import { useState, useContext, useEffect } from "react";
import BounceLoader from "react-spinners/BounceLoader";
import UIstore from "../../Stores/UIstore";
import icon_lottask from '../../Assets/img/icon-nav-lot-task-gray.png'
import iconnav_calendar from '../../Assets/img/icon-nav-calendar-gray.png'
import iconnav_calendarblue from '../../Assets/img/icon-nav-calendar-blue.png'
import iconnav_dashboard from '../../Assets/img/icon-nav-dashboard.png'
import iconnav_dashboardblue from '../../Assets/img/icon-nav-dashboard-blue.png'
import iconnav_document from '../../Assets/img/icon-nav-document-gray.png'
import iconnav_documentblue from '../../Assets/img/icon-nav-document-blue.png'
import iconnav_lottaskblue from '../../Assets/img/icon-nav-lot-task-blue.png'
import { useHistory, useLocation } from "react-router-dom";
// Can be a string as well. Need to ensure each key-value pair ends with ;
export const Footer = observer((props) => {


    const uistore = useContext(UIstore);
    let location = useLocation("");
    const history = useHistory();
    const [Activebar, setActivebar] = useState('')
    const [locationKeys, setLocationKeys] = useState([])

    const pushPage = (page) => {
        // console.log('clickInfo', clickInfo);
        history.push({ pathname: `/admin/${page}` });
        history.go('d');

    }

    useEffect(() => {
        return history.listen(location => {
            console.log("location", location);
            let location_without = location.pathname.replace(/admin/g, "");
            let pagename = location_without.replace(/\//g, '')
            setActivebar(pagename)
            if (history.action === 'PUSH') {
                setLocationKeys([location.key])
            }
            if (history.action === 'POP') {
                if (locationKeys[1] === location.key) {
                    setLocationKeys(([_, ...keys]) => keys)
                    // Handle forward event
                } else {
                    setLocationKeys((keys) => [location.key, ...keys])
                    // Handle back event
                }
            }
        })
    }, [history, locationKeys])


    useEffect(() => {
        let location_without = location.pathname.replace(/admin/g, "");
        let pagename = location_without.replace(/\//g, '')
        setActivebar(pagename)
        console.log("pagename :: ", pagename);

    }, []);

    const RenderFooter = () => {
        if (!uistore.EnableFooter) {
            return
        }
        return <div className="footer-container">

            {Activebar === 'HomeDash' ? <div className="w-100 border-btm">
                <img src={iconnav_dashboardblue} className='icon-default mr-2 mb-2' alt="" />
            </div> : <div onClick={() => {
                pushPage('HomeDash')
            }} className="w-100">
                <img src={iconnav_dashboard} className='icon-default mr-2 mb-2' alt="" />
            </div>}


            {Activebar === 'Worklotmanage' ? <div className="w-100 border-btm">
                <img src={iconnav_lottaskblue} className='icon-default mr-2 mb-2' alt="" />
            </div> : <div onClick={() => {
                pushPage('Worklotmanage')
            }} className="w-100">
                <img src={icon_lottask} className='icon-default mr-2 mb-2' alt="" />
            </div>}

            {Activebar === 'Calendarworker'
                ? <div className="w-100 border-btm">
                    <img src={iconnav_calendarblue} className='icon-default mr-2 mb-2' alt="" />
                </div>
                : <div onClick={() => {
                    pushPage('Calendarworker')
                }} className="w-100">
                    <img src={iconnav_calendar} className='icon-default mr-2 mb-2' alt="" />
                </div>}


            {Activebar === 'HomeMenu' ? <div className="w-100 border-btm">
                <img src={iconnav_documentblue} className='icon-default mr-2 mb-2' alt="" />
            </div> : <div onClick={() => {
                pushPage('HomeMenu')
            }} className="w-100">
                <img src={iconnav_document} className='icon-default mr-2 mb-2' alt="" />
            </div>}



        </div>
    }

    return (
        <div>
            {RenderFooter()}
        </div>
    );



})



export default Footer;






