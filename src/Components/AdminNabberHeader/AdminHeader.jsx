import { observer } from "mobx-react-lite";
import React, { useContext, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import UIstore from "../../Stores/UIstore";
// import Cookies from "js-cookie";
// = observer((props) =>
const AdminHeader = observer((props) => {
    let location = useLocation('')
    const uistore = useContext(UIstore);

    const [currentpage, setcurrentpage] = useState('')
    const [currentpageauth, setcurrentpageauth] = useState("");

    useEffect(() => {
        let location_without = location.pathname.replace(/admin/g, "");
        let pagename = location_without.replace(/\//g, '')

        let location_auth = location.pathname.replace(/auth/g, "");
        let pagenameauth = location_auth.replace(/\//g, "");

        setcurrentpage(pagename)

        setcurrentpageauth(pagenameauth)
    }, []);

    const { active } = props

    if (uistore.EnablesubHeader == false) {
        return (<div> </div>)
    }
    if (active) {
        return (
            <div className="row container-header">
                <div className="col overview-menu-active d-flex align-items-center justify-content-center w-100" >
                    {/* <a href={`/admin/farm?farm_id=${Cookies.get('farmId')}`}> */}
                    <a href={`/admin/homedash`}>
                        <p className='overview-text'>ภาพรวม</p>
                    </a>
                </div>
                <div className="col managework-menu-active"
                >
                    <a href="/admin/calendar">
                        <p className='managework-text-active'>จัดการงาน</p>
                    </a>
                </div>
            </div>
        )
    }
    return (
        <div className="row container-header">
            <div className="col overview-menu-active" >
                {/* <a href={`/admin/farm?farm_id=${Cookies.get('farmId')}`}> */}
                <a href={`/admin/homedash`}>
                    <p className="overview-text-active">ภาพรวม</p>
                </a>
            </div>
            <div className="col managework-menu" >
                <a href="/admin/calendar">
                    <p className="managework-text">จัดการงาน</p>
                </a>
            </div>
        </div>
    )
})

export default AdminHeader


