import { observer } from "mobx-react";
import { useState, useContext } from "react";
import BounceLoader from "react-spinners/BounceLoader";
import UIstore from "../Stores/UIstore";

// Can be a string as well. Need to ensure each key-value pair ends with ;
export const Loader = observer((props) => {


    const uistore = useContext(UIstore);
    let [loading, setLoading] = useState(true);
    let [color, setColor] = useState("#005096");

    return (
        <div>
            {uistore.Isloading ? <div className="loader-content">

                <BounceLoader
                    color={color} loading={true} size={100} />
            </div> : null}

        </div>
    );



})



export default Loader;






