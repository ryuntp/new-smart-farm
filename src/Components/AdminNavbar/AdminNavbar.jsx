/*!
=========================================================
* Paper Dashboard PRO React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

// import routes from "routes.js";
import React, { useContext, useEffect, useState } from "react";
import axios from "axios";
import classnames from "classnames";
import { observer } from "mobx-react-lite";
import {
  Button,
  Collapse,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  NavbarBrand,
  Navbar,
  NavItem,
  Card,
  CardBody,
  Nav,
  Container,
  Popover,
  Badge,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import { NavLink, useLocation } from "react-router-dom";

// import Calendars from "react-calendar";
import Notification from "../../View/Notification/Notificaiton";
import NotificaitonCalendar from "../../View/Notification/NotificaitonCalendar";
// import Cookies from "js-cookie"
import Appmodal from "../../Components/Modals/Appmodal";
import UIstore from "../../Stores/UIstore";
import Createmodal from "../Modals/Createmodal";
import Problemmodal from "../Modals/Problemmodal";
import Breedmodal from "../Modals/Breedmodal";
const URL = "https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com";

// @inject('stores')
// @observer

export const AdminNavbar = observer((props) => {
  const uistore = useContext(UIstore);
  let location = useLocation("");

  // const todostore = useContext(Todostore)

  // const year = date.getFullYear() + 543
  const month = [];
  month[0] = "มกราคม";
  month[1] = "กุมภาพันธ์";
  month[2] = "มีนาคม";
  month[3] = "เมษายน";
  month[4] = "พฤษภาคม";
  month[5] = "มิถุนายน";
  month[6] = "กรกฏาคม";
  month[7] = "สืิงหาคม";
  month[8] = "กันยายน";
  month[9] = "ตุลาคม";
  month[10] = "พฤศจิกายน";
  month[11] = "ธันวาคม";

  const [date, setDate] = useState(null);
  const [hatchNoti, sethatchNoti] = useState(null);
  const [boxNoti, setboxNoti] = useState(null);
  const [dataNoti, setdataNoti] = useState(null);
  const [notiCountShow, setnotiCountShow] = useState(null);
  const [poundsNoti, setpoundsNoti] = useState(null);
  const [fouronenoti, setfouronenoti] = useState(null);
  const [farmNoti, setfarmNoti] = useState(null);
  const [sevennoti, setsevennoti] = useState(null);
  const [fourfournoti, setfourfournoti] = useState(null);
  const [collapseOpen, setcollapseOpen] = useState(false);
  const [signOut, setsignOut] = useState(null);
  const [popoverOpen, setpopoverOpen] = useState(false);
  const [popoverNoti, setpopoverNoti] = useState(false);
  const [currentpage, setcurrentpage] = useState("");
  const [currentpageauth, setcurrentpageauth] = useState("");
  
  const sendtime = async (e) => {
    await setDate(e);

    props.parentCallback(date);
    console.log(date);
  };

  useEffect(() => {
    let location_without = location.pathname.replace(/admin/g, "");
    let pagename = location_without.replace(/\//g, '')


    let location_withoutauth = location.pathname.replace(/auth/g, "");
    let pagenameauth = location_withoutauth.replace(/\//g, "");



    setcurrentpage(pagename);
    setcurrentpageauth(pagenameauth)
    const currentDate = new Date();
    const data =
      ("0" + (currentDate.getMonth() + 1)).slice(-2) +
      "-" +
      ("0" + currentDate.getDate()).slice(-2) +
      "-" +
      currentDate.getFullYear();
   
    // axios.get(`${URL}/allactv2/${data}/${Cookies.get('farmId')}`, { params: data })
    //   .then(res => {
    //     this.setState({
    //       taskLists: res.data
    //     })
    //   })
    // console.log("this.store : ", this.store)
    // if (this.state.taskLists[0]){
    //    axios.get(`${URL}/poundlistass/${this.state.taskLists[0].assigned_id}`,{params: data})
    //   .then(res => {
    //      console.log(res.data)
    //      this.setState({
    //       pound :res.data
    //     })
    //     console.log(this.state.pound)
    //   })
    // }
  }, []);

  return (
    <div>
      {uistore.EnableHeader === false ? (
        <div></div>
      ) : (
        <div className="navbar">
          <div 
          // className="navbar-container"
          className={`navbar-container  ${uistore.Headercolor === 'blue' ? "bg-dodgerblue" : "bg-white"}`}
          >
           
            <div className='d-flex'>
              <NavbarBrand href="#pablo" onClick={(e) => e.preventDefault()}>
                {uistore.EnableHeader === true ? (
                  <div className="logo-container">
                    <p className="logo-text">ACHETHAI</p>
                    <p className="version-text">Beta 1.0.01</p>
                  </div>
                ) : null}
              </NavbarBrand>
            </div>

            {uistore.EnableHeader === true ? null : (
              <div className="mt-5">
                <span
                  style={{
                    fontFamily: "IBM Plex Sans Thai",
                    color: "#ffffff",
                    fontSize: "16px",
                    fontWeight: "bold",
                    width: "100%",
                  }}
                >
                  {currentpage}
                </span>
              </div>
            )}

            {uistore.EnableHeader === true  ? (
              <div className="right-menu">
                <div
                  className={
                    uistore.EnableHeader === true 
                      ? "text-white"
                      : "text-white "
                  }
                  style={{ zIndex: "100" }}
                  id="notiPopOver"
                >
                  <i className="far fa-bell fa-2x noti"></i>

                  {poundsNoti || farmNoti || hatchNoti || boxNoti ? (
                    <div>
                      <Badge
                        pill
                        color="danger"
                        className="ml-3"
                        style={{
                          marginTop: "-40px",
                          position: "absolute",
                          display: notiCountShow ? "flex" : "none",
                        }}
                      >
                        {poundsNoti.length +
                          farmNoti.length +
                          hatchNoti.length +
                          boxNoti.length}
                      </Badge>
                    </div>
                  ) : null}

                  <Popover
                    placement="bottom"
                    isOpen={popoverNoti}
                    target="notiPopOver"
                    popperClassName={"card-width100"}
                    style={{
                      backgroundColor: "lightgrey",
                      textAlign: "left",
                      width: "100%",
                    }}
                    toggle={() => {
                      setpopoverNoti(!popoverNoti);
                      console.log("popoverNoti :: ", popoverNoti);
                      // setnotiCountShow(false)
                    }}
                  >
                    <Notification
                      pageName={currentpage}
                      poundsNoti={poundsNoti}
                      farmNoti={farmNoti}
                      hatchNoti={hatchNoti}
                      boxNoti={boxNoti}
                    />
                  </Popover>
                </div>

                <div>
                  <i className="fas fa-bars fa-2x menu"></i>
                </div>
              </div>
            ) : null}

            {currentpage == "งานที่ต้องทำ" ? (
              <div
                className={"mt-5 text-white position-absolute"}
                style={{
                  position: "absolute",
                  left: "85%",
                  right: 0,
                  top: 0,
                  bottom: 0,
                }}
                id="calendarPopOver"
              >
                <i className="fas fa-calendar-alt fa-2x"></i>

                <Popover
                  placement="bottom"
                  isOpen={popoverOpen}
                  target="calendarPopOver"
                  toggle={this.togglePopOver}
                >
                  {/* <Calendars locale={"th"} className={"border-0"}
          locale={'th'}
          className={'border-0'}
          onChange={async (e) => {
            await setDate(e)
            await sendtime(e)
          }}
          value={date} /> */}
                </Popover>
              </div>
            ) : null}

            {/*CalendarNoti*/}

            {currentpage === "Calendar" ||
              currentpage === "ปฏิทินมอบหมายงาน" ? (
              <div>
                <div
                  className={
                    currentpage === "Calendar"
                      ? "mt-4 text-white"
                      : "mt-5 text-white position-absolute"
                  }
                  style={{
                    position: "absolute",
                    left: "85%",
                    right: 0,
                    top: 0,
                    bottom: 0,
                  }}
                  id="notiPopOver"
                >
                  <i className="far fa-bell fa-2x"></i>
                  {sevennoti || fouronenoti || fourfournoti ? (
                    <div>
                      <Badge
                        pill
                        color="danger"
                        className="ml-3"
                        style={{
                          marginTop: "-40px",
                          position: "absolute",
                          display: notiCountShow ? "flex" : "none",
                        }}
                      >
                        {fouronenoti.length +
                          sevennoti.length +
                          fourfournoti.length}
                      </Badge>
                    </div>
                  ) : null}

                  <Popover
                    placement="bottom"
                    isOpen={popoverNoti}
                    target="notiPopOver"
                    toggle={() => {
                      setpopoverNoti(!popoverNoti);
                      setnotiCountShow(false);
                    }}
                  >
                    <NotificaitonCalendar
                      pageName={currentpage}
                      fourfournoti={this.state.fourfournoti}
                      fouronenoti={this.state.fouronenoti}
                      sevennoti={this.state.sevennoti}
                    />
                  </Popover>
                </div>
                <a href={`/admin/export`}>
                  <div
                    className={
                      currentpage === "Calendar"
                        ? "mt-4 text-white"
                        : "mt-5 text-white position-absolute"
                    }
                    style={{
                      position: "absolute",
                      left: "10%",
                      right: 0,
                      top: 0,
                      bottom: 0,
                    }}
                    id="fmcl"
                  >
                    <i className="far fa-file-alt fa-2x"></i>
                  </div>
                </a>
              </div>
            ) : null}

            <Collapse
              className="justify-content-end"
              navbar
              isOpen={collapseOpen}
            >
              <Nav navbar>
                <UncontrolledDropdown className="btn-rotate" nav>
                  <DropdownToggle
                    aria-haspopup={true}
                    caret
                    color="default"
                    data-toggle="dropdown"
                    id="navbarDropdownMenuLink"
                    nav
                  >
                    <i className="nc-icon nc-single-02" />
                    <p>
                      <span className="d-lg-none d-md-block">Some Actions</span>
                    </p>
                  </DropdownToggle>
                  <DropdownMenu aria-labelledby="navbarDropdownMenuLink" right>
                    <NavLink to="/admin/user-profile">
                      <DropdownItem href="http://localhost:3000/admin/user-profile">
                        ข้อมูลผู้ใช้งาน
                      </DropdownItem>
                    </NavLink>
                    <NavLink to="/auth/login">
                      <DropdownItem>ออกจากระบบ</DropdownItem>
                    </NavLink>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </Nav>
            </Collapse>
          </div>
          <Modal
            isOpen={signOut}
            style={{ maxHeight: "200px" }}
            className="custom-modal-style"
          >
            <ModalHeader className="">
              <div className="d-flex justify-content-center">
                <h5
                  className="text-center"
                  style={{
                    color: "black",
                    fontSize: "14px",
                    fontFamily: "IBM Plex Sans Thai",
                    fontWeight: "bold",
                  }}
                >
                  {" "}
                  ต้องการออกจากระบบ ?{" "}
                </h5>
                <i
                  style={{ position: "absolute", right: "5%" }}
                  className="far fa-times-circle fa-2x text-danger"
                  onClick={() => this.setState({ signOut: false })}
                ></i>
              </div>
            </ModalHeader>
            <ModalFooter className="justify-content-center">
              <a href={`/auth/login`}>
                <Button color="secondary">ยืนยัน</Button>
              </a>
              <Button
                className="mr-3"
                color="primary"
                onClick={() => {
                  // this.setState({ signOut: false })
                  setsignOut(false);
                }}
              >
                ยกเลิก
              </Button>
            </ModalFooter>
          </Modal>

          <Createmodal />

          <Problemmodal />

          <Breedmodal />
        </div>
      )}
    </div>
  );
});

export default AdminNavbar;
