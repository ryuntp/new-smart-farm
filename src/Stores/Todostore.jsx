import { createContext } from 'react'
import { observable, computed, makeAutoObservable } from 'mobx'

export class Todostore {

  constructor() {
    makeAutoObservable(this)
  }
  todos = [
    { id: 1, text: 'Buy eggs', completed: true },
    { id: 2, text: 'Write a post', completed: false }
  ]

  TODO_number = 0

  get remainingTodos() {
    return this.todos.filter(t => !t.completed).length
  }

  toggleTodoPlus = value => {
    this.TODO_number = value
  }

  toggleTodo = index => {
    this.todos[index].completed = !this.todos[index]
      .completed
  }
}

// decorate(Todos, {
//   todos: observable,
//   remainingTodos: computed
// })

export default createContext(new Todostore())
