import { createContext, useContext } from "react";
import { observable, computed, makeAutoObservable } from "mobx";
import axiosInstance from "../Lib/AxiosInterceptor";
import moment from "moment";
import { UIstore } from "./UIstore";

export class CalendarsworkerStore {
  constructor(arg) {
    this.uistore = new UIstore(this);
    makeAutoObservable(this);
  }

  counter = 0;
  workselectmodal = false;
  workerselectmodal = false;
  workerselectdata = [];
  workerSingleSelectData = [];
  workerselectdatacalendar = [];
  workersearchdata = [];
  poundlistForcheck = [];

  workdetailBymodal = [];

  worktemplateDisplay = "";
  worktemplateDisplayCalendar = "";
  eventData = [];
  updateworkDateSelect = "";
  list_workdata = [];
  worktemplate_list = [];
  worktemplate_listCalendar = [];

  cardinfoSendapi = [];
  cardinfoSendapi_index = 0;

  option = {
    worktype: [],
    worklist: [],
    workerlist: [],
    dateoption: "custom",
    // "startdate": "2021-05-02",
    // "enddate": "2022-02-06"
    startdate: new Date().toISOString(),
    enddate: new Date(
      new Date().setDate(new Date().getDate() + 30)
    ).toISOString(),
  };

  optioncalendars = {
    worktype: [],
    worklist: [],
    workerlist: [],
    dateoption: "custom",
    startdate: new Date().toISOString(),
    enddate: new Date(
      new Date().setDate(new Date().getDate() + 30)
    ).toISOString(),
  };

  Setworkersearchdata = (value) => {
    this.workersearchdata = value;
  };

  SetupdateworkDateSelect = (value) => {
    this.updateworkDateSelect = value;
  };

  Setoption = (value) => {
    this.option = value;
  };
  SetworkdetailBymodal = (value) => {
    this.workdetailBymodal = value;
  };

  SetpoundlistForcheck = (value) => {
    this.poundlistForcheck = value;
  };

  Setoptioncalendars = (value) => {
    this.optioncalendars = value;
  };

  SetworktemplateDisplay = (value) => {
    this.worktemplateDisplay = value;
  };
  SetworktemplateDisplayCalendar = (value) => {
    this.worktemplateDisplayCalendar = value;
  };

  Setworkselectmodal = (value) => {
    this.workselectmodal = value;
  };

  Setworkerselectmodal = (value) => {
    this.workerselectmodal = value;
  };

  Setlist_workdata = (value) => {
    this.list_workdata = value;
  };
  Setinitial_events = (value) => {
    this.eventData = value;
  };

  Setworktemplate_list = (value) => {
    this.worktemplate_list = value;
  };
  Setworktemplate_listCalendar = (value) => {
    this.worktemplate_listCalendar = value;
  };

  SetcardinfoSendapi = (value) => {
    this.cardinfoSendapi = value;
  };

  SetcardinfoSendapi_Followindex = (value, index) => {
    this.cardinfoSendapi[index] = value;
  };

  SetcardinfoSendapi_index = (value) => {
    this.cardinfoSendapi_index = value;
  };

  AddcardinfoSendapi = (value) => {
    this.cardinfoSendapi.push(value);
  };

  addworkerSelectdata = (value) => {
    this.workerselectdata.push(value);
  };
  addWorkerSingleSelectData = (value) => {
    this.workerSingleSelectData = [value];
  };
  removeworkerSelectdata = (index) => {
    this.workerselectdata.splice(index, 1);
  };
  removeallworkerSelectdata() {
    this.workerselectdata = [];
  }

  addworkerSelectdatacalendar = (value) => {
    this.workerselectdatacalendar.push(value);
  };
  removeworkerSelectdatacalendar = (index) => {
    this.workerselectdatacalendar.splice(index, 1);
  };
  removeallworkerSelectdatacalendar() {
    this.workerselectdatacalendar = [];
  }

  async Getlist_workdata() {
    await axiosInstance
      .post(`/manager/work/worklistwithfilter`, this.option)
      .then((res) => {
        this.Setlist_workdata(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }
  async Getoneworkdata(id) {
    await axiosInstance
      .get(`/worker/work/onework/${id}`)
      .then((res) => {
        this.SetupdateworkDateSelect(res.data[0]);
        this.SetpoundlistForcheck(res.data[0].poundlist);
      })
      .catch((err) => {
        console.log(err);
      });
  }
  async Checkpoundlist() {
    var poundlist = [];
    for (const key in this.cardinfoSendapi) {
      if (Object.hasOwnProperty.call(this.cardinfoSendapi, key)) {
        const elementSendapi = this.cardinfoSendapi[key];
        for (const key in elementSendapi.poundlist) {
          if (Object.hasOwnProperty.call(elementSendapi.poundlist, key)) {
            const elementpoundlist = elementSendapi.poundlist[key];
            console.log("elementpoundlist", elementpoundlist);
            poundlist.push(elementpoundlist);
          }
        }
      }
    }
    await axiosInstance
      .post(`/all/work/checkpoundlist`, {
        workid: this.updateworkDateSelect.id,
        poundlist: poundlist,
      })
      .then((res) => {
        console.log(res.data[0], "res.data");
        this.SetpoundlistForcheck(res.data[0].poundlist);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  Changeallchecked = (value, index) => {
    var newArrObj = this.worktemplate_list;
    for (let index = 0; index < value[0].worklist.length; index++) {
      const element = value[0].worklist[index];
      element.checked = !element.checked;
    }
    newArrObj[index] = value;
    this.Setworktemplate_list(newArrObj);
  };

  ChangeallcheckedCalendar = (value, index) => {
    var newArrObj = this.worktemplate_listCalendar;
    for (let index = 0; index < value[0].worklist.length; index++) {
      const element = value[0].worklist[index];
      element.checked = !element.checked;
    }
    newArrObj[index] = value;
    this.Setworktemplate_listCalendar(newArrObj);
  };

  Getinitial_events = () => {
    axiosInstance
      .post(`/manager/work/worklistwithfilter`, this.optioncalendars)
      .then((res) => {
        const eventArr = [];
        for (let index = 0; index < res.data.length; index++) {
          const element = res.data[index];
          for (const key in element.worklist) {
            if (Object.hasOwnProperty.call(element.worklist, key)) {
              const event = element.worklist[key];
              eventArr.push(event);
            }
          }
        }
        this.Setinitial_events(eventArr);
      });
  };

  Getworktemplete = (value) => {
    axiosInstance
      .get(`/manager/work/allworktemplate`)
      .then((res) => {
        const arrayData = res.data[0].array;
        for (let index = 0; index < arrayData.length; index++) {
          const element = arrayData[index];
          for (const key in element[0].worklist) {
            if (Object.hasOwnProperty.call(element[0].worklist, key)) {
              const worklist = element[0].worklist[key];
              worklist.checked = false;
              console.log("worklist", worklist.checked);
            }
          }
        }
        this.Setworktemplate_list(res.data[0].array);
        this.Setworktemplate_listCalendar(res.data[0].array);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  Postupdatework = () => {
    axiosInstance
      .post(`/all/work/updatework`, {
        workid: this.updateworkDateSelect.id,
        isall: this.poundlistForcheck.length === 0 ? true : false,
        cardinfo: this.cardinfoSendapi,
      })
      .then((res) => {
        const arrayData = res.data[0].array;
        for (let index = 0; index < arrayData.length; index++) {
          const element = arrayData[index];
          for (const key in element[0].worklist) {
            if (Object.hasOwnProperty.call(element[0].worklist, key)) {
              const worklist = element[0].worklist[key];
              worklist.checked = false;
            }
          }
        }
        this.Setworktemplate_list(res.data[0].array);
        this.Setworktemplate_listCalendar(res.data[0].array);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  SearchWorker = (value) => {
    axiosInstance
      .get(`manager/user/searchworker/${value}`)
      .then((res) => {
        this.Setworkersearchdata(res.data[0].array);
        console.log(res.data[0].array);
      })
      .catch((err) => {
        console.log(err);
      });
  };
}

export default createContext(new CalendarsworkerStore());
