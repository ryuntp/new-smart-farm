import { createContext } from "react";
import { observable, computed, makeAutoObservable } from "mobx";
import axiosInstance from "../Lib/AxiosInterceptor";
import moment from "moment";

export class Worklotmanagestore {
    constructor() {
        makeAutoObservable(this);
    }
    filterhistory = false
    countpound = []
    crickettypelist = [
        {
            name: 'จิ้งหรีดสะดิ้ง',
            id: 1,
            selected: false
        },
        {
            name: 'จิ้งหรีดทองดำ',
            id: 2,
            selected: false
        },
        {
            name: 'จิ้งหรีดทองแดง',
            id: 3,
            selected: false
        },
        {
            name: 'จิ้งหรีด S15',
            id: 4,
            selected: false
        }
    ]
    history_arr = [
        {
            name: 'lot1',
            active: true
        },
        {
            name: 'lot2',
            active: false
        }
    ];


    option = {
        "initdate": [moment(new Date(new Date().setDate(new Date().getDate() + 30)).toISOString()).format("YYYY-MM-DD"),moment(new Date().toISOString()).format("YYYY-MM-DD")],
        "crickettype_id": [],
        "havestdate": [],
        // "startdate": new Date().toISOString(),
        // "enddate": new Date(new Date().setDate(new Date().getDate() + 30)).toISOString()
    }

    addcrickettypelist = (value) => {
        this.option.crickettype_id.push(value)
    }

    removecrickettypelist = (index) => {
        this.option.crickettype_id.splice(index, 1);
    }

    setfilterhistory = (value) => {
        this.filterhistory = value;
    };

    setoption = (value) => {
        this.option = value;
    };
    setcountpound = (value) => {
        this.countpound = value;
    };
    sethistory_arr = (value) => {
        this.history_arr = value;
    };

    async gethistory(value) {
        await axiosInstance.post(`/manager/lot/lothistorywithfilter`, this.option)
            .then((res) => {
                for (const key in res.data[0].lotlist) {
                    if (Object.hasOwnProperty.call(res.data[0].lotlist, key)) {
                        const element = res.data[0].lotlist[key];
                        element.active = false
                    }
                }

                this.sethistory_arr(res.data[0])
            })
            .catch((err) => {
                console.log(err);
            })
    };

    getCountpound = (value) => {
        axiosInstance.get(`/all/place/countpound`)
            .then((res) => {


                this.setcountpound(res.data[0])
            })
            .catch((err) => {
                console.log(err);
            })

    }
}

export default createContext(new Worklotmanagestore());
