import { createContext } from "react";
import { observable, computed, makeAutoObservable } from "mobx";

export class Problemstore {
  constructor() {
    makeAutoObservable(this);
  }
  enemyList = "false";
  note = "";

  Setenemy = (value) => {
    this.enemyList = value;
    this.note = value;
  };
}

// decorate(Todos, {
//   todos: observable,
//   remainingTodos: computed
// })

export default createContext(new Problemstore());
