import { createContext } from "react";
import { observable, computed, makeAutoObservable } from "mobx";

export class HomedashStore {
    constructor() {
        makeAutoObservable(this);
    }
    ponds_id = 0;

    insectarium_id = 0;

    ponds_detail = {}


    setponds_detail = (value) => {
        this.ponds_detail = value
    }

    setponds_id = (value) => {
        this.ponds_id = value;
    };

    setinsectarium_id = (value) => {
        this.insectarium_id = value;
    };
}

export default createContext(new HomedashStore());
