import { createContext } from 'react'
import { observable, computed, makeAutoObservable } from 'mobx'

export class Rootstore {

  constructor() {
    makeAutoObservable(this)
  }
  todos = [
    { id: 1, text: 'Buy eggs', completed: true },
    { id: 2, text: 'Write a post', completed: false }
  ]
  
  datacardinfo = []

  Number_Test = 0

  get remainingTodos() {
    return this.todos.filter(t => !t.completed).length
  }

  toggleTodoPlus = value => {
    this.Number_Test = value
  }

  toggleTodo = index => {
    this.todos[index].completed = !this.todos[index]
      .completed
  }


  Setdatacardinfo = value => {
    this.datacardinfo = value
  }

  Adddatacardinfo = value => {
    this.datacardinfo.push(value)
  }

  removedatacardinfo = index => {
    this.datacardinfo.splice(index, 1);
  }

}

// decorate(Todos, {
//   todos: observable,
//   remainingTodos: computed
// })

export default createContext(new Rootstore())
