import { createContext } from "react";
import { observable, computed, makeAutoObservable } from "mobx";

export class UIstore {
  constructor() {
    makeAutoObservable(this);
  }
  Isloading = false;
  EnableHeader = false;
  Headercolor = '';
  EnablesubHeader = false;
  EnableFooter = false;
  Createmodal = false;
  Problemmodal = false;
  Breedmodal = false;
  EscapeModal = false;
  Filterworker = false;
  InfectedModal = false;
  addworkCalendar = false;
  addBreedmodal = false;
  addnoteCreateworkmodal = false;
  addProblemmodal = false;
  addEscapeModal = false;
  addInfectedModal = false;
  updateworkcalendarmodal = false;
  addEnemyNoteModal = false;
  addnoteBreedmodal = false;
  addEscapeNoteModal = false;
  addInfectedNoteModal = false;

  addEnemyListModal = false;

  duplicateProblemmodal = false;
  duplicateBreedmodal = false;
  duplicateEscapemodal = false;
  duplicateInfectedmodal = false;

  // create lot modal
  assignTaskModal = false;
  createLotModal = false;
  addnoteCreateLotmodal = false;
  // choose breed modal
  chooseBreedModal = false;
  addNewBreedModal = false;
  step2Modal = false;
  breedName = null;
  ///////
  editNotiModal = false;

  SetIsloading = (value) => {
    console.log("asd", value);
    this.Isloading = value;
  };

  SetEnableHeader = (value) => {
    this.EnableHeader = value;
  };
  SetHeadercolor = (value) => {
    this.Headercolor = value;
  };
  
  SetEnableFooter = (value) => {
    this.EnableFooter = value;
  };
  
  SetEnablesubHeader = (value) => {
    this.EnablesubHeader = value;
  };

  SetduplicateBreedmodal = (value) => {
    this.duplicateBreedmodal = value;
  };

  SetaddworkCalendar = (value) => {
    this.addworkCalendar = value;
  };

  SetaddnoteCreateworkmodal = (value) => {
    this.addnoteCreateworkmodal = value;
  };

  SetduplicateProblemmodal = (value) => {
    this.duplicateProblemmodal = value;
  };

  SetaddnoteBreedmodal = (value) => {
    this.addnoteBreedmodal = value;
  };

  Setupdateworkcalendarmodal = (value) => {
    this.updateworkcalendarmodal = value;
  };

  SetaddBreedmodal = (value) => {
    this.addBreedmodal = value;
  };

  SetaddProblemmodal = (value) => {
    this.addProblemmodal = value;
  };

  SetaddEnemyListModal = (value) => {
    this.addEnemyListModal = value;
  };

  SetaddEnemyNoteModal = (value) => {
    this.addEnemyNoteModal = value;
  };

  SetBreedmodal = (value) => {
    this.Breedmodal = value;
  };

  SetFilterworker = (value) => {
    this.Filterworker = value;
  };

  SetCreatemodal = (value) => {
    this.Createmodal = value;
  };

  SetProblemmodal = (value) => {
    this.Problemmodal = value;
  };

  SetEscapemodal = (value) => {
    this.EscapeModal = value;
  };

  SetaddEscapeModal = (value) => {
    this.addEscapeModal = value;
  };

  SetduplicateEscapemodal = (value) => {
    this.duplicateEscapemodal = value;
  };

  SetaddNoteEscapeModal = (value) => {
    this.addEscapeNoteModal = value;
  };
  SetInfectedmodal = (value) => {
    this.InfectedModal = value;
  };

  SetaddInfectedModal = (value) => {
    this.addInfectedModal = value;
  };

  SetduplicateInfectedmodal = (value) => {
    this.duplicateInfectedmodal = value;
  };

  SetaddNoteInfectedModal = (value) => {
    this.addInfectedNoteModal = value;
  };
  SetAddnoteCreateLotModal = (value) => {
    this.addnoteCreateLotmodal = value;
  };

  //// assigntask modal
  setAssignTaskModal = (value) => {
    console.log("hihi ", value);
    this.assignTaskModal = value;
  };
  setCreateLotModal = (value) => {
    this.createLotModal = value;
  };
  // choose breed modal
  setChooseBreedModal = (value) => {
    this.chooseBreedModal = value;
  };
  setAddNewBreedModal = (value) => {
    this.addNewBreedModal = value;
  };
  setStep2Modal = (value, breedName) => {
    if (breedName) {
      this.breedName = breedName;
      console.log("breedName ", breedName);
    }
    this.step2Modal = value;
  };
  setEditNotiModal = (value) => {
    console.log("ttt ", value);
    this.editNotiModal = value;
  };
}

// decorate(Todos, {
//   todos: observable,
//   remainingTodos: computed
// })

export default createContext(new UIstore());
