import { createContext } from "react";
import { observable, computed, makeAutoObservable } from "mobx";

export class EscapeStore {
  constructor() {
    makeAutoObservable(this);
  }
  note = "";

  setNote = (value) => {
    this.note = value;
  };
}

export default createContext(new EscapeStore());
