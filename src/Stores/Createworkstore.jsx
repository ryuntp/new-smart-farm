import { createContext } from "react";
import { observable, computed, makeAutoObservable } from "mobx";
import axiosInstance from "../Lib/AxiosInterceptor";
import { toJS } from "mobx";

export class Createworkstore {
  constructor() {
    makeAutoObservable(this);
  }
  notecreatework = "";

  modal_splitlot = false;

  modal_jobformat = false;

  modal_createjobformat = false;

  modal_jobdescription = false;

  selectedWork = null;

  selectedWorkInput = null;

  taskNames = [];
  answers = [];

  Setnotecreatework = (value) => {
    this.notecreatework = value;
  };

  Setmodal_splitlot = (value) => {
    this.modal_splitlot = value;
  };

  Setmodal_jobformat = (value) => {
    this.modal_jobformat = value;
  };

  Setmodal_createjobformat = (value) => {
    this.modal_createjobformat = value;
  };

  Setmodal_jobdescription = (value) => {
    this.modal_jobdescription = value;
  };

  SetSelectedWork = (value) => {
    this.selectedWork = value;
    this.taskNames = [];
    this.answers = [];
  };

  SetTaskNames = (value) => {
    this.taskNames = [...this.taskNames, value];
  };
  SetAnswer = (value) => {
    this.answers = value;
  };

  async getWorkInputs() {
    if (this.selectedWork) {
      let inputs = await axiosInstance.get(
        `/manager/work/workdetail/${this.selectedWork.id}`
      );

      console.log("inputsinputs ", inputs.data);
      this.selectedWorkInput = inputs.data;

      this.taskNames = [];
      for (let task of inputs.data) {
        this.taskNames.push(task.name);
      }
    }
  }
}

// decorate(Todos, {
//   todos: observable,
//   remainingTodos: computed
// })

export default createContext(new Createworkstore());
