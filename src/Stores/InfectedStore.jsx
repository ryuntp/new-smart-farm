import { createContext } from "react";
import { observable, computed, makeAutoObservable } from "mobx";

export class InfectedStore {
  constructor() {
    makeAutoObservable(this);
  }
  note = "";

  setNote = (value) => {
    this.note = value;
  };
}

export default createContext(new InfectedStore());
