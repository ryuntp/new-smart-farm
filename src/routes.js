/*!

=========================================================
* Paper Dashboard PRO React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// import รายงาน from "views/pages/รายงาน.jsx"
// import คำถามที่พบบ่อย from "views/pages/คำถามที่พบบ่อย.jsx"
// import Buttons from "views/components/Buttons.jsx";
import HomeDash from "./View/Home/HomeDash";
import HomeDashDetail from './View/Home/HomeDashDetail'
import HomeMenu from './View/Home/HomeMenu'
import Login from './View/Login/Login'
import Worklotmanage from './View/Worklot/Worklotmanage'
import Worklothistory from './View/Worklot/Worklothistory'
import Worklotdetail from './View/Worklot/Worklotdetail'
import Creatework from './View/Creatework/Creatework'
import History from './View/History/History'
import NotiScreen from './View/NotiScreen/NotiScreen'
import Calendarworker from "./View/Worker/Calendarworker";
import Calendardetail from "./View/Worker/Calendardetail"

// const routes2 = [
//   {
//     path: "/login",
//     name: "Login",
//     mini: "L",
//     component: Login,
//     layout: "/auth"
//   }
// ]
// const routes3 = [
//   {
//     path: "/user-profile",
//     name: "UserProfile",

//     component: UserProfile,
//     layout: "/admin"
//   }
// ]
const routes = [
  {
    path: "/login",
    name: "Login",
    mini: "L",
    component: Login,
    layout: "/auth"
  },
  {
    path: "/HomeDash",
    name: "HomeDash",
    component: HomeDash,
    layout: "/admin",

  },
  {
    path: "/HomeDashDetail",
    name: "HomeDashDetail",
    component: HomeDashDetail,
    layout: "/admin",
  },
  {
    path: "/HomeMenu",
    name: "HomeMenu",
    component: HomeMenu,
    layout: "/admin",
  },
  {
    path: "/Worklotmanage",
    name: "Worklotmanage",
    component: Worklotmanage,
    layout: "/admin",
  },
  {
    path: "/Worklotdetail",
    name: "Worklotdetail",
    component: Worklotdetail,
    layout: "/admin",
  },
  {
    path: "/Worklothistory",
    name: "Worklothistory",
    component: Worklothistory,
    layout: "/admin",
  },

  {
    path: "/Creatework",
    name: "Creatework",
    component: Creatework,
    layout: "/admin",
  },
  {
    path: "/Calendarworker",
    name: "Calendarworker",
    component: Calendarworker,
    layout: "/admin",
  },
  {
    path: "/Calendardetail",
    name: "Calendardetail",
    component: Calendardetail,
    layout: "/admin",
  },
  {
    path: "/History",
    name: "History",
    component: History,
    layout: "/admin",
  },
  {
    path: "/NotiScreen",
    name: "NotiScreen",
    component: NotiScreen,
    layout: "/admin",
  }



];

export { routes };
