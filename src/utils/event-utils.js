
let eventGuid = 0
let todayStr = new Date().toISOString().replace(/T.*$/, '') // YYYY-MM-DD of today

export const INITIAL_EVENTS = [
  {
    id: 7,

    start: "2022-02-05T00:00:00.000Z",
    title: "งานประจำวันสร้างให้1",

    // id: createEventId(),
    // title: 'All-day event',
    // start: todayStr
  },
  {
    id: createEventId(),
    title: 'Timed event',
    start: todayStr + 'T12:00:00'
  },
  // {
  //   id: createEventId(),
  //   title: 'Timed event',
  //   start: todayStr + 'T12:00:00'
  // },



]

export function createEventId() {
  return String(eventGuid++)
}
