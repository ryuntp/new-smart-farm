import React, { useContext, useEffect, useState } from "react";
import Rootstore from "../../Stores/Rootstore";
import axios from "axios";
import { observer } from "mobx-react-lite";
import UIstore from "../../Stores/UIstore";
import _ from "lodash";
import view_icon from "../../Assets/img/icon-general-view.png";
import checkIcon from "../../Assets/img/fb-check-a@2x.png";
import axiosInstance from "../../Lib/AxiosInterceptor";
import moment from "moment";
// const stores = useContext(StoreContext);

const handleColorType = (bgcolor) => {
  switch (bgcolor) {
    case "Bret":
      return "#03a9f3";
    case "Bruh":
      return "#f56342";
    default:
      return "#000";
  }
};
const URL = "https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com";

const History = observer((props) => {
  const [Tab, setTab] = React.useState(1);
  const [hisList, setHisList] = useState([]);
  const [notiList, setNotiList] = useState([]);
  const uistore = useContext(UIstore);

  useEffect(() => {
    const fetch = async () => {
      await getNotifications();
    };
    fetch();
    console.log("tab", Tab);
  }, [Tab]);

  const handleDisplayTime = (time) => {
    return time.split("T")[1].slice(0, 5);
  };

  const calPoundList = (arr) => {
    let ans = [];
    let firstval;

    for (let [i, num] of arr.entries()) {
      if (num + 1 === arr[i + 1]) {
        if (!firstval) firstval = num;
      } else {
        if (firstval) {
          if (firstval !== num) {
            ans.push(`${firstval}-${num}`);
          }
        } else {
          ans.push(num.toString());
        }

        firstval = undefined;
      }
    }

    return ans;
  };

  const handlePoundList = (arr) => {
    return arr.map((pound) => {
      return (
        <span className="sarabun_M_300 text_dimgray">
          {pound.insectarium_name}: บ่อที่ {calPoundList(pound.pound_name)}
        </span>
      );
    });
  };

  const sumPounds = (arr) => {
    let sum = 0;

    for (const pound of arr) {
      sum += pound.pound_name.length;
    }

    return sum;
  };

  const handleTimeMoment = (time) => {
    let res = {};
    let days = {
      Monday: "จันทร์",
      Tuesday: "อังคาร",
      Wednesday: "พุธ",
      Thursday: "พฤหัสบดี",
      Friday: "ศุกร์",
      Saturday: "เสาร์",
      Sunday: "อาทิตย์",
    };
    res["date"] = moment(time).format("DD");
    res["day"] = moment(time).format("dddd");
    return res;
  };

  const getNotifications = async () => {
    if (Tab === 1) {
      let history = await axiosInstance.get("/manager/work/workhistory");

      console.log("history ", history);
      let abc = handleTimeMoment(history.data[0].time);
      console.log("abc ", abc);
      setHisList(history.data);
    }
    if (Tab === 2) {
      let req2 = await axiosInstance.get(
        "/all/notification/allnotification/1/1",
        {
          headers: { authorization: localStorage.getItem("token") },
        }
      );
      console.log("req2 ", req2);
      setNotiList(req2.data);
    }
  };

  const renderNotiList = () => {
    return (
      <div className="card-noti d-flex flex-column align-items-center pt-4 w-100">
        <div className="tab-container d-flex flex-row justify-content-between align-items-center">
          <div
            onClick={() => {
              setTab(1);
            }}
            className={`d-flex align-items-center justify-content-center ${
              Tab === 1 ? "tab-active " : "tab-unactive"
            }`}
          >
            <span
              className={`${
                Tab === 1
                  ? "prompt_L_600 text_dodgerblue "
                  : "prompt_L text_dimgray"
              }`}
            >
              งานเพาะเลี้ยง
            </span>
            <span className=""></span>
          </div>
          <div
            onClick={() => {
              setTab(2);
            }}
            className={`d-flex align-items-center justify-content-center ${
              Tab === 2 ? "tab-active " : "tab-unactive"
            }`}
          >
            <span
              className={`${
                Tab == 2
                  ? "prompt_L_600 text_dodgerblue "
                  : "prompt_L text_dimgray"
              }`}
            >
              การแจ้งเตือน
            </span>
          </div>
        </div>

        {Tab === 1 ? (
          <div className="w-100">
            <div className="px-4 w-100 d-flex flex-row justify-content-between align-items-center">
              <span className="prompt_XXL text_blueteal">ประวัติงาน</span>

              <div className="d-flex flex-row justify-content-center align-items-center">
                <span className="prompt_M text_gray">
                  {hisList.length} รายการ
                </span>
              </div>
            </div>
            {hisList.map((data, index) => {
              return (
                <div className={index === 0 ? "w-100 mt-3" : "w-100 mt-4"}>
                  <div className="px-4 w-100 d-flex flex-row justify-content-between align-items-center">
                    <span className="prompt_XL text_blueteal">
                      {handleTimeMoment(data.time).date},{" "}
                      {handleTimeMoment(data.time).day}
                    </span>

                    <div className="d-flex flex-row justify-content-center align-items-center">
                      <span className="prompt_M text_gray">
                        {data.worklist.length} รายการ
                      </span>
                    </div>
                  </div>
                  {data.worklist.map((job, index) => {
                    return (
                      <div
                        key={index}
                        className="d-flex flex-column w-100 border-bottom pb-3"
                      >
                        <div className="my-3 d-flex flex-column px-4">
                          <div className="w-100 d-flex flex-row justify-content-between align-items-center">
                            <span className="prompt_L text_blueteal">
                              {" "}
                              {job.title}{" "}
                            </span>
                            <div className="d-flex flex-row justify-content-center align-items-center">
                              <img
                                src={checkIcon}
                                alt=""
                                width={30}
                                height={30}
                                className="mr-2"
                              />
                            </div>
                          </div>
                          <span className="sarabun_M_300 text_dimgray">
                            {" "}
                            {job.lot_name}: {handlePoundList(job.poundlist)}{" "}
                          </span>
                        </div>
                        <div className="d-flex flex-row justify-content-between align-items-center w-100 px-4">
                          <div className="d-flex flex-row align-items-center">
                            <div
                              className="mr-3"
                              style={{
                                width: 30,
                                height: 30,
                                borderRadius: 100,
                                backgroundColor: "red",
                              }}
                            ></div>
                            <span className="sarabun_M_300 ">{job.name}</span>
                          </div>
                          <span className="sarabun_M_300 text_dimgray">
                            {handleDisplayTime(job.start)}
                          </span>
                        </div>
                      </div>
                    );
                  })}
                </div>
              );
            })}
          </div>
        ) : null}

        {Tab === 2 ? (
          <div className="w-100">
            <div className="px-4 w-100 d-flex flex-row justify-content-between align-items-center">
              <span className="prompt_XXL text_blueteal">
                การแจ้งเตือนที่ผ่านมา
              </span>
            </div>

            <div className="px-4 border-bottom">
              {notiList.map((data, index) => {
                return (
                  <div
                    key={index}
                    className="d-flex flex-column w-100 pb-3 card-notihis-green my-4"
                  >
                    <div className="my-3 d-flex flex-column px-4">
                      <div className="w-100 d-flex flex-row justify-content-between align-items-center">
                        <span className="prompt_L text_blueteal">
                          {" "}
                          {data.name}{" "}
                        </span>
                        <div className="d-flex flex-row justify-content-center align-items-center">
                          <span className="prompt_L text_blueteal">
                            X {sumPounds(data?.poundlist)} บ่อ
                          </span>
                        </div>
                      </div>{" "}
                      {handlePoundList(data.poundlist)}
                    </div>
                    <div className="d-flex flex-row justify-content-between align-items-center w-100 px-4">
                      <div className="d-flex flex-row align-items-center">
                        <div
                          className="mr-3"
                          style={{
                            width: 30,
                            height: 30,
                            borderRadius: 100,
                            backgroundColor: "red",
                          }}
                        ></div>
                        <span className="sarabun_M_300 ">
                          {data.worker_name}
                        </span>
                      </div>
                      <span className="sarabun_M_300 text_dimgray">
                        {handleDisplayTime(data.time)}
                      </span>
                    </div>
                  </div>
                );
              })}
            </div>

            <div
              onClick={() => {
                uistore.SetCreatemodal(true);
              }}
              className="w-100 py-3 d-flex justify-content-center align-items-center "
            >
              {/* <div className="d-flex flex-row align-items-center">
                <div className="add-circle-orange d-flex align-items-center justify-content-center "></div>

                <span className="prompt_XL ml-3"> เพิ่มการแจ้งเตือน</span>
              </div> */}
            </div>
          </div>
        ) : null}
      </div>
    );
  };

  return renderNotiList();
});

export default History;
