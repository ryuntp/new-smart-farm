import React, { useState, useEffect, useContext } from "react";
import axios from 'axios'
import {
  Card,
  CardBody,
  Row,
  Col,
  FormGroup,
  Input,
  Collapse,
} from "reactstrap";
import tear_img from '../../Assets/img/Tear.jpg'
import smile_icon from '../../Assets/img/icon-emoji-smile-sst.png'
import view_icon from '../../Assets/img/icon-general-view.png'
import { observer } from "mobx-react";
import HomedashStore from "../../Stores/HomedashStore";
import { Link } from "react-router-dom";

// import "react-circular-progressbar/dist/styles.css";
const URL = "https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com"

const handleGraphColorTemp = (percentage) => {
  if (percentage == 32) {
    return "#25f19b";
  } else if (percentage > 32 && percentage < 35) {
    return "#FF9900";
  } else if (percentage < 32 && percentage > 29) {
    return "#FF9900";
  } else if (percentage >= 35 || percentage <= 29) {
    return "#FF0000";
  }
};

const handleGraphColorAmm = (percentage) => {
  if (percentage < 10) {
    return "#25f19b";
  } else if (percentage == 10) {
    return "#FF9900";
  } else if (percentage > 10) {
    return "#FF0000";
  }
};

const handleGraphColorHum = (percentage) => {
  if (percentage <= 50) {
    return "#25f19b";
  } else if (percentage > 50 && percentage <= 60) {
    return "#FF9900";
  } else if (percentage > 60) {
    return "#FF0000";
  }
};

const HatcheryDetail = observer(({
  percentage1,
  percentage2,
  card,
  index,
  id
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [item_fromparam, setitem_fromparam] = useState(false);
  const [eggstatus, seteggstatus] = useState(card.egg_lossc);
  const [hum, sethum] = useState("")
  const [temp, settemp] = useState("")

  const homedashStore = useContext(HomedashStore)
  // setponds_id

  // const onClick1 = () => {

  //   seteggstatus("ปกติ")

  //   const status = { status: "ปกติ" }
  //   console.log(status)
  //   axios.post(`${URL}/updateeggstatus/${card.egg_id}`, status)
  //     .then(res => {
  //       console.log(res)
  //     })

  // };
  // const onClick2 = () => {

  //   seteggstatus("ไม่ปกติ")

  //   const status = { status: "ไม่ปกติ" }
  //   axios.post(`${URL}/updateeggstatus/${card.egg_id}`, status);
  // };

  // const gethummidity = async () => {
  //   try {
  //     const gethummidity = await axios.get(`${URL}/api/sendhumiegg`)

  //     sethum(gethummidity.data.humiritypound);
  //     console.log(gethummidity)

  //   } catch (err) {
  //     console.error(err.message);
  //   }
  // };

  // const gettemperature = async () => {
  //   try {
  //     const gettemperature = await axios.get(`${URL}/api/sendtempegg`)

  //     settemp(gettemperature.data.temperaturepound);
  //     console.log(gettemperature)

  //   } catch (err) {
  //     console.error(err.message);
  //   }
  // };

  useEffect(() => {

    // gethummidity()
    // gettemperature()
  }, []);

  return (
    <div className="card-ponds ponds-body" id={index} key={index}>

      <Row onClick={() => setIsOpen(!isOpen)}>
        <Col xs="12">
          <div
            className="ponds-header">
            <div className='d-flex flex-column'>
              <span
                className="prompt_XXL text_blueteal">
                {"โรงเลี้ยง  "}
                {card.id}
              </span>
              <span
                className="ponds-text sarabun_L_300">
                {/* {"Warehouse  "} */}
                {card.name}
              </span>
            </div>

            <div className="mt-1 ml-3" onClick={() => setIsOpen(!isOpen)}>
              <i
                className="nc-icon nc-minimal-down"
                style={{
                  color: "black",
                  fontWeight: "bold",
                  float: "right",
                }}
              />
            </div>
          </div>

        </Col>
      </Row>
      <Collapse isOpen={isOpen}>
        <div className="card-ponds ponds-collapse mt-4">

          <div className='d-flex justify-content-around align-items-center w-100'>
            <div className='d-flex flex-row align-items-center'>
              <div className='ponds-progress-bar'>
                <div className='ponds-progress-bar ponds-progress-bar-in'>
                </div>
              </div>


              <div className='d-flex justify-content-between align-items-center flex-column mb-3'>
                <span className='prompt_XXXL text_black'>30</span>
                <div className='d-flex flex-row '>
                  <span className='sarabun_M text_dimgray mr-1 '>อุณหภูมิ </span>
                  <span className='sarabun_M text_dimgray '> °C</span>
                </div>
              </div>
            </div>


            <div className='line-ponds'>

            </div>

            <div className='d-flex flex-row align-items-center'>

              <div>
                <img className='tear' src={tear_img} />
              </div>

              {/* <div className='ponds-progress-bar'>
                <div className='ponds-progress-bar ponds-progress-bar-in'>
                </div>
              </div> */}


              <div className='d-flex justify-content-between align-items-center flex-column mb-3'>
                <span className='prompt_XXXL text_black'>28</span>
                <div className='d-flex flex-row '>
                  <span className='sarabun_M text_dimgray mr-1 '>ความชื้น </span>
                  <span className='sarabun_M text_dimgray '> °C</span>
                </div>
              </div>
            </div>


          </div>




        </div>


        <div className='ammonoia-content'>

          <div className='d-flex flex-row align-items-center'>
            <img src={smile_icon} alt="" className='smile_icon' />

            <span className='sarabun_M text_dimgray ml-2'>
              แอมโมเนียร์
            </span>
          </div>


          <div className='d-flex flex-row align-items-center'>
            <span className='prompt_XXL text_darkturquoise ml-2'>
              33.2
            </span>

            <span className='sarabun_M_300 ml-2'>
              RPM
            </span>
          </div>

        </div>

        <div className='position-relative mt-1'>
          <div className='separate-line'>
            <div className='half-circle-left' />

            <div className='half-circle-right' />


          </div>
          <div className=' line-dash' />

        </div>



        <div className='ponds-list'>

          {card.poundList.map((item, index) => {
            return <Link to={`/admin/HomeDashDetail?ponds_id=${item.id}&insectarium_id=${item.insectarium_id}`} style={{ textDecoration: 'none' }}>
              <div
                // href={`/admin/Home?ponds_id=${item.id}&insectarium_id=${item.insectarium_id}`}
                onClick={() => {

                }}
                className="mb-4">
                <div className='d-flex flex-row justify-content-between'>
                  <span className='prompt_XL text_black'>
                    {item.name}
                  </span>

                  {item.status === '1' ? <div className="status-warning">
                    <span className="sarabun_M_300 text_white">ผิดปกติ</span>
                  </div> : null}
                </div>

                {(index + 1) == card.poundList.length ? '' : <hr />}

              </div>
            </Link>




          })}

          <div className='line-dash not-absolute' />


          <div className='d-flex flex-row justify-content-center align-items-center w-100 mt-4'>
            <img src={view_icon} alt="" width={30} height={30} className="mr-2" />

            {/* <span className='blue'> */}

            <a
              href={`/admin/HomeDashDetail`}

              className='prompt_XL text_dodgerblue'>
              ดูทั้งหมด
              {/* <p style={{ color: "white", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)" }}>
               
                  </p> */}
            </a>
            {/* </span> */}
          </div>

        </div>



      </Collapse>

    </div>
  )


});

export default HatcheryDetail;
