
import axios from 'axios';
import React from "react";
import AdminFooter from "../../components/AdminNavbarFooter/AdminFooter"
import WorkerFooter from "../../components/WorkerNavBarFooter/WorkerFooter"
import BoxCard from "../BoxCard"
import Cookies from "js-cookie"
import {
  Button,
  FormGroup,
  Form,
  Input,
  Row,
  Col,
  Collapse,
  Card,
  CardBody,
  CardFooter
} from "reactstrap";
import Steps, {Step} from "rc-steps"
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import AdminNavBar from '../../Components/AdminNavbar/AdminNavbar'
import { withRouter } from "react-router-dom";
const URL ="https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com"
const hatcheryData = {
    id: 1,
    hatcheryStatus: "ปกติ",
    recentClean: "16/1/64 23.00น.",
    temp: "25",
    humidity: "35",
    period: 11,
    workers: [
      {
        id: 1,
        firstName: "เนวิน"
      },
      {
        id: 2,
        firstName: "มดดำ"
      }
    ],
    rooms: [
      {
        id: 1,
        roomStatus: "ว่าง",
        startHatchDate: "20/1/64",
        endHatchDate: "17/3/64",
        ponds: [1,2,4]
      },
      {
        id: 2,
        roomStatus: "ไม่ว่าง",
        startHatchDate: "22/4/64",
        endHatchDate: "13/12/64",
        ponds: [1,3,5]
      }
    ]
  }


  const handleGraphColorTemp = (percentage) => {
    if (percentage == 32) {
      return "#25f19b";
    } else if (percentage > 32 && percentage < 35) {
      return "#FF9900";
    } else if (percentage < 32 && percentage > 29) {
      return "#FF9900";
    } else if (percentage >= 35 || percentage <= 29) {
      return "#FF0000";
    }
  };

  const handleGraphColorAmm = (percentage) => {
    if (percentage < 10) {
      return "#25f19b";
    } else if (percentage == 10) {
      return "#FF9900";
    } else if (percentage > 10) {
      return "#FF0000";
    }
  };

  const handleGraphColorHum = (percentage) => {
    if (percentage <= 50) {
      return "#25f19b";
    } else if (percentage > 50 && percentage <= 60) {
      return "#FF9900";
    } else if (percentage > 60) {
      return "#FF0000";
    }
  };

class Hatchery extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hatch: [],
      allbox:[],
      data1:[],
      data2:[],
      data3:[],
      ids:"",
      escapeInsectReport: "normal",
      hatcheryStatus: "ปกติ",
      isOpen: false,
      hum:"",
      temp:"",
    //   rodSmellReport: "ปกติ",
    //   deadInsects: "ปกติ",
    //   humidity:"0",
    //   temp:"10",
    //   hatcheryData: {
    //       id: 1,
    //       hatcheryStatus: "ปกติ",
    //       temp: "25",
    //       humidity: "35",
    //       rodSmell: "ปกติ",
    //       deadInsects: "ปกติ",
    //       escapeInsects: "ปกติ",
    //       accumulatedWater: 11,
    //       accumulatedFood: 21
    //     },
      toggleEnemyReport: false,
      enemyReport: "normal",
      insectariumStatus :'ปกติ'
    }
      this.checkstatus = this.checkstatus.bind(this);
     
  }
  componentDidMount() {
    
    this.gettemp();
    this.gethum();
    var query = new URLSearchParams(this.props.location.search);
    var params = query.get('hatchery_id');
    this.gethatch(params);
    this.getallbox(params);
  }
  
  onClick1 = () => {

    this.setState(prevState => ({
      hatch: {                   // object that we want to update
          ...prevState.hatch,    // keep all other key-value pairs
          'egg_lossc':"ปกติ"       // update the value of specific key
      }
  }))
  console.log("bf",this.state.ids);
  const status = {status:"ปกติ"} 
  console.log(status)
  axios.post(`${URL}/updateeggstatus/${this.state.ids}`,status)
  .then(res => {
    console.log(res)})
    console.log("at",this.state.ids);   
}

onClick2 = () => {

  this.setState(prevState => ({
    hatch: {                   // object that we want to update
        ...prevState.hatch,    // keep all other key-value pairs
        'egg_lossc':"ไม่ปกติ"       // update the value of specific key
    }
}))
console.log("bf",this.state.ids);
const status = {status:"ไม่ปกติ"} 
console.log(status)
axios.post(`${URL}/updateeggstatus/${this.state.ids}`,status)
.then(res => {
  console.log(res)})
  console.log("at",this.state.ids);   
}


  gethatch = (params) => {
  const { ids } = this.state
  const idegg = ids ? ids : params
  axios.get(`${URL}/egg/${idegg}`)
      .then(res => {
      console.log(res.data[0])
      this.setState({hatch: res.data[0]});
      console.log("param",params)
      if(params){
        console.log("pspspssp");
        this.setState({ ids: params });
      }
      console.log("eiei")
      

      })
      
  }
  getallbox = (params) => {
  const { ids } = this.state
  const idegg = ids ? ids : params
    axios.get(`${URL}/allbox/${idegg}`)
        .then(res => {
        console.log(res.data)
        this.setState({allbox: res.data});
        console.log(this.state.allbox)
  
        })
        
  }

  renderWorkers = (workers) => {
    if (!workers) { return }
    return workers.map((worker, index) => (
      worker.firstName + " "
    ))
  }

  renderBoxCards = (allbox) => {
    return allbox.map((allbox, index) => (
      <BoxCard
      key={index}
      allbox={allbox}
      index={index}
      />
    ))
  }

  checkstatus = () => {
    if(this.state.deadInsects =='ปกติ' && this.state.rodSmellReport == 'ปกติ'){
      this.setState({insectariumStatus:'ปกติ'})
    }
    else{
      this.setState({insectariumStatus:'ไม่ปกติ'})
    }
    console.log(this.state.insectariumStatus)
    console.log(this.state.rodSmellReport)
  }

  gethum = () => {
    axios.get(`${URL}/api/sendhumiegg`)
        .then(res => {
          console.log(res)
        this.setState({ hum:res.data.humiritypound });
        })
        
    }
    gettemp = () => {
      axios.get(`${URL}/api/sendtempegg`)
          .then(res => {
            console.log(res)
          this.setState({ temp:res.data.temperaturepound });
          })
          
      }

  dashboard = () => { 
    // - you can now access this.props.history for navigation
    this.props.history.push({pathname: "/admin/homedash", state: this.state});
  };

  render() {
   
    const { insectariumStatus, rodSmellReport, deadInsects, insectariumData, toggleEnemyReport, enemyReport, escapeInsectReport, hatcheryStatus, isOpen,hatch} = this.state
    const percentage1 = 25;
    const percentage2 = 5;
    const percentage3 = 35;
    
    const query = new URLSearchParams(this.props.location.search);
    const hatcheryId = query.get('hatchery_id')
    return (
      <>
        <div className="content">
          <button
            className="btn btn-primary d-flex"
            onClick={this.dashboard}
            >
              <i className="fas fa-chevron-circle-left mr-2 mt-1"></i>
              <p style={{fontColor: "white", fontSize:"12px", fontFamily: 'IBM Plex Sans Thai'}}>กลับหน้าแดชบอร์ด</p>
          </button>
        <Row>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col xs="12">
                      <div className="border mb-3 mt-1 d-flex" style={{height: '50px', borderRadius: '12px', justifyContent: 'center', alignItems: 'center'}}>
                        <div className="w-100" style={{display:'contents'}}>
                          <p style={{fontColor:"#58585e", fontSize:"18px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold',marginTop: '20px'}}>สถานะห้องอบไข่ {hatch.egg_name}</p>
                          <div className="border w-25 mb-4" style={{height: '30px', borderRadius: '4px',marginTop: '20px', backgroundColor: hatch.egg_lossc === "ปกติ" ? '#09c676' : '#e34849', color:'white', textAlign: 'center', marginLeft: '8px'}}>
                            <p style={{fontColor: "white", fontSize:"18px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', marginTop: '4px'}}> {hatch.egg_lossc} </p>
                          </div>
                        </div>
                      </div>
                        {/* <div>
                          <p className="w-100" style={{fontColor: "white", fontSize:"12px", fontFamily: 'IBM Plex Sans Thai', top: '58%', textAlign: 'center', right: '0%', position: 'absolute'}}>ทำความสะอาดล่าสุดเมื่อ {hatcheryData.recentClean}</p>
                        </div> */}
                    </Col>
                  </Row>
                  <Row className="justify-content-center">
                    <Col md="4" xs="4">
                      <div>
                        <p className="text-center" style={{fontColor:"#58585e", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>อุณหภูมิ</p>
                        <CircularProgressbar 
                        value={this.state.temp} 
                        text={`${this.state.temp}°C`} 
                        styles={buildStyles({
                          pathColor: handleGraphColorTemp(this.state.temp)})} />
                      </div>
                    </Col>
                    <Col md="4" xs="4">
                    <div>
                      <p className="text-center" style={{fontColor:"#58585e", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>ความชื้น</p>
                        <CircularProgressbar 
                        value={this.state.hum} 
                        text={`${this.state.hum}%`} 
                        styles={buildStyles({
                          pathColor: handleGraphColorHum(this.state.hum)})} />
                    </div>
                    </Col>
                  </Row>
                  <Row>
                    <p className="text-center ml-4 mt-4" style={{fontColor:"#58585e", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>แจ้งจิ้งหรีดหลุด</p>
                  </Row>
                  <Row>
                    <div className="row mt-3" style={{position: "relative", bottom: "0px", width: '80%', zIndex: '100', height: '50px', boxShadow: "-2px 2px 11px -6px #333", borderRadius: '12px', left:"15%"}}>
                      <div className="col" style={{backgroundColor: hatch.egg_lossc === "ปกติ" ? "#09c676" : "white", padding: '0px 0px 0px 0px', display: 'relative', borderTopLeftRadius: '12px', borderBottomLeftRadius: '12px'}} onClick={this.onClick1}>
                        <p style={{color:  hatch.egg_lossc === "ปกติ" ? "white" : "gray", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>ปกติ</p>
                      </div>
                      <div className="col" style={{backgroundColor: hatch.egg_lossc === "ไม่ปกติ" ? "#e34849" : "white", padding: '0px 0px 0px 0px', display: 'relative', borderTopRightRadius: '12px', borderBottomRightRadius: '12px'}} onClick={this.onClick2}>
                        <p style={{color: hatch.egg_lossc === "ไม่ปกติ" ? "white" : "gray", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)"}}>จิ้งหรีดหลุด</p>
                      </div>
                    </div>
                  </Row>
                  <Row>
                    {/* <p className="text-center ml-4 mt-4" style={{fontColor:"#58585e", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>พนักงานประจำห้องอบไข่</p> */}
                  </Row>
                  <Row className="mb-3">
                    {/* <p className="text-center ml-4 mt-2" style={{fontColor:"#58585e", fontSize:"12px", fontFamily: 'IBM Plex Sans Thai'}}> {this.renderWorkers(hatcheryData.workers)} </p> */}
                  </Row>
                </CardBody>
                <CardFooter style={{height: '56px', background: 'linear-gradient(90deg, rgba(34,134,249,1) 0%, rgba(5,214,187,1) 100%)', borderBottomRightRadius: '12px', borderBottomLeftRadius: '12px', display: 'flex', flexWrap:'wrap', width: '100%' }} onClick={() => this.setState({isOpen: !isOpen})}>
                    <p style={{color: "white", width: '50%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '17px', marginTop: '15px', position: 'relative', left:'22%'}}>กล่องทั้งหมด</p>
                    <i className= {isOpen === false ? "nc-icon nc-minimal-down" : "nc-icon nc-minimal-up"} style={{color:'white', position: "relative", left: '14%', top: '50%', fontWeight: 'bold'}} />
                </CardFooter>
              </Card>
            </Col>
            <Collapse isOpen={this.state.isOpen} className="w-100">
              {this.renderBoxCards(this.state.allbox)}
            </Collapse>
        </Row>
        <AdminNavBar pageName={"ห้องอบไข่"}/>
        {(Cookies.get('workerType') === "1") ? <WorkerFooter /> : <AdminFooter/> }
        
        </div>
      </>
    );
  }
}

export default withRouter(Hatchery);
