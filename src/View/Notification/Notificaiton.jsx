import React, { useState, useEffect } from "react";
import {
  Badge,
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Row,
  Col,
  Collapse,
} from "reactstrap";
import view_icon from "../../Assets/img/icon-general-view.png";
// import ico_css_plus from '../../Assets/img/ico-css-plus.png'
import axiosInstance from "../../Lib/AxiosInterceptor";
import moment from "moment";

import _ from "lodash";
import { observer } from "mobx-react-lite";
import UIstore from "../../Stores/UIstore";
import { useContext } from "react";
import Appmodal from "../../Components/Modals/Appmodal";
import icon_edit from "../../Assets/img/ico-css-edit.png";
import icon_downarrow from "../../Assets/img/dropdown-down.png";
import checkIcon from "../../Assets/img/fb-check-a@2x.png";
import { Dropdown } from "react-bootstrap";

const Notification = observer((props) => {
  const { pageName, poundsNoti, farmNoti, hatchNoti, boxNoti } = props;
  const [Tab, setTab] = React.useState(1);
  const [dailyList, setDailyList] = useState([]);
  const [notiList, setNotiList] = useState([]);
  const [selectedNoti, setSelectedNoti] = useState([]);
  //dropdown
  const [value, setValue] = useState("");
  const [status, setStatus] = useState([
    {
      label: "รอแก้ไข",
      value: "0",
    },
    {
      label: "เสร็จสิ้น",
      value: "1",
    },
  ]);

  const uistore = useContext(UIstore);
  //   const [data, setData] = useState([
  //       {
  //         insectariumId: 1,
  //         pondId: 4,
  //         issue: "จิ้งหรีดตาย"
  //     },
  //       {
  //         insectariumId: 3,
  //         // pondId: 7,
  //         issue: "แอมโมเนีย"
  //       }
  //   ])

  useEffect(() => {
    const fetch = async () => {
      await getNotifications();
    };

    fetch();
    console.log("tab", Tab);
  }, [Tab, selectedNoti, value]);

  const calPoundList = (arr) => {
    let ans = [];
    let firstval;

    for (let [i, num] of arr.entries()) {
      if (num + 1 === arr[i + 1]) {
        if (!firstval) firstval = num;
      } else {
        if (firstval) {
          if (firstval !== num) {
            ans.push(`${firstval}-${num}`);
          }
        } else {
          ans.push(num.toString());
        }

        firstval = undefined;
      }
    }

    return ans;
  };

  const CustomToggle = React.forwardRef((props, ref) => (
    <div
      className="d-flex flex-row justify-content-between"
      ref={ref}
      onClick={(e) => {
        e.preventDefault();

        props.onClick(e);
      }}
    >
      {props.children}
      <img src={icon_downarrow} alt="" className="icon-mini" />
    </div>
  ));
  const CustomMenu = React.forwardRef((props, ref) => {
    return (
      <div
        ref={ref}
        style={props.style}
        className={props.className}
        aria-labelledby={props.labeledBy}
        onClick={() => {
          console.log(":props ;:", props.key);
        }}
      >
        {props.children}
      </div>
    );
  });

  const getNotifications = async () => {
    // if (pageName === "Dashboard") {
    //     console.log('pageName', pageName);
    //     //axious blabla
    // } else {
    //   console.log('pageName2', pageName);
    //     //axious blabla
    // }
    if (Tab === 1) {
      let req = await axiosInstance.get(
        "/manager/work/allworkintoday",
        // "/all/notification/allnotification/0/0",
        {
          headers: { authorization: localStorage.getItem("token") },
        }
      );
      console.log("req ", req);
      setDailyList(req.data);
    }
    if (Tab === 2) {
      let req2 = await axiosInstance.get(
        "/all/problem/allproblem/0",
        // "/all/notification/allnotification/0/1",
        {
          headers: { authorization: localStorage.getItem("token") },
        }
      );
      console.log("req2 ", req2);
      setNotiList(req2.data);
    }
  };

  const handleDisplayTime = (time) => {
    if (!time) return;
    return time.split("T")[1].slice(0, 5);
  };

  const handlePoundList = (arr) => {
    if (!arr) return;
    return arr.map((pound) => {
      return (
        <span className="sarabun_M_300 text_dimgray">
          {pound.insectarium_name}: บ่อที่ {calPoundList(pound.pound_name)}
        </span>
      );
    });
  };

  const handleOnclickNoti = async (id, type) => {
    let target;
    setValue(null);
    if (type === "daily") {
      target = dailyList.filter((val) => val.id === id);
      setSelectedNoti(target[0]);
    } else if (type === "urgent") {
      target = notiList.filter((val) => val.id === id);
      setSelectedNoti(target[0]);
    }

    console.log("target ", target);
    uistore.setEditNotiModal(!uistore.editNotiModal);
  };

  const sumPounds = (arr) => {
    if (!arr) return;
    let sum = 0;

    for (const pound of arr) {
      sum += pound.pound_name.length;
    }

    return sum;
  };

  const handleTimeMoment = (time) => {
    let res = {};
    let days = {
      Monday: "จันทร์",
      Tuesday: "อังคาร",
      Wednesday: "พุธ",
      Thursday: "พฤหัสบดี",
      Friday: "ศุกร์",
      Saturday: "เสาร์",
      Sunday: "อาทิตย์",
    };
    res["date"] = moment(time).format("DD");
    res["day"] = moment(time).format("dddd");
    return res;
  };

  const handleDDStatus = (status) => {
    switch (status) {
      case "0":
        return "รอแก้ไข";
      case "1":
        return "เสร็จสิ้น";
      default:
        break;
    }
  };

  const onSaveNotiStatus = async () => {
    const noId = selectedNoti.id;
    const status = value;

    const req = await axiosInstance.post(
      `all/problem/updatestatusproblem/${noId}/${status}`
      // {
      //   headers: { authorization: localStorage.getItem("token") },
      // }
    );

    console.log("savenoti ", req);
  };

  const renderNotiList = () => {
    return (
      <div className="card-noti d-flex flex-column align-items-center pt-4">
        <div className="tab-container d-flex flex-row justify-content-between align-items-center">
          <div
            onClick={() => {
              setTab(1);
            }}
            className={`d-flex align-items-center justify-content-center ${
              Tab == 1 ? "tab-active " : "tab-unactive"
            }`}
          >
            <span
              className={`${
                Tab == 1
                  ? "prompt_L_600 text_dodgerblue "
                  : "prompt_L text_dimgray"
              }`}
            >
              งานเพาะเลี้ยง
            </span>
            <span className=""></span>
          </div>
          <div
            onClick={() => {
              setTab(2);
            }}
            className={`d-flex align-items-center justify-content-center ${
              Tab == 2 ? "tab-active " : "tab-unactive"
            }`}
          >
            <span
              className={`${
                Tab == 2
                  ? "prompt_L_600 text_dodgerblue "
                  : "prompt_L text_dimgray"
              }`}
            >
              การแจ้งเตือน
            </span>
          </div>
        </div>

        {Tab == 1 ? (
          <div className="w-100">
            <div className="px-4 w-100 d-flex flex-row justify-content-between align-items-center">
              <span className="prompt_XXL text_blueteal">วันนี้</span>

              <div className="d-flex flex-row justify-content-center align-items-center">
                <img
                  src={view_icon}
                  alt=""
                  width={30}
                  height={30}
                  className="mr-2"
                />

                <a
                  href={"/admin/Notiscreen"}
                  className="prompt_M text_dodgerblue"
                >
                  ดูทั้งหมด
                </a>
              </div>
            </div>
            {dailyList.length === 0 ? (
              <div className="mt-3 d-flex flex-column w-100 border-bottom pb-3">
                <span className="prompt_XL text_blueteal align-self-center">
                  {" "}
                  ไม่พบการแจ้งเตือน
                </span>
              </div>
            ) : (
              dailyList.map((datum, index) => {
                if (index <= 3) {
                  return (
                    <div
                      key={index}
                      className="d-flex flex-column w-100 border-bottom pb-3"
                      onClick={() => handleOnclickNoti(datum.id, "daily")}
                    >
                      <div className="my-3 d-flex flex-column px-4">
                        <span className="prompt_L text_blueteal">
                          {" "}
                          {datum.name}{" "}
                        </span>
                        {/* <span className="sarabun_M_300 text_dimgray"> */}
                        {/* {" "} */}
                        {handlePoundList(datum.poundlist)}
                        {/* </span> */}
                      </div>
                      <div className="d-flex flex-row justify-content-between align-items-center w-100 px-4">
                        <div className="d-flex flex-row align-items-center">
                          <div
                            className="mr-3"
                            style={{
                              width: 30,
                              height: 30,
                              borderRadius: 100,
                              backgroundColor: "#ff0",
                            }}
                          ></div>
                          <span className="sarabun_M_300 ">
                            {datum.worker_name}
                          </span>
                        </div>
                        <span className="sarabun_M_300 text_dimgray">
                          {handleDisplayTime(datum.time)}
                        </span>
                      </div>
                    </div>
                  );
                }
              })
            )}
            <div
              onClick={() => {
                uistore.SetCreatemodal(true);
              }}
              className="w-100 py-3 d-flex justify-content-center align-items-center "
            >
              <div className="d-flex flex-row align-items-center">
                <div className="add-circle-orange  d-flex align-items-center justify-content-center mr-3"></div>

                <span className="prompt_XL ">เพิ่มการแจ้งเตือน</span>
              </div>
            </div>
          </div>
        ) : null}

        {Tab == 2 ? (
          <div className="w-100">
            <div className="px-4 w-100 d-flex flex-row justify-content-between align-items-center">
              <span className="prompt_XXL text_blueteal">รอตรวจสอบ</span>

              <div className="d-flex flex-row justify-content-center align-items-center">
                <img
                  src={view_icon}
                  alt=""
                  width={30}
                  height={30}
                  className="mr-2"
                />

                <a
                  href={"/admin/Notiscreen"}
                  className="prompt_M text_dodgerblue"
                >
                  ดูทั้งหมด
                </a>
              </div>
            </div>

            <div className="px-4 border-bottom">
              {notiList.length === 0 ? (
                <div className="mt-3 d-flex flex-column w-100 border-bottom pb-3">
                  <span className="prompt_XL text_blueteal align-self-center">
                    {" "}
                    ไม่พบการแจ้งเตือน
                  </span>
                </div>
              ) : (
                notiList.map((datum, index) => {
                  if (index <= 4) {
                    // return (
                    //   <div
                    //     key={index}
                    //     className="d-flex flex-column w-100 pb-3 card-mention-green my-4"
                    //   ></div>
                    // );
                    return (
                      <div
                        key={index}
                        className="d-flex flex-column w-100 border-bottom pb-3"
                        onClick={() => handleOnclickNoti(datum.id, "urgent")}
                      >
                        <div className="my-3 d-flex flex-column px-4">
                          <span className="prompt_L text_blueteal">
                            {" "}
                            {datum.name}{" "}
                          </span>{" "}
                          {handlePoundList(datum.poundlist)}
                        </div>
                        <div className="d-flex flex-row justify-content-between align-items-center w-100 px-4">
                          <div className="d-flex flex-row align-items-center">
                            <div
                              className="mr-3"
                              style={{
                                width: 30,
                                height: 30,
                                borderRadius: 100,
                                backgroundColor: "#ff0",
                              }}
                            ></div>
                            <span className="sarabun_M_300 ">
                              {datum.worker_name}
                            </span>
                          </div>
                          <span className="sarabun_M_300 text_dimgray">
                            {handleDisplayTime(datum.time)}
                          </span>
                        </div>
                      </div>
                    );
                  }
                })
              )}
            </div>

            <div
              onClick={() => {
                uistore.SetCreatemodal(true);
              }}
              className="w-100 py-3 d-flex justify-content-center align-items-center "
            >
              <div className="d-flex flex-row align-items-center">
                <div className="add-circle-orange d-flex align-items-center justify-content-center "></div>

                <span className="prompt_XL ml-3"> เพิ่มการแจ้งเตือน</span>
              </div>
            </div>
          </div>
        ) : null}
        <Appmodal
          visible={uistore.editNotiModal}
          toggle_click={() => {
            uistore.setEditNotiModal(!uistore.editNotiModal);
            setValue(null);
          }}
          nocontainer={true}
          className={"d-flex justify-content-center align-items-center"}
        >
          <div
            className="d-flex flex-column "
            style={{
              width: "100%",
              minHeight: 600,
              backgroundColor: "#fff",
            }}
          >
            <div
              className={
                Tab === 1
                  ? "head-edit-noti-modal position-relative"
                  : "head-edit-noti-alert-modal position-relative"
              }
            >
              <div className="prompt_XXL text_white mt-4 text-center position-absolute edit-noti-name">
                {selectedNoti.name}
              </div>
              <div
                className={
                  Tab === 1
                    ? "prompt_XL text_greenteal mt-4 text-center position-absolute edit-noti-time"
                    : "prompt_XL text_redteal mt-4 text-center position-absolute edit-noti-time"
                }
              >
                {handleDisplayTime(selectedNoti.time)}
              </div>
              <div className="edit-noti-date position-absolute">
                <div className="position-relative">
                  <div className="text-center">
                    <span className="prompt_XXL text_blueteal mt-4 text-center">
                      {handleTimeMoment(selectedNoti.time).date}
                    </span>
                  </div>
                  <div className="text-center">
                    <span className="prompt_XXL text_blueteal mt-4 text-center">
                      {handleTimeMoment(selectedNoti.time).day}
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className="px-4 d-flex flex-column position-relative">
              <span
                className="prompt_XL text_blueteal mt-4 text-left"
                style={{
                  lineHeight: 1.13,
                }}
              >
                จำนวน {sumPounds(selectedNoti.poundlist)} บ่อ
              </span>
              {localStorage.getItem("role") === "0" ||
              localStorage.getItem("id") === selectedNoti.worker_id ? (
                <div
                  onClick={() => {
                    // uistore.setAssignTaskModal(!uistore.assignTaskModal);
                  }}
                  className="d-flex flex-row justify-content-center align-items-center"
                  style={{
                    position: "absolute",
                    top: 20,
                    right: 15,
                  }}
                >
                  <img src={icon_edit} alt="" className="icon-mini" />
                  <span className="sarabun_L_100 text_blueteal">
                    แก้ไขข้อมูล
                  </span>
                </div>
              ) : null}

              <span className="sarabun_L_300 text_dimgray my-3 mb-4 text-left d-flex flex-column">
                {handlePoundList(selectedNoti.poundlist)}
              </span>

              <div className="align-self-center cardcolor-lightcyan d-flex flex-column">
                <span className="prompt_XL text_black mb-2">Note</span>

                <div className="d-flex flex-row align-items-center">
                  {/* <span className="sarabun_M text_black">โรงเลี้ยง 01 : </span> */}
                  <span className="sarabun_M_300 text_black ml-1">
                    {selectedNoti.note}
                  </span>
                </div>
              </div>
              <div className=" line-dash not-absolute mt-5" />
              <div className="position-relative mt-3">
                <span
                  className="prompt_XL text_blueteal mt-4 text-left"
                  style={{
                    lineHeight: 1.13,
                  }}
                >
                  สถานะ
                </span>
                <div
                  className="d-flex flex-row justify-content-center align-items-center"
                  style={{
                    position: "absolute",
                    top: 0,
                    right: 5,
                  }}
                >
                  {value === "1" ? (
                    <img
                      src={checkIcon}
                      alt=""
                      width={30}
                      height={30}
                      className="mr-2"
                    />
                  ) : null}

                  <Dropdown
                    onSelect={(e) => {
                      console.log("e", e);
                      setValue(e);
                    }}
                  >
                    <Dropdown.Toggle
                      as={CustomToggle}
                      id="dropdown-custom-components"
                    >
                      <span className="sarabun_L_300 text_black">
                        {handleDDStatus(value) ||
                          handleDDStatus(selectedNoti.status)}
                      </span>
                    </Dropdown.Toggle>
                    <Dropdown.Menu as={CustomMenu}>
                      {status.map((item) => {
                        return (
                          <Dropdown.Item
                            disabled={
                              localStorage.getItem("role") === "0" ||
                              localStorage.getItem("id") ===
                                selectedNoti.worker_id
                                ? false
                                : true
                            }
                            key={item.label}
                            eventKey={item.value}
                          >
                            {item.label}
                          </Dropdown.Item>
                        );
                      })}
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>
              <div
                onClick={async () => {
                  // uistore.SetduplicateBreedmodal(!uistore.duplicateBreedmodal);
                  await onSaveNotiStatus();
                  await getNotifications();
                  uistore.setEditNotiModal(!uistore.editNotiModal);
                }}
                className="mt-5 btn-confirm-100 d-flex justify-content-center align-items-center"
                style={{
                  marginTop: -10,
                }}
              >
                <span className="prompt_XL text_white">บันทึก</span>
              </div>
            </div>
          </div>
        </Appmodal>
      </div>
    );
  };

  return renderNotiList();
});

export default Notification;
