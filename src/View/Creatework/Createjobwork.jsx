import React, { useContext, useEffect, useState } from "react";
import Rootstore from "../../Stores/Rootstore";
import axios from "axios";
import icon_leftarrow from "../../Assets/img/dropdown-left.png";
import icon from "../../Assets/img/dropdown-left-white.png";
import icon_attention from "../../Assets/img/icon-attention.png";
import icon_menu from "../../Assets/img/menu.png";
import icon_dots from "../../Assets/img/dots.png";

import icon_search from "../../Assets/img/ico-search3x.png";
import icon_lottask from "../../Assets/img/icon-nav-lot-task.png";
import icon_calendar from "../../Assets/img/icon-nav-calendar.png";
import img_main002 from "../../Assets/img/main-bg-002.png";

import { observer } from "mobx-react-lite";
import Appmodal from "../../Components/Modals/Appmodal";
import Createworkstore from "../../Stores/Createworkstore";
import axiosInstance from "../../Lib/AxiosInterceptor";
import _ from "lodash";

export const Createjobwork = observer(() => {
  const [isTab0, setisTab0] = useState(true);
  const [isTab1, setisTab1] = useState(true);
  const [isSelect_type, setisSelect_type] = useState(0);
  const [active_input, setactive_input] = useState(null);

  const [selectjobdis, setselectjobdis] = useState(0);

  const [typingTimeout, setTypingTimeout] = useState(0);
  const [typing, setTyping] = useState(false);
  const [name, setname] = useState("");
  const [jobformat_name, setjobformat_name] = useState("");
  const [job_title, setjob_title] = useState("");

  const createworkstore = useContext(Createworkstore);

  const [joblist, setjoblist] = useState([]);
  const [everylist, seteverylist] = useState([]);
  const [workdetail, setworkdetail] = useState([]);

  useEffect(() => {
    getshow_all_work(1, 0);
  }, []);

  const getshow_all_work = async (farm_id, type) => {
    try {
      // const all_work = await axiosInstance.get(`/worktemplate/allworktem/1/${type}`)
      const all_work = await axiosInstance.get(`/manager/work/allworktemplate`);
      console.log("all_work", all_work.data[0].array);
      setjoblist(_.get(all_work, "data[0].array[0][0]", []));
      seteverylist(_.get(all_work, "data[0].array[1][0]", []));
    } catch (err) {
      console.error("jhgjhgjughjg", err.message);
    }
  };

  const creatework_template = async (farm_id, type) => {
    try {
      const work_template = await axiosInstance.post(
        `/worktemplate/createworktem`,
        {
          name: jobformat_name,
          type: selectjobdis,
          fid: 1,
          workdetail: workdetail,
        }
      );
      console.log("work_template", work_template);
      setworkdetail([]);
      setjob_title("");
    } catch (err) {
      console.error("jhgjhgjughjg", err.message);
    }
  };

  const Searchwork = async (farm_id, name) => {
    try {
      const Searchwork = await axiosInstance.get(
        `/worktemplate/searchwork/1/${name}`
      );
      var data_work = Searchwork.data[0].array;

      var datatype_0 = [];
      var datatype_1 = [];
      for (let index = 0; index < data_work.length; index++) {
        const element = data_work[index];

        if (element.type == 0) {
          datatype_0.push(element);
        } else {
          datatype_1.push(element);
        }
      }

      setjoblist(datatype_0);
      seteverylist(datatype_1);
    } catch (err) {
      console.error("jhgjhgjughjg", err.message);
    }
  };

  const renderJobdis_select = () => {
    if (selectjobdis == 0) {
      return (
        <div className="d-flex align-items-center justify-content-center mt-4 mb-3">
          <input className="input-jobdisbox" placeholder="X X X X X" />
        </div>
      );
    } else {
      return (
        <div className="d-flex align-items-center justify-content-center mt-4 mb-3">
          <div className="checklist-box">
            <div className="d-flex flex-row align-items-center mr-3">
              <img src={icon_attention} alt="" className="icon-default mr-1" />
              <span className="sarabun_L ">Yes</span>
            </div>

            <div
              className="mx-5"
              style={{
                width: 2,
                height: 15,
                backgroundColor: "#cdd9e9",
              }}
            />

            <div className="d-flex flex-row align-items-center justify-content-center ml-3">
              <span className="sarabun_L ">No</span>
            </div>
          </div>
        </div>
      );
    }
  };

  const handleSelectWork = (name, id) => {
    createworkstore.SetSelectedWork({ name, id });
    createworkstore.getWorkInputs();
    createworkstore.Setmodal_jobformat(!createworkstore.modal_jobformat);
  };

  return (
    <div className="content p-0">
      <Appmodal
        visible={createworkstore.modal_jobformat}
        toggle_click={() => {
          createworkstore.Setmodal_jobformat(!createworkstore.modal_jobformat);
        }}
        nocontainer={false}
        className={
          "d-flex justify-content-center align-items-center createjobwork-layout"
        }
      >
        <div
          className="d-flex flex-column px-3 py-4 position-relative"
          style={{
            width: "100%",
            minHeight: "100vh",
            backgroundColor: "#fff",
          }}
        >
          <div className="d-flex flex-row justify-content-between align-items-center w-100">
            <div
              onClick={() => {
                createworkstore.Setmodal_jobformat(
                  !createworkstore.modal_jobformat
                );
              }}
              className="d-flex flex-row justify-content-center align-items-center"
              style={{}}
            >
              <img src={icon_leftarrow} alt="" className="icon-mini" />
              <span className="sarabun_L_100 text_dimgray">BACK</span>
            </div>

            <div
              onClick={() => {
                // uistore.SetBreedmodal(!uistore.Breedmodal)
              }}
              className="d-flex flex-row justify-content-center align-items-center"
              style={{}}
            >
              <img
                src={icon_search}
                // className={`active: activeWith == 1`}
                className={`img-search-icon ${
                  active_input == false ? "active" : ""
                }`}
                onClick={() => {
                  setactive_input(!active_input);
                }}
                style={{
                  zIndex: 88,
                }}
              />

              <input
                type="text"
                value={name}
                onBlur={() => {
                  setactive_input(!active_input);
                }}
                onChange={(event) => {
                  const self = this;

                  if (typingTimeout) {
                    clearTimeout(typingTimeout);
                  }

                  setname(event.target.value);
                  setTyping(false);
                  setTypingTimeout(
                    setTimeout(function () {
                      Searchwork(1, name);
                    }, 1000)
                  );
                }}
                className={`input-transition ${
                  active_input == false ? "active" : ""
                }`}
              />
            </div>
          </div>

          <div className="d-flex flex-row list-job">
            <div
              onClick={() => {
                setisTab0(!isTab0);
                getshow_all_work(1, 0);
              }}
              className={` mr-4 ${
                isTab0 == true ? "status-blueteal" : "status-paleturquoise"
              }`}
            >
              <span
                className={`prompt_XL ${
                  isTab0 == true ? "text_white" : "text_dodgerblue"
                }`}
              >
                งานเพาะเลี้ยง
              </span>
            </div>

            <div
              onClick={() => {
                setisTab1(!isTab1);
                getshow_all_work(1, 1);
              }}
              className={`${
                isTab1 == true ? "status-blueteal" : "status-paleturquoise"
              }`}
            >
              <span
                className={`prompt_XL ${
                  isTab1 == true ? "text_white" : "text_dodgerblue"
                }`}
              >
                งานประจำวัน
              </span>
            </div>
            {/* <div className='status-paleturquoise mr-4'>

                        </div> */}
          </div>

          <div
            onClick={() => {
              createworkstore.Setmodal_createjobformat(
                !createworkstore.modal_createjobformat
              );
            }}
            className="mt-4 d-flex flex-row align-items-center justify-content-center"
          >
            <div className="add-circle-darkturquoise mr-3">
              <span className="text_white"></span>
            </div>
            <span className="prompt_XL text_black">สร้างรูปแบบงานใหม่</span>
          </div>

          {joblist != "" ? (
            <div className="d-flex flex-column mt-4">
              <div className="d-flex flex-row align-items-center">
                <div className="d-flex flex-row align-items-center">
                  <img
                    src={icon_lottask}
                    className="icon-default mr-2"
                    alt=""
                  />
                </div>
                <div className="d-flex flex-row align-items-center justify-content-between w-100">
                  <span className="prompt_XL text_black">งานเพาะเลี้ยง</span>
                  <span className="sarabun_L_300 text_dimgray">
                    {joblist.worklist.length} รายการ
                  </span>
                </div>
              </div>

              {_.get(joblist, "worklist", []).map((item, index) => {
                return (
                  <div
                    className="d-flex flex-row align-items-center mt-3"
                    onClick={() => handleSelectWork(item.name, item.id)}
                  >
                    <div className="d-flex flex-row align-items-center">
                      <img src={icon} className="icon-default mr-2" alt="" />
                    </div>
                    <div className="d-flex flex-row align-items-center justify-content-between  border-bottom  w-100 pb-3">
                      <span className="prompt_XL text_black">{item.name}</span>
                    </div>
                  </div>
                );
              })}
            </div>
          ) : null}

          {everylist != "" ? (
            <div className="d-flex flex-column mt-4">
              <div className="d-flex flex-row align-items-center">
                <div className="d-flex flex-row align-items-center">
                  <img
                    src={icon_calendar}
                    className="icon-default mr-2"
                    alt=""
                  />
                </div>
                <div className="d-flex flex-row align-items-center justify-content-between w-100">
                  <span className="prompt_XL text_black">งานประจำวัน</span>
                  <span className="sarabun_L_300 text_dimgray">
                    {everylist.length} รายการ
                  </span>
                </div>
              </div>

              {_.get(everylist, "worklist", []).map((item, index) => {
                return (
                  <div
                    className="d-flex flex-row align-items-center mt-3"
                    onClick={() => handleSelectWork(item.name, item.id)}
                  >
                    <div className="d-flex flex-row align-items-center">
                      <img src={icon} className="icon-default mr-2" alt="" />
                    </div>
                    <div className="d-flex flex-row align-items-center justify-content-between  border-bottom  w-100 pb-3">
                      <span className="prompt_XL text_black">{item.name}</span>
                    </div>
                  </div>
                );
              })}
            </div>
          ) : (
            "wtf"
          )}
        </div>
      </Appmodal>

      <Appmodal
        visible={createworkstore.modal_createjobformat}
        toggle_click={() => {
          createworkstore.Setmodal_createjobformat(
            !createworkstore.modal_createjobformat
          );
        }}
        nocontainer={false}
        className={
          "d-flex justify-content-center align-items-center createjobwork-layout"
        }
      >
        <div
          className="d-flex flex-column  py-4 position-relative"
          style={{
            width: "100%",
            minHeight: "100vh",
            backgroundColor: "#fff",
          }}
        >
          <div className="d-flex flex-row justify-content-between align-items-center w-100 px-3">
            <div
              onClick={() => {
                createworkstore.Setmodal_createjobformat(
                  !createworkstore.modal_createjobformat
                );
              }}
              className="d-flex flex-row justify-content-center align-items-center"
              style={{}}
            >
              <img src={icon_leftarrow} alt="" className="icon-mini" />
              <span className="sarabun_L_100 text_dimgray">BACK</span>
            </div>
          </div>

          <div
            className="px-3 pb-3"
            style={{
              boxShadow: "0 2px 4px 0 rgba(0, 0, 0, 0.1)",
            }}
          >
            <div className="mt-4 d-flex flex-column">
              <span className="prompt_XXL text_blueteal">
                สร้างรูปแบบงานใหม่
              </span>

              <span className="prompt_XL text_black mt-2">ชื่อรูปแบบงาน</span>
            </div>

            <div className="mt-3">
              <input
                value={jobformat_name}
                onChange={(event) => {
                  setjobformat_name(event.target.value);
                }}
                className="input-jobformat w-100"
              />
            </div>

            <div>
              <div className="mt-3 d-flex flex-row ">
                <span className="prompt_XL text_black mt-2">ประเภทงาน</span>

                <img
                  src={icon_attention}
                  alt=""
                  className="icon-default ml-2"
                />
              </div>

              <div className="d-flex flex-row list-job">
                <div
                  onClick={() => {
                    setisSelect_type(0);
                  }}
                  className={` mr-4 ${
                    isSelect_type == 0
                      ? "status-blueteal"
                      : "status-paleturquoise"
                  }`}
                >
                  <span
                    className={` prompt_XL ${
                      isSelect_type == 0 ? "text_white" : "text_dodgerblue"
                    }`}
                  >
                    งานเพาะเลี้ยง
                  </span>
                </div>

                <div
                  onClick={() => {
                    setisSelect_type(1);
                  }}
                  className={` mr-4 ${
                    isSelect_type == 1
                      ? "status-blueteal"
                      : "status-paleturquoise"
                  }`}
                >
                  <span
                    className={` prompt_XL ${
                      isSelect_type == 1 ? "text_white" : "text_dodgerblue"
                    }`}
                  >
                    งานประจำวัน
                  </span>
                </div>
                {/* <div className='status-paleturquoise mr-4'>

                                </div> */}
              </div>
            </div>
          </div>

          <div className="line-bg-gray " />

          <div className="px-3">
            <div className="mt-4 d-flex flex-column">
              <span className="prompt_XXL text_blueteal">รายละเอียดงาน</span>
            </div>
          </div>

          {workdetail == "" ? (
            <div className="d-flex flex-column justify-content-start align-items-center">
              <img src={img_main002} alt="" className="icon-mediumlarge mt-2" />

              <span className="prompt_XL text_silver ">ยังไม่ได้ระบุ</span>

              <span className="sarabun_L_300 text_silver mt-2">
                กรุณาระบุรายละเอียดงาน
              </span>

              <div
                onClick={() => {
                  createworkstore.Setmodal_jobdescription(
                    !createworkstore.modal_jobdescription
                  );
                }}
                className="btn-addjobwork"
              >
                <span className="prompt_XL text_white"> + ระบุเลย </span>
              </div>
            </div>
          ) : (
            <div className="">
              {workdetail.map((item, index) => {
                return (
                  <div>
                    <div className="d-flex flex-row my-3 mx-3 align-items-center">
                      <div>
                        <img src={icon_menu} alt="" className="icon-default" />
                      </div>
                      <div className="d-flex flex-column w-100 ml-3">
                        <div className="d-flex flex-row w-100 justify-content-between align-items-center">
                          <span className="prompt_XL text_black">
                            {item.name}
                          </span>
                          <div style={{}}>
                            <img
                              src={icon_dots}
                              alt=""
                              className="icon-default"
                            />
                          </div>
                        </div>
                        <div className="d-flex align-items-center mt-3 mb-3">
                          {item.worktemplate_type == 0 ? (
                            <input
                              className="input-jobdisbox"
                              placeholder="X X X X X"
                            />
                          ) : (
                            <div className="checklist-box">
                              <div className="d-flex flex-row align-items-center mr-3">
                                <img
                                  src={icon_attention}
                                  alt=""
                                  className="icon-default mr-1"
                                />
                                <span className="sarabun_L ">Yes</span>
                              </div>

                              <div
                                className="mx-5"
                                style={{
                                  width: 2,
                                  height: 15,
                                  backgroundColor: "#cdd9e9",
                                }}
                              />

                              <div className="d-flex flex-row align-items-center justify-content-center ml-3">
                                <span className="sarabun_L ">No</span>
                              </div>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                    <div className="line-dash not-absolute w-100 ml-0 my-0 mb-3" />
                  </div>
                );
              })}

              <div
                onClick={() => {
                  createworkstore.Setmodal_jobdescription(
                    !createworkstore.modal_jobdescription
                  );
                }}
                className="d-flex flex-row align-items-center my-3 mx-3"
              >
                <div className="add-circle-darkturquoise">
                  <span className="text_white"></span>
                </div>
                <span className="prompt_XL text_black ml-3">
                  เพิ่มงานที่ต้องทำ
                </span>
              </div>

              <div className="mx-3">
                <div
                  onClick={() => {
                    creatework_template();
                  }}
                  className="btn-confirm-100 px-2 d-flex justify-content-center align-items-center"
                >
                  <span className="prompt_XL text_white">บันทึก</span>
                </div>
              </div>
            </div>
          )}
        </div>
      </Appmodal>

      <Appmodal
        visible={createworkstore.modal_jobdescription}
        toggle_click={() => {
          createworkstore.Setmodal_jobdescription(
            !createworkstore.modal_jobdescription
          );
        }}
        nocontainer={true}
        className={"d-flex justify-content-center align-items-center"}
      >
        <div
          className="d-flex flex-column  py-4 position-relative"
          style={{
            width: "100%",
            minHeight: 420,
            backgroundColor: "#fff",
          }}
        >
          <div className="px-3 mb-2">
            <span className="prompt_XL text_black">ชื่องาน</span>

            <div className="mt-3">
              <input
                value={job_title}
                onChange={(event) => {
                  setjob_title(event.target.value);
                }}
                className="input-jobformat w-100"
              />
            </div>
          </div>

          <div className="line-dash not-absolute w-100 ml-0 my-4" />

          <div className="px-3 mt-1">
            <span className="prompt_XL text_black">รูปแบบการเก็บข้อมูล</span>

            <div className="create-jobdisbox">
              <div className="d-flex flex-row justify-content-between align-items-center">
                <div
                  onClick={() => {
                    setselectjobdis(0);
                  }}
                  className={`${
                    selectjobdis === 0 ? "btn-freetext-active" : "btn-freetext"
                  }`}
                >
                  <span
                    className={`${
                      selectjobdis === 0
                        ? "prompt_XL text_dodgerblue"
                        : "prompt_XL text_dimgray"
                    }`}
                  >
                    {" "}
                    ช่องกรอกอิสระ{" "}
                  </span>
                </div>
                <div
                  onClick={() => {
                    setselectjobdis(1);
                  }}
                  className={`${
                    selectjobdis === 1
                      ? "btn-checklist-active"
                      : "btn-checklist"
                  }`}
                >
                  <span
                    className={`${
                      selectjobdis === 1
                        ? "prompt_XL text_dodgerblue"
                        : "prompt_XL text_dimgray"
                    }`}
                  >
                    {" "}
                    เช็คลิสท์{" "}
                  </span>
                </div>
              </div>

              {renderJobdis_select()}
            </div>
          </div>

          <div className="d-flex flex-row w-100 px-3 mt-4 ">
            <div
              onClick={() => {
                createworkstore.Setmodal_jobdescription(
                  !createworkstore.modal_jobdescription
                );
              }}
              className="btn-cancel-100 d-flex justify-content-center align-items-center mr-3"
            >
              <span className="prompt_XL text_dodgerblue">ยกเลิก</span>
            </div>

            <div
              onClick={() => {
                createworkstore.Setmodal_jobdescription(
                  !createworkstore.modal_jobdescription
                );
                workdetail.push({
                  name: job_title,
                  worktemplate_type: selectjobdis,
                });
                setworkdetail(workdetail);
              }}
              className="btn-confirm-100 d-flex justify-content-center align-items-center ml-3 "
            >
              <span className="prompt_XL text_white">บันทึก</span>
            </div>
          </div>
        </div>
      </Appmodal>
    </div>
  );
});

// const HomeDash = () => {

// };

export default Createjobwork;
