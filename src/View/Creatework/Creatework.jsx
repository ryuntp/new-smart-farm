import React, { useContext, useEffect, useState, forwardRef } from "react";
import Rootstore from "../../Stores/Rootstore";
import Todostore from "../../Stores/Todostore";
import axios from "axios";
import { observer } from "mobx-react-lite";
import icon_leftarrowwhite from "../../Assets/img/dropdown-left-white.png";
import icon_downarrow from "../../Assets/img/dropdown-down.png";
import icon_calendar from "../../Assets/img/icon-nav-calendar-blue.png";
import icon_history from "../../Assets/img/icon-general-history3x.png";
import icon_leftarrow from "../../Assets/img/dropdown-left.png";
import icon_midelete from "../../Assets/img/mi-delete.png";
import icon_alert from "../../Assets/img/icon-general-alert.png";
import icon_close from "../../Assets/img/close.png";
import Collapse from "../../Components/Collapsed/Collapsed";
import { useLocation } from "react-router-dom";

import Switch from "react-switch";
import { Dropdown } from "react-bootstrap";
import Appmodal from "../../Components/Modals/Appmodal";
import UIstore from "../../Stores/UIstore";
import Createworkstore from "../../Stores/Createworkstore";
import { Radio } from "../../Components/Radio";
import Createjobwork from "./Createjobwork";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Workersearchmodal from "../../Components/Modals/Workersearchmodal";
import CalendarsworkerStore from "../../Stores/CalendarsworkerStore";
import _ from "lodash";
import { toJS } from "mobx";
import { FormGroup, Col, Input } from "reactstrap";
import axiosInstance from "../../Lib/AxiosInterceptor";
import { useHistory } from "react-router-dom";
// const stores = useContext(StoreContext);

export const Creatework = observer(() => {
  let history = useHistory();

  const [isOpen, setIsOpen] = useState(false);
  const [active, setActive] = useState(null);
  const [selected, setSelected] = useState("first");
  const uistore = useContext(UIstore);
  const createworkstore = useContext(Createworkstore);
  const calendarsworkerStore = useContext(CalendarsworkerStore);
  const [isExpanded_filterspecies, setExpanded_filterspecies] = useState(false);

  const [note, setnote] = useState("");
  const [value, setValue] = useState("");
  const [valueWorklot, setValueWorklot] = useState("");
  const [valueStartdate, setvalueStartdate] = useState("");
  const [valueTime_date, setvalueTime_date] = useState("");
  const [valueRepeatdate, setvalueRepeatdate] = useState("");
  const [valueEnd_date, setvalueEnd_date] = useState("");

  const [checkedsplitlot, setcheckedsplitlot] = useState(false);
  const [checkedRepeat, setcheckedRepeat] = useState(false);
  const [Worklot_active, setWorklotactive] = useState(false);
  const [date, setDate] = useState(new Date());
  const [answers, setAnswers] = useState([]);

  const [Work_Arr, setWork_Arr] = useState([
    {
      label: "New York",
      value: "newYork",
    },
    {
      label: "Dublin",
      value: "dublin",
    },
  ]);

  const [Start_date, setStart_date] = useState([
    {
      label: "01-02-2012",
      value: "01-02-2012",
    },
    {
      label: "01-02-2012",
      value: "01-02-2012",
    },
    {
      label: "01-02-2012",
      value: "01-02-2012",
    },
    {
      label: "01-02-2012",
      value: "01-02-2012",
    },
  ]);

  const [End_date, setEnd_date] = useState([
    {
      label: "11-02-2012",
      value: "11-02-2012",
    },
    {
      label: "11-02-2012",
      value: "11-02-2012",
    },
    {
      label: "11-02-2012",
      value: "11-02-2012",
    },
    {
      label: "11-02-2012",
      value: "11-02-2012",
    },
  ]);

  const [Repeat_date, setRepeat_date] = useState([
    {
      label: "ทุกวัน",
      value: "eday",
    },
    {
      label: "ทุกสัปดาห์",
      value: "eweek",
    },
    {
      label: "ทุกเดือน",
      value: "emonth",
    },
    {
      label: "ทุกปี",
      value: "eyear",
    },
    {
      label: "other",
      value: "custom",
    },
  ]);

  const location = useLocation();

  const [Time_date, setTime_date] = useState([
    {
      label: "11:00",
      value: "11:00",
    },
    {
      label: "12:00",
      value: "12:00",
    },
  ]);

  const CustomInput = forwardRef(({ value, onClick }, ref) => (
    <div
      className="drop-downwork-blue d-flex flex-row align-items-center justify-content-between"
      onClick={onClick}
      ref={ref}
    >
      <span>{value}</span>

      <img src={icon_calendar} alt="" className="icon-mini" />
    </div>
  ));

  const CustomEndDateInput = forwardRef(({ value, onClick }, ref) => (
    <div
      className="drop-downwork-white d-flex flex-row align-items-center justify-content-between mt-3"
      onClick={onClick}
      ref={ref}
    >
      <span>{value}</span>

      <img src={icon_calendar} alt="" className="icon-mini" />
    </div>
  ));

  const [Weekly_arr, setWeekly] = useState([
    {
      label: "อา",
      value: "อา",
      select: false,
    },
    {
      label: "จ",
      value: "จ",
      select: false,
    },
    {
      label: "อ",
      value: "อ",
      select: false,
    },
    {
      label: "พ",
      value: "พ",
      select: false,
    },
    {
      label: "พฤ",
      value: "พฤ",
      select: false,
    },
    {
      label: "ศ",
      value: "ศ",
      select: false,
    },
    {
      label: "ส",
      value: "ส",
      select: false,
    },
  ]);

  useEffect(() => {
    console.log(
      "createworkstore.selectedWorkInput ",
      toJS(createworkstore.selectedWorkInput)
    );
    console.log("createworkstore.taskNames ", toJS(createworkstore.taskNames));
    console.log("createworkstore.answers ", toJS(createworkstore.answers));
  }, [
    createworkstore.selectedWorkInput,
    createworkstore.taskNames,
    createworkstore.answers,
  ]);

  const updateselectChanged = (select, index) => (event) => {
    console.log("select : ", select);
  };
  const CustomToggle = React.forwardRef((props, ref) => (
    <div
      className="drop-downwork-blue d-flex flex-row justify-content-between mt-3"
      ref={ref}
      onClick={(e) => {
        e.preventDefault();

        props.onClick(e);
      }}
    >
      {props.children}
      <img src={icon_downarrow} alt="" className="icon-mini" />
    </div>
  ));
  const CustomToggle_white = React.forwardRef((props, ref) => (
    <div
      className="drop-downwork-white d-flex flex-row justify-content-between mt-3"
      ref={ref}
      onClick={(e) => {
        e.preventDefault();

        props.onClick(e);
      }}
    >
      {props.children}
      <img src={icon_downarrow} alt="" className="icon-mini" />
    </div>
  ));

  const CustomMenu = React.forwardRef((props, ref) => {
    return (
      <div
        ref={ref}
        style={props.style}
        className={[props.className]}
        aria-labelledby={props.labeledBy}
        onClick={() => {
          console.log(":props ;:", props.key);
        }}
      >
        {props.children}
      </div>
    );
  });

  const handleChangesplitlot = (checked) => {
    setcheckedsplitlot(checked);
  };

  const handleChangeRepeat = (checked) => {
    setcheckedRepeat(checked);
  };

  const handleDisplayDropdownRepeat = (value) => {
    switch (value) {
      case "eday":
        return "ทุกวัน";

      case "eweek":
        return "ทุกสัปดาห์";

      case "emonth":
        return "ทุกเดือน";

      case "eyear":
        return "ทุกปี";

      case "custom":
        return "กำหนดเอง";

      default:
        return "";
        break;
    }
  };

  const renderlistworklot = () => {
    if (Worklot_active == true) {
      return (
        <div
          onClick={() => {
            setWorklotactive(false);
          }}
          className="drop-downsearch-worklot mt-3 px-3 py-2"
        >
          <div className="d-flex flex-row justify-content-between ">
            <span className="sarabun_XL text_dimgray">ค้นหาลอต</span>
            <img src={icon_downarrow} alt="" className="icon-mini" />
          </div>
          <div className="d-flex flex-row align-items-center justify-content-center mt-3">
            <div className="add-circle-darkturquoise mr-3">
              <span className="text_white"></span>
            </div>
            <span className="prompt_XL text_black">สร้างลอตใหม่</span>
          </div>
          <div className="line-dash not-absolute w-100 ml-0 my-3"></div>

          <div className="d-flex flex-column">
            <div className="d-flex flex-row w-100 justify-content-between align-items-center pb-3">
              <span className="sarabun_XL text_black">ลอต 03</span>
              <span className="sarabun_L_300 text_dimgray">SD301121001</span>
            </div>
            <div className="d-flex flex-row w-100 justify-content-between align-items-center pb-3">
              <span className="sarabun_XL text_black">ลอต 02</span>
              <span className="sarabun_L_300 text_dimgray">SD301121001</span>
            </div>
            <div className="d-flex flex-row w-100 justify-content-between align-items-center pb-3">
              <span className="sarabun_XL text_black">ลอต 01</span>
              <span className="sarabun_L_300 text_dimgray">SD301121001</span>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div
          onClick={() => {
            setWorklotactive(true);
          }}
          className="drop-downwork-blue d-flex flex-row justify-content-between mt-3"
        >
          <span className="sarabun_L_300 text_dimgray"> ลอต 03 </span>
          <img src={icon_downarrow} alt="" className="icon-mini" />
        </div>
      );
    }
  };

  const renderRepeat = () => {
    if (checkedRepeat === false) {
      return (
        <div className="d-flex flex-row justify-content-between align-items-center px-3">
          <div className="d-flex flex-row">
            <img src={icon_history} alt="" className="icon-mini mr-2" />

            <span className="prompt_XL_400 text_dimgray">Repeat</span>
          </div>

          <Switch
            uncheckedIcon={false}
            checkedIcon={false}
            onChange={handleChangeRepeat}
            onColor={"#1e76fe"}
            offColor="#e2e2e2"
            checked={checkedRepeat}
          />
        </div>
      );
    } else {
      return (
        <div
          className="px-3 py-3"
          style={{
            backgroundColor: "#edf5ff",
          }}
        >
          <div className="d-flex flex-row justify-content-between align-items-center ">
            <div className="d-flex flex-row">
              <img src={icon_history} alt="" className="icon-mini mr-2" />

              <span className="prompt_XL_400 text_dodgerblue">Repeat</span>
            </div>

            <Switch
              uncheckedIcon={false}
              checkedIcon={false}
              onChange={handleChangeRepeat}
              onColor={"#1e76fe"}
              offColor="#e2e2e2"
              checked={checkedRepeat}
            />
          </div>

          <div className="d-flex flex-row w-100 mt-3">
            <div className="w-100 mr-2">
              <span className="prompt_XL text_black">รอบซ้ำ</span>
              <Dropdown
                onSelect={(e) => {
                  console.log("e", e);
                  setvalueRepeatdate(e);
                }}
              >
                <Dropdown.Toggle
                  as={CustomToggle_white}
                  id="dropdown-custom-components"
                >
                  <span className="sarabun_L_300 text_black">
                    {handleDisplayDropdownRepeat(valueRepeatdate)}
                  </span>
                </Dropdown.Toggle>
                <Dropdown.Menu as={CustomMenu}>
                  {Repeat_date.map((item) => {
                    return (
                      <Dropdown.Item key={item.label} eventKey={item.value}>
                        {item.label}
                      </Dropdown.Item>
                    );
                  })}
                </Dropdown.Menu>
              </Dropdown>
            </div>

            <div className="w-100 ml-2">
              <span className="prompt_XL text_black">วันที่สิ้นสุด</span>
              <DatePicker
                selected={valueEnd_date ? new Date(valueEnd_date) : null}
                // dateFormat="Pp"
                onChange={(date) => {
                  setvalueEnd_date(date.toISOString());
                }}
                customInput={<CustomEndDateInput />}
              />
            </div>
          </div>

          {valueRepeatdate == "custom" ? (
            <div className="d-flex flex-row justify-content-between align-items-center w-100 mt-3">
              {Weekly_arr.map((item, index) => {
                return (
                  <div
                    key={index}
                    onClick={() => {
                      let newArr = Weekly_arr.map((item, i) => {
                        if (index == i) {
                          item.select = !item.select;
                          return item;
                        } else {
                          return item;
                        }
                      });

                      setWeekly(newArr);
                    }}
                    className={`${
                      item.select === false
                        ? "btn-select-day"
                        : "btn-selected-day"
                    }`}
                  >
                    <span
                      className={`prompt_M  ${
                        item.select === false ? "text_dodgerblue" : "text_white"
                      }`}
                    >
                      {item.label}
                    </span>
                  </div>
                );
              })}
            </div>
          ) : null}
        </div>
      );
    }
  };

  const renderBodyworkerlist = () => {
    return (
      <div>
        <div className="d-flex flex-row justify-content-start mx-3 mb-3">
          {calendarsworkerStore.workerselectdata.map((item, index) => {
            return (
              <div
                key={index}
                className="tag-paleturquoise d-flex flex-row align-items-center justify-content-around pr-3 mr-3"
              >
                <div className="img-person-mini mr-1"></div>

                <span className="sarabun_M text_black">{item.name}</span>
                <div
                  onClick={() => {
                    calendarsworkerStore.removeworkerSelectdata(index);
                  }}
                >
                  <img
                    src={icon_close}
                    className="ml-3"
                    alt=""
                    style={{ width: 9, height: 9 }}
                  />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  };

  const handleCustomDates = (arr) => {
    console.log("arr ", arr);
    let repeatStatus = _.includes(
      ["eday", "eweek", "emonth", "eyear"],
      valueRepeatdate
    )
      ? 0
      : 1;

    if (repeatStatus === 0) return;
    let ans = [];

    for (let [i, date] of arr.entries()) {
      if (date.select === false) {
        ans[i] = 0;
      } else {
        ans[i] = 1;
      }
    }

    console.log("ans ", ans);

    return ans;
  };

  const handleSubmit = async () => {
    let body = {
      work_id: parseInt(createworkstore?.selectedWork?.id) || null,
      worker_id: parseInt(
        _.get(calendarsworkerStore, "workerSingleSelectData[0].id", null)
      ),
      time: date || null,
      workdetail_question: createworkstore.taskNames || [],
      workdetail_answer: createworkstore.answers || [],
      repeat_status: _.includes(
        ["eday", "eweek", "emonth", "eyear"],
        valueRepeatdate
      )
        ? 0
        : 1 || null,
      repeat_option: valueRepeatdate || null,
      enddate_repeate: valueEnd_date || null,
      custom_repeate: handleCustomDates(Weekly_arr) || [],
      note: note,
    };

    console.log("body ", body);

    const createAssign = await axiosInstance.post(
      `/manager/lot/createlot`,
      body
    );

    if (createAssign.status === 200) {
      history.goBack();
    }
  };

  const renderHeadWorkerlist = () => {
    return (
      <div>
        {" "}
        {calendarsworkerStore.workerselectdata.length !== 0 ? (
          <div className="d-flex flex-row align-items-center">
            <span className="prompt_L text_black mr-2">
              {calendarsworkerStore.workerselectdata.length} คน
            </span>
            <div
              onClick={() => {
                calendarsworkerStore.removeallworkerSelectdata();
              }}
              className="cancel_btn"
            >
              X
            </div>
          </div>
        ) : (
          <div
            onClick={() => {
              calendarsworkerStore.Setworkerselectmodal(true);
            }}
            className="d-flex flex-row align-items-center"
          >
            <span className="prompt_L text_black mr-2">
              {_.get(
                calendarsworkerStore,
                "workerSingleSelectData[0].name",
                "ระบุ"
              )}{" "}
            </span>
            <img src={icon_downarrow} className="icon-mini" />
          </div>
        )}
      </div>
    );
  };

  const submitWorker = () => {
    var condi_option = calendarsworkerStore.option;
    condi_option.workerlist = [];
    for (const key in calendarsworkerStore.workerSingleSelectData) {
      if (
        Object.hasOwnProperty.call(
          calendarsworkerStore.workerSingleSelectData,
          key
        )
      ) {
        const element = calendarsworkerStore.workerSingleSelectData[key];
        condi_option.workerlist.push(Number(element.id));
      }
    }
    calendarsworkerStore.Setoption(condi_option);
  };

  const selectWorker = (item) => {
    calendarsworkerStore.addWorkerSingleSelectData(item);
  };

  const handleAnswers = (value, idx) => {
    let newArr = [...createworkstore.answers];
    newArr[idx] = value;
    createworkstore.SetAnswer(newArr);
  };

  const renderManagerTasks = (tasks) => {
    if (!tasks) {
      return;
    }

    return tasks.map((task, index) => {
      console.log("task.type ", task.type_id);
      if (task.type_id === 0) {
        return (
          <Col
            md="12"
            className="mt-2 ml-3"
            id={`m_task_${index}`}
            key={`m_task_${index}`}
          >
            <small
              style={{
                fontSize: "14px",
                fontFamily: "IBM Plex Sans Thai",
                fontWeight: "bold",
              }}
            >
              {task.name}
            </small>
            <FormGroup className="mt-2 ml-2">
              <Input
                placeholder=""
                id={`m_task_${index}`}
                type="checkbox"
                onChange={(e) => {
                  handleAnswers(e.target.checked, index);
                }}
              />
            </FormGroup>
          </Col>
        );
      } else if (task.type_id === 1) {
        return (
          <Col
            md="12"
            className="mt-2 ml-3"
            id={`m_task_${index}`}
            key={`m_task_${index}`}
          >
            <small
              style={{
                fontSize: "14px",
                fontFamily: "IBM Plex Sans Thai",
                fontWeight: "bold",
              }}
            >
              {task.name}
            </small>
            <FormGroup className="mt-2 input-task w-100">
              <Input
                placeholder=""
                id={`m_task_${index}`}
                type="input"
                onChange={(e) => handleAnswers(e.target.value, index)}
              />
            </FormGroup>
          </Col>
        );
      }
    });
  };

  const rendersplitlot = () => {
    if (checkedsplitlot == true) {
      return (
        <div className="list-splitlot">
          <div
            onClick={() => {
              createworkstore.Setmodal_splitlot(
                !createworkstore.modal_splitlot
              );
            }}
            className="btn-add-worklot mr-4"
          ></div>

          <div className="status-blueteal mr-4"></div>

          <div className="status-paleturquoise mr-4"></div>
          <div className="status-blueteal mr-4"></div>
        </div>
      );
    } else {
      return (
        <div className="d-flex flex-row justify-content-between align-items-center">
          <span className="prompt_XL_400 text_dimgray">แบ่งลอตย่อย</span>

          <Switch
            uncheckedIcon={false}
            checkedIcon={false}
            onChange={handleChangesplitlot}
            checked={checkedsplitlot}
          />
        </div>
      );
    }
  };

  return (
    <div className="creatework-layout p-0">
      <div className="head-workmanage">
        <div
          onClick={() => {
            // uistore.SetBreedmodal(!uistore.Breedmodal)
          }}
          className="d-flex flex-row justify-content-center align-items-center"
          style={{
            position: "absolute",
            top: 20,
            left: 15,
          }}
        >
          <img src={icon_leftarrowwhite} alt="" className="icon-mini" />
          <span className="sarabun_L_100 text_white">BACK</span>
        </div>

        <div
          className="d-flex flex-column"
          style={{
            position: "absolute",
            bottom: 23,
            left: 23,
          }}
        >
          <span className="prompt_XXL text_white">มอบหมายงาน</span>
          <span className="sarabun_L text_blueteal">Assign Work</span>
        </div>
      </div>

      <div className="body-workmanage ">
        <div>
          <span className="prompt_XL text_black">งาน</span>
          <div
            onClick={() => {
              createworkstore.Setmodal_jobformat(
                !createworkstore.modal_jobformat
              );
            }}
            className="drop-downwork-blue d-flex flex-row justify-content-between mt-3"
          >
            <span className="sarabun_L_300 text_dimgray">
              {" "}
              {createworkstore?.selectedWork?.name || ""}
            </span>
            <img src={icon_downarrow} alt="" className="icon-mini" />
          </div>
        </div>

        <div className="mt-4">
          <div className="d-flex flex-row justify-content-between align-items-center">
            <span className="prompt_XL text_black">ลอตที่ให้งาน</span>

            <span className="sarabun_M_300 text_dimgray">SD301121001-03A</span>
          </div>
          {renderlistworklot()}
        </div>

        <div className="mt-4">{rendersplitlot()}</div>

        <div className="mt-4">
          <div className="d-flex flex-column justify-content-start align-items-start">
            <span className="sarabun_L text_dimgray mb-3">
              โรงเลี้ยง 01 <span className="sarabun_L_300">: 101-103,109</span>
            </span>

            <span className="sarabun_L text_dimgray">
              โรงเลี้ยง 02 <span className="sarabun_L_300">: 201,208</span>
            </span>
          </div>
        </div>
      </div>

      <div className="line-dash not-absolute w-100 ml-0 my-4"></div>

      <div className="worker-date ">
        <div className="d-flex flex-row w-100">
          <div className="w-100 mr-2">
            <span className="prompt_XL text_black">วันที่เริ่มต้น&เวลา</span>
            <div className="d-flex mt-3 mb-3">
              <div className="col-12">
                <div>
                  <div className="d-flex flex-row">
                    <div className="w-100 mx-3">
                      <DatePicker
                        selected={new Date(date)}
                        timeFormat="HH:mm"
                        dateFormat="Pp"
                        timeIntervals={15}
                        showTimeSelect
                        onChange={(date) => {
                          setDate(date.toISOString());
                        }}
                        customInput={<CustomInput />}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="line-dash not-absolute w-100 ml-0 my-4"></div>

      <div className="mb-5">
        <div
          className="px-4"
          onClick={() => setExpanded_filterspecies((x) => !x)}
        >
          <div className="d-flex flex-row justify-content-between align-items-center pb-4">
            <span className="prompt_XL text_black">คนงาน</span>
            {renderHeadWorkerlist()}
          </div>
        </div>
        <Collapse
          isActive={isExpanded_filterspecies}
          render_children={renderBodyworkerlist()}
        />
        <div className="position-relative mt-1 ">
          <div className=" line-solid not-absolute" />
        </div>
      </div>

      <div className="mt-4">{renderRepeat()}</div>

      <div className="line-dash not-absolute w-100 ml-0 my-4"></div>
      <div>
        <span className="ml-4 text_blueteal prompt_XXL">รายละเอียดงาน</span>
        {/* {_.get(createworkstore, "selectedWorkInput[0].name", "test")} */}
        {renderManagerTasks(_.get(createworkstore, "selectedWorkInput", []))}
      </div>
      <div className="line-dash not-absolute w-100 ml-0 my-4"></div>
      <div className="bottom-content">
        <div
          onClick={() => {
            uistore.SetaddnoteCreateworkmodal(!uistore.addnoteCreateworkmodal);
          }}
          className="d-flex flex-row align-items-center justify-content-between px-3"
        >
          <span className="text_black prompt_XL">หมายเหตุ</span>

          {createworkstore.notecreatework == "" ? (
            <div className="add-circle-darkturquoise">
              <span className="text_white"></span>
            </div>
          ) : (
            <div className="d-flex flex-row align-items-center">
              <span className=" text_black prompt_L mr-3 ">
                {createworkstore.notecreatework}
              </span>
              <div className="add-circle-darkturquoise">
                <span className="text_white"></span>
              </div>
            </div>
          )}
        </div>
        <div className="d-flex flex-row w-100 px-3 mt-3 mb-4">
          <div
            className="btn-confirm-100 d-flex justify-content-center align-items-center mr-3"
            onClick={() => {
              handleSubmit();
            }}
          >
            <span className="prompt_XL text_white">ยืนยันห</span>
          </div>
          <div className="btn-cancel-100 d-flex justify-content-center align-items-center ml-3">
            <span className="prompt_XL text_dodgerblue">ยกเลิก</span>
          </div>
        </div>
      </div>

      <Appmodal
        visible={uistore.addnoteCreateworkmodal}
        toggle_click={() => {
          uistore.SetaddnoteCreateworkmodal(!uistore.addnoteCreateworkmodal);
        }}
        nocontainer={true}
        className={"d-flex justify-content-center align-items-center"}
      >
        <div
          className="d-flex flex-column "
          style={{
            width: "100%",
            minHeight: 600,
            backgroundColor: "#fff",
          }}
        >
          <div className="note-headmodal d-flex align-items-center w-100">
            <div
              onClick={() => {
                uistore.SetaddnoteCreateworkmodal(
                  !uistore.addnoteCreateworkmodal
                );
              }}
              className="d-flex flex-row justify-content-center align-items-center"
            >
              <img src={icon_leftarrow} alt="" className="icon-mini" />
              <span className="sarabun_L_100 text_black">BACK</span>
            </div>
          </div>

          <div className="px-4 mt-4">
            <div className="d-flex flex-row justify-content-between align-items-center">
              <span className="prompt_XL text_black">หมายเหตุ</span>

              <div
                onClick={() => {
                  createworkstore.Setnotecreatework(null);
                  uistore.SetaddnoteCreateworkmodal(
                    !uistore.addnoteCreateworkmodal
                  );
                }}
              >
                <img src={icon_midelete} className="icon-minismall mr-3" />
                <span className="prompt_M_200 text-decoration-underline text_tomato">
                  ลบ
                </span>
              </div>
            </div>
          </div>

          <div className="mx-4 mt-4">
            <textarea
              value={note}
              onChange={(e) => {
                // console.log("e",);
                setnote(e.target.value);
              }}
              className="input-area-addnote"
              id="w3review"
              name="w3review"
              rows="4"
              cols="50"
            ></textarea>
          </div>

          <div className="d-flex flex-row mx-4 justify-content-between mt-4">
            <div
              onClick={() => {
                uistore.SetaddnoteCreateworkmodal(
                  !uistore.addnoteCreateworkmodal
                );
              }}
              className="btn-minicancle d-flex justify-content-center align-items-center"
            >
              <span className="prompt_XL text_dodgerblue">ยกเลิก</span>
            </div>
            <div
              onClick={() => {
                createworkstore.Setnotecreatework(note);
                uistore.SetaddnoteCreateworkmodal(
                  !uistore.addnoteCreateworkmodal
                );
              }}
              className="btn-miniconfirm d-flex justify-content-center align-items-center"
            >
              <span className="prompt_XL text_white">ยืนยัน</span>
            </div>
          </div>
        </div>
      </Appmodal>

      <Appmodal
        visible={createworkstore.modal_splitlot}
        toggle_click={() => {
          createworkstore.Setmodal_splitlot(!createworkstore.modal_splitlot);
        }}
        nocontainer={true}
        className={"d-flex justify-content-center align-items-center"}
      >
        <div
          className="d-flex flex-column px-3 py-4 position-relative"
          style={{
            width: "100%",
            minHeight: 600,
            backgroundColor: "#fff",
          }}
        >
          <span className="prompt_XXL text_blueteal">แบ่งลอตย่อย</span>
          <div className="d-flex flex-row align-items-center justify-content-between mt-2">
            <span className="prompt_XL text_dark">ลอตย่อย</span>
            <span className="prompt_M_300 text_dimgray">SD301121001-01B</span>
          </div>
          <input className="input-splitlot w-100 mt-3" />
          <div className="mt-4 d-flex flex-column">
            <span className="prompt_XL text_dark">โรงเลี้ยง&บ่อ</span>

            <span className="sarabun_L_300 text_orangefat mt-1">
              * บ่อที่ไม่ได้ระบุจะยังอยู่ใน ลอตย่อยก่อนหน้าทั้งหมด
            </span>
          </div>
          <div className="mt-4">
            <div>
              <Radio
                value="บ่อที่เหลือทั้งหมด"
                selected={selected}
                text="บ่อที่เหลือทั้งหมด"
                onChange={setSelected}
              />
            </div>

            <hr />
            <div className="mt-2">
              <Radio
                value="ระบุเอง"
                selected={selected}
                text="ระบุเอง"
                onChange={setSelected}
              />
            </div>
          </div>
          <div
            className="d-flex flex-row justify-content-between mt-4 w-100"
            style={{
              position: "absolute",
              left: 0,
              bottom: 20,
              paddingLeft: 15,
              paddingRight: 15,
            }}
          >
            <div
              onClick={() => {
                createworkstore.Setmodal_splitlot(
                  !createworkstore.modal_splitlot
                );
              }}
              className="btn-minicancle d-flex justify-content-center align-items-center w-50 mr-2"
            >
              <span className="prompt_XL text_dodgerblue">ยกเลิก</span>
            </div>
            <div
              onClick={() => {
                createworkstore.Setmodal_splitlot(
                  !createworkstore.modal_splitlot
                );
              }}
              className="btn-miniconfirm d-flex justify-content-center align-items-center w-50 ml-2"
            >
              <span className="prompt_XL text_white">ยืนยัน</span>
            </div>
          </div>
        </div>
      </Appmodal>

      <Workersearchmodal
        Type={"detail"}
        submitWorker={submitWorker}
        selectedWorker={selectWorker}
      />

      <Createjobwork />
    </div>
  );
});

export default Creatework;
