import React, { useState, useEffect, useContext } from "react";
import axios from 'axios'
// import Cookies from "js-cookie"
import {
  Card,
  CardBody,
  Row,
  Col,
  FormGroup,
  Input,
  Collapse,

} from "reactstrap";

// import {
//   CircularProgressbar,
//   buildStyles,
//   CircularProgressbarWithChildren,
// } from "react-circular-progressbar";
// import "react-circular-progressbar/dist/styles.css";
import UIstore from "../../Stores/UIstore";
import icon_leftarrowwhite from '../../Assets/img/dropdown-left-white.png'
import icon_water from '../../Assets/img/icon-dashboard-water.png'
import icon_food from '../../Assets/img/icon-dashboard-food.png'
import icon_sading01 from '../../Assets/img/bug-sading-01.png'
import moment from 'moment'

import { observer } from 'mobx-react-lite'
import HomedashStore from "../../Stores/HomedashStore";
import axiosInstance from "../../Lib/AxiosInterceptor";
import { useLocation } from "react-router-dom";
const URL = "https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com"

const HomeDashDetail = observer((props, card, index, id) => {
  const [Tab, setTab] = useState(0)
  const [sensorvalue, setvalue] = useState([])
  const [statusproblem, setstatusproblem] = useState(0)
  const [ponds_detail, setponds_detail] = useState([])
  const [problem_list, setproblem_list] = useState([])
  const homedashStore = useContext(HomedashStore)
  const uistore = useContext(UIstore);

  const changestatusproblem = (value) => {
    setstatusproblem(value)
    getproblemsinonepound(homedashStore.insectarium_id, homedashStore.ponds_id, value)
  };

  const getponds_detail = (ponds_id) => {
    axiosInstance.get(`all/place/onepound/${ponds_id}`).then((res) => {
      homedashStore.setponds_detail(res.data[0])
      // setproblem_list(res.data[0])
    })
      .catch((e) => {
        console.log("error : :", e);
      })
  };

  const calPoundList = (arr) => {

    let ans = [];
    let firstval;
    for (let [i, num] of arr.entries()) {
      if ((Number(num) + 1) === Number(arr[i + 1])) {
        if (!firstval) firstval = num;
      } else {
        if (firstval) {
          if (firstval !== num) {
            ans.push(`${firstval}-${num}`);
          }
        } else {
          ans.push(num.toString());
        }

        firstval = undefined;
      }

    }

    return ans;
  };



  const getproblemsinonepound = async (insectarium_id, ponds_id, status) => {
    try {
      const problemsinonepound = await axiosInstance.get(`all/problem/problemsinonepound/${ponds_id}/${status}`)
      setproblem_list(problemsinonepound.data[0].array)
    } catch (err) {
      console.error('', err.message);
    }
  };


  const render_inprogress = () => {
    return (
      <div className='dashboard-list px-3 '>
        <div className='d-flex flex-row w-100 justify-content-between align-items-center py-3'>
          <span className='prompt_XXL text_blueteal'>
            การแจ้งเตือน
          </span>
          <span className='sarabun_M_300 text_dimgray'>
            {problem_list.length} รายการ
          </span>
        </div>
        {problem_list.map((item, index) => {
          return <div className='card-mention-orange mb-4'>

            <div className='d-flex flex-row justify-content-between w-100 align-items-center'>

              <span className='prompt_L text_orangefat'>{item.name}</span>

              <span className='sarabun_M_300 text_orangefat'>X <span className='prompt_XL text_orangefat px-1'>{item.poundCount}</span> บ่อ</span>
            </div>

            <div className='mt-1 d-flex'>
              <span className='sarabun_M text_dimgray '>{homedashStore.ponds_detail.lot_id} :
                {item.poundlist.map((itemin, index) => {
                  return <span className="sarabun_M_300 text_dimgray">
                    &nbsp; {calPoundList(itemin.pound_name)}
                  </span>
                })}
              </span>
            </div>

            <div className='d-flex flex-row justify-content-between align-items-center mt-3'>
              <div className='d-flex flex-row align-items-center'>
                <div className='icon-mini rounded-circle mr-2' style={{ backgroundColor: "#f0f" }}></div>
                <span className='sarabun_M_300'>{item.worker_name}</span>
              </div>

              <span className='sarabun_M_300 text_dimgray'>
                {moment(item.time).format('HH:mm')}
              </span>
            </div>


          </div>

        })}



      </div>
    )

  }

  const render_finish = () => {
    return (
      <div className='dashboard-list px-3 '>

        <div className='d-flex flex-row w-100 justify-content-between align-items-center py-3'>
          <span className='prompt_XXL text_blueteal'>
            ประวัติงาน
          </span>
          <span className='sarabun_M_300 text_dimgray'>
            {problem_list.length} รายการ
          </span>
        </div>
        {problem_list.map((item, index) => {
          return <div className='card-mention-gray mb-4'>
            <div className='d-flex flex-row justify-content-between w-100 align-items-center'>
              <span className='prompt_L text_greenteal'>{item.name}</span>
              <span className='sarabun_M_300 text_greenteal'>X <span className='prompt_XL text_greenteal px-1'>{item.poundCount}</span> บ่อ</span>
            </div>

            <div className='mt-1 d-flex'>
              <span className='sarabun_M text_dimgray '>{homedashStore.ponds_detail.lot_id} :
                {item.poundlist.map((itemin, index) => {
                  return <span className="sarabun_M_300 text_dimgray">
                    &nbsp; {calPoundList(itemin.pound_name)}
                  </span>
                })}
              </span>
            </div>

            <div className='d-flex flex-row justify-content-between align-items-center mt-3'>
              <div className='d-flex flex-row align-items-center'>
                <div className='icon-mini rounded-circle mr-2' style={{ backgroundColor: "#f0f" }}></div>
                <span className='sarabun_M_300'>{item.worker_name}</span>
              </div>

              <span className='sarabun_M_300 text_dimgray'>
                {moment(item.time).format('HH:mm')}
              </span>
            </div>


          </div>
        })}



      </div>
    )
  }

  useEffect(() => {
    const search = props.location.search;
    const Params = new URLSearchParams(search)
    const ponds_id = Params.get('ponds_id');
    const insectarium_id = Params.get('insectarium_id');

    homedashStore.setponds_id(ponds_id)
    homedashStore.setinsectarium_id(insectarium_id);

    getproblemsinonepound(insectarium_id, ponds_id, statusproblem)
    getponds_detail(ponds_id)
    uistore.SetEnableHeader(false)
    uistore.SetEnablesubHeader(false)
  }, []);

  return (
    <div className="" id={index} key={index}>

      <div className='homedash-detail'>
        <div className='head-content'>
          <div
            onClick={() => {

            }}
            className="d-flex flex-row justify-content-center align-items-center"
            style={{
              position: 'absolute',
              top: 20,
              left: 15
            }}>
            <img src={icon_leftarrowwhite} alt='' className='icon-mini' />
            <span className='sarabun_L_100 text_white'>
              BACK
            </span>
          </div>

          <div className='text-content'>
            <span className='prompt_XXL text_white'>
              พร้อมผสมพัน
            </span>

            <span className='sarabun_L text_blueteal mt-2'>
              {homedashStore.ponds_detail.inectarium_name}
            </span>
          </div>


        </div>

        <div className="d-flex main-content flex-column bg-gray">

          <div className='status-cricket'>
            <div className='gray-box-ab'>
              <div className='d-flex flex-row align-items-center px-3'>
                <img src={icon_sading01} className='icon-medium mr-3' />
                <div className='d-flex flex-column'>
                  <span className='prompt_XL'>

                    {homedashStore.ponds_detail.name}
                  </span>
                  <span className='sarabun_L_300 text_dimgray mt-1'>
                    {/* ล็อตเลี้ยง 1 :  */}
                    {homedashStore.ponds_detail.lot_id}
                  </span>
                </div>
              </div>

              <div className='line-dash not-absolute w-100 ml-0 my-3'>

              </div>


              <div className='d-flex flex-row align-items-center justify-content-between px-3'>
                <div className='d-flex flex-row align-items-center'>
                  <div className='mr-4' style={{
                    width: 55,
                    height: 55,
                    borderRadius: 100,
                    backgroundColor: "#ff0",
                  }}>
                  </div>
                  <div className='d-flex flex-column'>
                    <span className='prompt_XL '>
                      {homedashStore.ponds_detail.duration} วัน
                    </span>
                    <span className='sarabun_M_300 text_dimgray'>
                      เริ่ม: {moment(homedashStore.ponds_detail.initdate).format('DD/MM/YY')}
                    </span>
                  </div>
                </div>

                <div className='status-darkturquoise-tag d-flex justify-content-center align-items-center'>
                  <span className='sarabun_m text_white'>
                    เก็บเกี่ยว
                  </span>
                </div>
              </div>
            </div>

            <div className='d-flex flex-row justify-content-between align-items-center w-100'>
              <div className='water-level'>
                <span className='sarabun_L text_dimgray'>น้ำสะสม</span>
                <div className='d-flex justify-content-center align-items-center'>
                  <span className='prompt_SL text_dodgerblue'>
                    {homedashStore.ponds_detail.water}
                  </span>
                  <img src={icon_water} className='icon-default ml-1' />
                </div>

                <span className='sarabun_L_300 text_dimgray'>
                  กก.
                </span>
              </div>
              <div className='food-level '>
                <span className='sarabun_L text_dimgray'>อาหารสะสม</span>


                <div className='d-flex justify-content-center align-items-center'>
                  <span className='prompt_SL text_darkturquoise'>
                    {homedashStore.ponds_detail.food}
                  </span>
                  <img src={icon_food} className='icon-default ml-2' />
                </div>

                <span className='sarabun_L_300 text_dimgray'>
                  กก.
                </span>

              </div>
            </div>
          </div>


          <div
            className='tab-status'>

            <div
              onClick={() => {
                setTab(0)
                changestatusproblem(0)
              }}
              className={`d-flex align-items-center justify-content-center ${(Tab === 0) ? "tab-active " : "tab-unactive"}`}
            >
              <span
                className={` ${(Tab === 0) ? "prompt_L text_dodgerblue" : "prompt_L text_dimgray"}`}
              >กำลังดำเนินการ</span>
            </div>

            <div style={{
              width: 1,
              height: 15,
              border: "solid 1px #cdd9e9",
            }}>

            </div>

            <div
              onClick={() => {
                setTab(1)
                changestatusproblem(1)
              }}
              className={`d-flex align-items-center justify-content-center ${(Tab === 1) ? "tab-active " : "tab-unactive"}`}>
              <span
                className={` ${(Tab === 1) ? "prompt_L text_dodgerblue" : "prompt_L text_dimgray"}`}
              >เสร็จสิ้น</span>
            </div>


          </div>


          {Tab === 0 ? render_inprogress() : null}
          {Tab === 1 ? render_finish() : null}


        </div>


      </div>

      {/* <Row onClick={() => setIsOpen(!isOpen)}>
        <Col xs="12">
          <div
            className="ponds-header">

            <div>
              <p
                className="ponds-text-head">
                {"โรงเลี้ยงที่  "}
                {card.insectarium_name}
              </p>
              <p
                className="ponds-text">
                {"Warehouse  "}
                {card.insectarium_name}
              </p>
            </div>
            <div
              className="border"
              style={{
                height: "25px",
                borderRadius: "4px",
                backgroundColor: farm === "ปกติ" ? "#09c676" : "#e34849",
                color: "white",
                textAlign: "center",
                marginLeft: "8px",
                width: "50px",
                margin: "-2px 0 0 auto",
              }}
            >
              <p
                style={{
                  fontColor: "white",
                  fontSize: "12px",
                  fontFamily: "IBM Plex Sans Thai",
                  fontWeight: "bold",
                  marginTop: "4px",
                }}
              >
                {farm}
              </p>
            </div>
            <div className="mt-1 ml-3" onClick={() => setIsOpen(!isOpen)}>
              <i
                className="nc-icon nc-minimal-down"
                style={{
                  color: "black",
                  fontWeight: "bold",
                  float: "right",
                }}
              />
            </div>
          </div>
       
        </Col>
      </Row> */}
      {/* <Collapse isOpen={isOpen}>



        <Row onClick={() => setIsOpen2(!isOpen2)}>
          <Col xs="12">
            <div
              className="w-100"
              style={{
                display: "flex",
                flexWrap: "wrap",
                marginBottom: "-18px",
                justifyContent: "space-between",
              }}
            >
              <p
                className="ml-3"
                style={{
                  fontColor: "white",
                  fontSize: "16px",
                  fontFamily: "IBM Plex Sans Thai",
                  fontWeight: "bold",
                }}
              >
                ชุดเซนเซอร์ทั้งหมด
              </p>
              <div className="mt-1 ml-3" onClick={() => setIsOpen2(!isOpen2)}>
                <i
                  className="nc-icon nc-minimal-down"
                  style={{
                    color: "black",
                    fontWeight: "bold",
                    float: "right",
                  }}
                />
              </div>
            </div>
            <hr />
          </Col>
        </Row>
        <Collapse isOpen={isOpen2}>

          {setgatesensor()}

        </Collapse>



        <Row>
          <p className="text-center ml-4 mt-4" style={{ fontColor: "#58585e", fontSize: "14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold' }}>แจ้งจิ้งหรีดหลุด</p>
        </Row>
        <Row>
          <div className="row mt-3" style={{ position: "relative", bottom: "0px", width: '80%', zIndex: '100', height: '50px', boxShadow: "-2px 2px 11px -6px #333", borderRadius: '12px', left: "15%" }}>
            <div className="col" style={{ backgroundColor: farm === "ปกติ" ? "#09c676" : "white", padding: '0px 0px 0px 0px', display: 'relative', borderTopLeftRadius: '12px', borderBottomLeftRadius: '12px' }} onClick={onClick1}>
              <p style={{ width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)" }}>ปกติ</p>
            </div>
            <div className="col" style={{ backgroundColor: farm === "ไม่ปกติ" ? "#e34849" : "white", padding: '0px 0px 0px 0px', display: 'relative', borderTopRightRadius: '12px', borderBottomRightRadius: '12px' }} onClick={onClick2}>
              <p style={{ width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)" }}>จิ้งหรีดหลุด</p>
            </div>
          </div>
        </Row>
        <Row>
          <p className="text-center ml-4 mt-4" style={{fontColor:"#58585e", fontSize:"14px", fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold'}}>พนักงานประจำบ่อ</p>
        </Row>
        <Row className="mb-3">
          <p className="text-center ml-4 mt-2" style={{fontColor:"#58585e", fontSize:"12px", fontFamily: 'IBM Plex Sans Thai'}}>นายวิชัย, นางวรรณี, นางปารีณา</p>
        </Row>

        <Row>
          <div className="row mt-3"
            style={{
              position: "relative",
              bottom: "15px",
              width: '80%',
              zIndex: '100',
              height: '30px',
              boxShadow: "-2px 2px 11px -6px #333",
              borderRadius: '12px',
              left: "15%"
            }}>
            <div className="col" style={{ backgroundColor: "#504A4B", padding: '0px 0px 0px 0px', display: 'relative', borderTopLeftRadius: '12px', borderBottomLeftRadius: '12px', borderTopRightRadius: '12px', borderBottomRightRadius: '12px' }}>
              <a href={'/admin/farm?farm_id=' + id}>
                <p style={{ color: "white", width: '100%', textAlign: 'center', fontFamily: 'IBM Plex Sans Thai', fontWeight: 'bold', fontSize: '16px', position: 'absolute', top: '50%', left: '50%', transform: "translate(-50%, -50%)" }}>ดูรายละเอียด</p>
              </a>

            </div>

          </div>
        </Row>

      </Collapse> */}

    </div>

  )


});

export default HomeDashDetail;
