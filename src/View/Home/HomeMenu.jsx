import React, { useState, useEffect } from "react";
import img_sading from "../../Assets/img/bug-sading.png";
import Progressbar from "react-customizable-progressbar";
import _ from "lodash";
import { observer } from "mobx-react-lite";
import UIstore from "../../Stores/UIstore";
import { useContext } from "react";
import { useHistory } from "react-router-dom";

const HomeMenu = observer((props) => {
  const [Tab, setTab] = React.useState(1);
  const [role, setRole] = useState(null);
  const uistore = useContext(UIstore);
  const history = useHistory();
  // const render_children = () =>{
  //     return
  // }
  useEffect(() => {
    uistore.SetEnableHeader(true);
    uistore.SetEnableFooter(true)

  }, []);

  const pushPage = (page) => {
    // console.log('clickInfo', clickInfo);
    history.push({ pathname: `/admin/${page}` });
    history.go('d');

  }

  return (
    <div className="menu-content">
      <div className="head-menu-text px-3">
        <hr className="w-100 m-0 text_dimgray mt-3" />
        <span className="sarabun_L_300 text_black mt-4">HELLO</span>

        <span className="prompt_XXL text_black">บุญประสิทธิ</span>
      </div>

      <div className="body-menu">
        <div className="absolute-contain mx-3">
          <div onClick={() => {
            pushPage('HomeDash')
          }} className="over-all ">
            <span className="prompt_XXL text_white"> ภาพรวม </span>
            <span className="sarabun_M text_blueteal"> DASHBOARD </span>

            <div className="percent-circle mt-3">
              <Progressbar
                strokeWidth={5}
                trackStrokeWidth={5}
                pointerStrokeWidth={5}
                strokeColor={"#fbd614"}
                trackStrokeColor={"#5dd1f6"}
                pointerStrokeColor={"#fbd614"}
                strokeLinecap={"inherit"}
                pointerRadius={8}
                progress={60}
                radius={65}
              />

              <div
                className="d-flex flex-column align-items-center"
                style={{
                  position: "absolute",
                }}
              >
                <span
                  className="prompt_HL "
                  style={{
                    color: "#fbd614",
                    lineHeight: 1,
                  }}
                >
                  58
                </span>
                <span
                  className="prompt_XL "
                  style={{
                    color: "#fbd614",
                  }}
                >
                  / 70 บ่อ
                </span>
              </div>
            </div>

            <img
              src={img_sading}
              alt=""
              className="icon-Large position-absolute"
              style={{ right: 0, bottom: 0 }}
            />
          </div>
          <div className="d-flex flex-row w-100 align-items-center justify-content-between mt-3">
            <div onClick={() => {
              if (localStorage.getItem("role") === 1) {
                pushPage('Worklotmanage')
              }

            }}

              className={`${localStorage.getItem("role") === 1 ? "work-manager" : "work-manager-disable"}`}>
              <span className="prompt_XXL text_white"> จัดการงาน </span>
              <span className="sarabun_M text_blueteal"> Work Manager </span>
            </div>

            <div onClick={() => {
              pushPage('Calendarworker')
            }} className="calendar-card">
              <span className="prompt_XXL text_white"> ปฎิทิน </span>
              <span className="sarabun_M text_blueteal"> Calendar </span>
            </div>
          </div>

          <div onClick={() => {
             if (localStorage.getItem("role") === 1) {
              pushPage('')
            }
          
          }} 
          className={`${localStorage.getItem("role") === 1 ? "document-card" : "document-card-disable"}`} >
            <span className="prompt_XXL text_white"> เอกสาร </span>
            <span className="sarabun_M text_blueteal"> Document </span>
          </div>

          <div className="mt-3 w-100 d-flex justify-content-center align-items-center">
            <span className="sarabun_M_300 text_dimgray">
              {" "}
              Digitize by : Brizket{" "}
            </span>
          </div>
        </div>
      </div>
    </div>
  );
});

export default HomeMenu;
