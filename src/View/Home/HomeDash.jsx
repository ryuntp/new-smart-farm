import React, { useContext, useEffect, useState } from "react";
import Rootstore from "../../Stores/Rootstore";
import Todostore from "../../Stores/Todostore";
import axios from "axios";
import { observer } from "mobx-react-lite";
import UIstore from "../../Stores/UIstore";
import HatcheryDetail from "../Hatchery/HatcheryDetail";
import axiosInstance from "../../Lib/AxiosInterceptor";

// const stores = useContext(StoreContext);

const handleColorType = (bgcolor) => {
  switch (bgcolor) {
    case "Bret":
      return "#03a9f3";
    case "Bruh":
      return "#f56342";
    default:
      return "#000";
  }
};

const workAPI = {
  id: 1,
  name: "Leanne Graham",
  username: "Bret",
  email: "Sincere@april.biz",
};

const percentage1 = 25;
const percentage2 = 5;
const percentage3 = 35;

export const HomeDash = observer(() => {
  const [isOpen, setIsOpen] = useState(false);
  const [active, setActive] = useState(null);

  const toggle = (id) => {
    setIsOpen(!isOpen);
    setActive(id);
  };

  const uistore = useContext(UIstore);

  const [insectariumAPI, setAPI] = useState(null);
  const [countinsectAPI, setcountinsect] = useState([1, 2, 3, 4, 5]);
  const [continsectarium, setContinsectarium] = useState(0);
  const [gettemp, settemp] = useState("");
  const [gethum, sethum] = useState("");

  const Allinsect = async () => {
    try {
      const Allinsect = await axiosInstance.get(
        `admin/dashboard/allinsectarium/`
      );
      // console.log('Allinsect', Allinsect);
      setAPI(Allinsect.data);
      setContinsectarium(Allinsect.data[0].countpound)
    } catch (err) {
      console.error("errrr", err.message);
    }
  };

  const Getcountinsect = async () => {
    try {
      const Countinsect = await axiosInstance.get(
        `all/place/countinsect`
      );
      // console.log('Allinsect', Allinsect);
      setcountinsect(Countinsect.data);
    } catch (err) {
      console.error("errrr", err.message);
    }
  };


  useEffect(() => {
    Getcountinsect();
    Allinsect();
    uistore.SetEnableHeader(true);
    uistore.SetEnablesubHeader(true);
    uistore.SetEnableFooter(true)
    // countcrickettype1()
    // countcrickettype2()
    // countcrickettype3()
    setTimeout(() => {
      //   Allegg()
      //   Allinsect()
      //   countcrickettype1()
      //   countcrickettype2()
      //   countcrickettype3()
    }, 3000);
  }, []);

  const renderJobCards = (cards) => {
    return cards.map((card, index) => (
      <div key={index} className="px-3">
        {/* <HomeDashDetail
          percentage1={gettemp}
          percentage2={percentage2}
          percentage3={gethum}
          isOpen={isOpen}
          toggle={toggle}
          active={active}
          card={card}
          index={index}
          id={insectariumAPI[index].insectarium_id}
        /> */}
      </div>
    ));
  };

  const renderHatcheryCards = (cards) => {
    if (!cards) {
      return;
    }
    var array_data = cards[0].array;
    return array_data.map((card, index) => (
      <div key={index} className="px-3 mb-2">
        <HatcheryDetail
          percentage1={percentage1}
          percentage2={percentage2}
          card={card}
          index={index}
        // id={hatcheriesAPI[index].egg_id}
        />
      </div>
    ));
  };

  return (
    <div className="content p-0">
      {/* <div>
        <NewAdminNavBar pageName={"Dashboard"} />
        <AdminHeader />
      </div> */}
      <div className="content-dashboard">
        <div className="">
          <p className="text_blueteal prompt_XXL">ภาพรวม</p>
        </div>

        <div className="cricket-list">
          {countinsectAPI.map((item,index) => {
            if(item.name === null){
              return
            }
            return <div className={`${item.name === 'สะดิ้ง' 
            ? "card-cricket01 cricket-bg01" 
            : item.name === 'ทองดำ' 
            ? "card-cricket02 cricket-bg02" 
            : item.name === 'ทองแดง' 
            ? "card-cricket03 cricket-bg03" : ""}`}>
              {/* bug-ina-nbg-01.jpg */}
              <div className="d-flex flex-column pl-3 pt-3">
                <p className="text_white prompt_XXL">{item.name}</p>
                <p className="text_blueteal sarabun_L">
                  {item.name === 'สะดิ้ง' ? "SADING" 
                  : item.name === 'ทองแดง' ?
                   "THONGDANG CRICKET" : 'THONGDUM CRICKET'}
                </p>
              </div>

              <div className="puddle pl-3">
                <p className="text_gold prompt_XXXL">{item.count}</p>
                <p className="text_gold sarabun_L ml-2">บ่อ</p>
              </div>
            </div>
          })}


          {/* <div className="card-cricket02 cricket-bg02">
            <div className="d-flex flex-column pl-3 pt-3">
              <p className="text_white prompt_XXL">จิ้งหรีดทอง</p>
              <p className="text_blueteal sarabun_L">BLACK CRICKET</p>
            </div>

            <div className="puddle pl-3">
              <p className="text_blueroyal prompt_XXXL">12</p>
              <p className="text_blueroyal sarabun_L ml-2">บ่อ</p>
            </div>
          </div> */}
        </div>
      </div>

      <div>
        <div className="mb-5 pb-5">
          <div className="d-flex flex-row align-items-center justify-content-between mt-4 px-3">
            <p className="text_blueteal prompt_XXL">บ่อเลี้ยง</p>
            <p className="sarabun_M text_dimgray">{continsectarium} บ่อเลี้ยง</p>
          </div>

          {renderHatcheryCards(insectariumAPI)}
        </div>

        {/* {insectariumAPI ? renderJobCards(insectariumAPI) : null} */}
      </div>
      {/* <img style={{ display: "block", marginLeft: "auto", marginRight: "auto" }} className="mt-3" height="40%" width="40%" src={ache} /> */}
    </div>
  );
});

// const HomeDash = () => {

// };

export default HomeDash;
