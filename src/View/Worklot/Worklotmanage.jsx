
import React, { useContext, useEffect, useState } from "react";
import Rootstore from "../../Stores/Rootstore";
import axios from 'axios'
import Progressbar from 'react-customizable-progressbar'
import { observer } from 'mobx-react-lite'
import icon_setting from '../../Assets/img/icon-general-setting.png'
import icon_generalhistory from '../../Assets/img/icon-general-history.png'
import icon_alert from '../../Assets/img/icon-general-alert.png'
import icon_Love from '../../Assets/img/icon-status-02-beed.png'
import UIstore from "../../Stores/UIstore";
import Worklotmanagestore from "../../Stores/Worklotmanagestore";
import { useHistory } from "react-router-dom";
// const stores = useContext(StoreContext);



export const Worklotmanage = observer(() => {
  const worklotmanagestore = useContext(Worklotmanagestore);
  const uistore = useContext(UIstore);
  const [isOpen, setIsOpen] = useState(false);
  const history = useHistory();
  const [active, setActive] = useState(null);

  const toggle = (id) => {
    setIsOpen(!isOpen);
    setActive(id)

  }


  useEffect(() => {
    uistore.SetEnableHeader(true)
    uistore.SetHeadercolor('blue')
    uistore.SetEnableFooter(true)
    uistore.SetEnablesubHeader(false)
    worklotmanagestore.getCountpound()
  }, []);



  return (

    <div className="content p-0" >
      {/* <div>
        <NewAdminNavBar pageName={"Dashboard"} />
        <AdminHeader />
      </div> */}
      <div className='head-workmanage' >
        <div className='d-flex flex-row align-items-center justify-content-between w-100 px-3 pb-5 mb-4'>
          <div className='d-flex flex-column '>
            <span className="prompt_XXL text_white">
              จัดการงาน
            </span>
            <span className="sarabun_M text_blueteal">
              WORK MANAGE
            </span>
          </div>

          <div className='d-flex flex-row pt-1'>
            <span className="prompt_M_200 mt-1 text-decoration-underline text_white">
              SETTING
            </span>

            <img src={icon_setting} className="ml-2" alt="" width={30} height={30} />
          </div>
        </div>


        <div className='progress-circle'>
          <div className='position-relative d-flex justify-items-center align-items-center'
          
          style={{
              padding: 2,
              boxShadow: '10px 10px 20px 0 rgba(0, 0, 0, 0.1)',
              backgroundColor: '#fff',
              borderRadius: 100
            }}
          >
            <Progressbar
              strokeWidth={8}
              trackStrokeWidth={8}
              pointerStrokeWidth={4}
              strokeColor={'#2a8be0'}
              trackStrokeColor={'#e1e1e1'}
              pointerStrokeColor={'#2a8be0'}
              strokeLinecap={'inherit'}
              // children={render_children()}
              pointerRadius={10}
              progress={60}
              radius={70}
            />
          </div>

          <div className='d-flex flex-column align-items-center' style={{
            position: 'absolute',
          }}>
            <span className='prompt_HL ' style={{
              color: "#2a8be0",
              lineHeight: 1
            }}>
              {worklotmanagestore.countpound.nonempty_pound}
            </span>
            <span className='prompt_XL ' style={{
              color: "#2a8be0"
            }}>
              / {worklotmanagestore.countpound.allpound} บ่อ
            </span>
          </div>
        </div>


      </div>

      <div
        onClick={() => {
          history.push({ pathname: `/admin/worklothistory` });
          // history.go('d')
          uistore.SetEnableHeader(false)
        }}
        className='d-flex justify-content-center align-items-center w-100'>
        <div className="mt-8 card-lemonchiffon d-flex flex-row align-items-center justify-content-center">
          <img src={icon_generalhistory} className='mr-1' alt="" width={25} height={25} />
          <span className='prompt_L_300 text_orange'>
            ประวัติการเพาะ
          </span>
        </div>
      </div>

      <div className='d-flex flex-row align-items-center justify-content-between mt-4 px-3'>
        <span className='prompt_XXL text_blueteal' >
          กำลังดำเนินการ
        </span>
        <span className="sarabun_M_300 text_dimgray">6 ล๊อตผลิต</span>
      </div>

      <div className='cricket-listmanage' >

        <div className='cricket-card1 d-flex flex-column justify-content-center '>
          <span className='prompt_XL text_white'>
            จิ้งหรีดสะดิ้ง
          </span>
          <span className='sarabun_L text_blueteal'>
            30 %
          </span>
        </div>

        <div className='cricket-card2 d-flex flex-column d-flex flex-column justify-content-center'>
          <span className='prompt_XL text_white'>
            จิ้งหรีดทองดำ
          </span>
          <span className='sarabun_L text_blueteal'>
            30 %
          </span>
        </div>



      </div>


      <div className='head-table'>
        <div className='w-100 text-center '>
          <span className='sarabun_M text_dimgray '>เลขล๊อตผลิต</span>

        </div>
        <div className='w-100 text-center'>
          <span className='sarabun_M text_dimgray'>วางไข่</span>
        </div>
        <div className='w-100 text-center'>
          <span className='sarabun_M text_dimgray'>เก็บเกี่ยว</span>
        </div>

      </div>

      <div className='body-worklot-tabel mb-5 pb-5'>

        <div onClick={() => {
          history.push({ pathname: `/admin/Worklotdetail` });
          // history.go('d')
          uistore.SetEnableHeader(false)
        }} className='card-lotmanage'>
          <div className='d-flex flex-row align-items-center justify-content-between'>
            <div className='d-flex flex-row'>
              <div className='circle-process mr-2'>

              </div>

              <div className='d-flex flex-column'>
                <span className='prompt_XL text_blueteal'>
                  ลอตเลี้ยง 3
                </span>

                <span className='sarabun_M_300 text_dimgray'>
                  รอฝักตัว
                </span>
              </div>
            </div>


            <div className='status-orange mt-2 mr-2 d-flex align-items-center justify-content-center'>

              <span className='sarabun_M_300 text_white'>ผิดปกติ</span>

              <img className="ml-1" src={icon_alert} alt="" width={25} height={25} />

            </div>
          </div>
          <div className='position-relative mx-3 my-2 mt-3'>

            <div className=' line-dash not-absolute ' />

          </div>
          <div className='d-flex flex-row align-items-center justify-content-between'>
            <div className='w-100 text-center position-relative'>
              <span className='sarabun_M_300 text_dimgray '>ORD20200001</span>
              <div style={{
                width: 1,
                height: 15, backgroundColor: "#cdd9e9",
                position: 'absolute',
                right: 0,
                top: 5
              }}>

              </div>
            </div>
            <div className='w-100 text-center position-relative'>
              <span className='sarabun_M_300 text_dimgray'>12/03/21</span>
              <div style={{
                width: 1,
                height: 15, backgroundColor: "#cdd9e9",
                position: 'absolute',
                right: 0,
                top: 5
              }}>

              </div>
            </div>
            <div className='w-100 text-center position-relative'>
              <span className='sarabun_M_300 text_dimgray'>12/03/21</span>

            </div>
          </div>
        </div>


        <div className='card-lotmanage'>
          <div className='d-flex flex-row align-items-center justify-content-between'>
            <div className='d-flex flex-row'>
              <div className='circle-process mr-2'>

              </div>

              <div className='d-flex flex-column'>
                <span className='prompt_XL text_blueteal'>
                  ลอตเลี้ยง 2
                </span>

                <span className='sarabun_M_300 text_dimgray'>
                  รอฝักตัว
                </span>
              </div>
            </div>


            <div className='status-darkturquoise-tag mt-2 mr-2 px-3'>


              <span className='sarabun_M_300 text_white'>พร้อมผสม</span>

              <img className="ml-1" src={icon_Love} alt="" width={25} height={25} />

            </div>
          </div>
          <div className='position-relative mx-3 my-2 mt-3'>

            <div className=' line-dash not-absolute' />

          </div>
          <div className='d-flex flex-row align-items-center justify-content-between'>
            <div className='w-100 text-center position-relative'>
              <span className='sarabun_M_300 text_dimgray '>ORD20200001</span>
              <div style={{
                width: 1,
                height: 15, backgroundColor: "#cdd9e9",
                position: 'absolute',
                right: 0,
                top: 5
              }}>

              </div>
            </div>

            <div className='w-100 text-center position-relative'>
              <span className='sarabun_M_300 text_dimgray'>12/03/21</span>
              <div style={{
                width: 1,
                height: 15, backgroundColor: "#cdd9e9",
                position: 'absolute',
                right: 0,
                top: 5
              }}>

              </div>
            </div>
            <div className='w-100 text-center'>
              <span className='sarabun_M_300 text_dimgray'>12/03/21</span>
            </div>
          </div>
        </div>




        <div className='card-lotmanage'>
          <div className='d-flex flex-row align-items-center justify-content-between'>
            <div className='d-flex flex-row'>
              <div className='circle-process mr-2'>

              </div>

              <div className='d-flex flex-column'>
                <span className='prompt_XL text_blueteal'>
                  ลอตเลี้ยง 1
                </span>

                <span className='sarabun_M_300 text_dimgray'>
                  รอฝักตัว
                </span>
              </div>
            </div>


            <div className='status-orange mt-2 mr-2 d-flex align-items-center justify-content-center p-1'>



              <img className="mb-1" src={icon_alert} alt="" width={25} height={25} />

            </div>
          </div>
          <div className='position-relative mx-3 my-2 mt-3'>

            <div className=' line-dash not-absolute' />

          </div>
          <div className='d-flex flex-row align-items-center justify-content-between'>
            <div className='w-100 text-center position-relative'>
              <span className='sarabun_M_300 text_dimgray '>ORD20200001</span>
              <div style={{
                width: 1,
                height: 15, backgroundColor: "#cdd9e9",
                position: 'absolute',
                right: 0,
                top: 5
              }}>

              </div>
            </div>
            <div className='w-100 text-center position-relative'>
              <span className='sarabun_M_300 text_dimgray'>12/03/21</span>
              <div style={{
                width: 1,
                height: 15, backgroundColor: "#cdd9e9",
                position: 'absolute',
                right: 0,
                top: 5
              }}>

              </div>
            </div>
            <div className='w-100 text-center'>
              <span className='sarabun_M_300 text_dimgray'>12/03/21</span>
            </div>
          </div>
        </div>

      </div>


     


    </div>
  );
})


// const HomeDash = () => {

// };

export default Worklotmanage;
