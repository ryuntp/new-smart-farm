
import React, { useContext, useEffect, useState } from "react";
import Rootstore from "../../Stores/Rootstore";
import Todostore from "../../Stores/Todostore";
import axios from 'axios'
import { observer } from 'mobx-react-lite'
import useCollapse from 'react-collapsed'
import DatePicker from "react-datepicker";
import { Dropdown } from "react-bootstrap";
import icon_leftarrow from '../../Assets/img/dropdown-left.png'
import icon_search from '../../Assets/img/ico-search.png'
import icon_filter from '../../Assets/img/icon-general-filter.png'
import icon_downarrow from '../../Assets/img/dropdown-down.png'
import bug_sading from '../../Assets/img/bug-sading.png'
import icon_calendar from '../../Assets/img/icon-nav-calendar-blue.png'
import view_icon from '../../Assets/img/icon-general-view.png'
import Collapse from '../../Components/Collapsed/Collapsed'
import Worklotmanage from "../../Stores/Worklotmanagestore";
import Appmodal from "../../Components/Modals/Appmodal";
import { useHistory } from "react-router-dom";
import _ from "lodash";
import moment from "moment";

// const stores = useContext(StoreContext);



export const Worklothistory = observer(() => {

    const worklotmanagestore = useContext(Worklotmanage);
    const [valueStartdate, setvalueStartdate] = useState("");
    const [valueTime_date, setvalueTime_date] = useState("");
    const [valueEnd_date, setvalueEnd_date] = useState("");
    const [select_species, setselect_species] = useState(false);
    const [height, setHeight] = useState(100);
    const history = useHistory();
    // const [isExpanded, setExpanded] = React.useState(true);
    const [isExpanded_filterdate, setExpanded_filterdate] = useState(false)
    const [isExpanded_filterharvestdate, setExpanded_filterharvestdate] = useState(false)

    const [isExpanded_filterspecies, setExpanded_filterspecies] = useState(false)





    useEffect(() => {
        worklotmanagestore.gethistory()

    }, [worklotmanagestore]);



    const CustomInput = React.forwardRef(({ value, onClick }, ref) => (
        <div className="drop-downwork-blue d-flex flex-row align-items-center justify-content-between mt-3" onClick={onClick} ref={ref}>
            <span>
                {value}
            </span>

            <img src={icon_calendar} alt="" className="icon-mini" />
        </div>
    ));


    const render_detail = (item, index) => {
        return <div className='d-flex flex-column mt-2 ' >

            <div className='position-relative mx-3 my-2 mt-0 px-2 '>

                <div className='line-dash not-absolute' />

            </div>


            <span className='prompt_XXL text_blueteal text-center mt-2'>จิ้งหรีด{item.cricket_name}</span>
            <span className='sarabun_L_300 text_dimgray text-center'>SADING</span>


            <div className='d-flex flex-column position-relative justify-content-center align-items-center mt-3'>
                <div className='d-flex flex-row justify-content-around w-100' style={{ zIndex: 88 }}>
                    <div className='d-flex flex-column align-items-center'>
                        <div className='cricket-circle' >

                        </div>
                        <span className='sarabun_L text_dimgray mt-2'>
                            เริ่มต้น
                        </span>
                        <span className='sarabun_L_300 text_dimgray'>
                            12/03/21
                        </span>
                    </div>
                    <div className='d-flex flex-column align-items-center'>
                        <div className='cricket-circle' >

                        </div>
                        <span className='sarabun_L text_dimgray mt-2'>
                            วางไข่
                        </span>
                        <span className='sarabun_L_300 text_dimgray'>
                            12/03/21
                        </span>
                    </div>
                    <div className='d-flex flex-column align-items-center'>
                        <div className='cricket-circle' >

                        </div>
                        <span className='sarabun_L text_dimgray mt-2'>
                            เก็บเกี่ยว
                        </span>
                        <span className='sarabun_L_300 text_dimgray'>
                            12/03/21
                        </span>
                    </div>


                </div>
                <div className="line-solid behind" />

            </div>

            <div className='d-flex justify-content-center align-items-center mb-3'>
                <div className='btn-more-detail'>
                    <img src={view_icon} className='icon-mini mr-2 ' alt="" />
                    <span className='prompt_L text_dodgerblue'>
                        รายละเอียด
                    </span>
                </div>
            </div>
        </div>
    }

    const render_date = () => {
        return <div className='d-flex flex-row w-100 mb-2'>
            <div className='w-50 mx-3'>
                <DatePicker
                    selected={new Date(worklotmanagestore.option.initdate[0])}
                    onChange={(date) => {
                        var condi_option = worklotmanagestore.option
                        condi_option.initdate[0] = moment(date).format("YYYY-MM-DD")
                        worklotmanagestore.setoption(condi_option)
                    }}
                    minDate={worklotmanagestore.option.initdate[1]}
                    customInput={<CustomInput />}
                />
            </div>

            <div className='w-50 mx-3'>
                <DatePicker
                    selected={new Date(worklotmanagestore.option.initdate[1])}
                    onChange={(date) => {
                        var condi_option = worklotmanagestore.option
                        condi_option.initdate[1] = moment(date).format("YYYY-MM-DD")
                        worklotmanagestore.setoption(condi_option)
                    }}
                    customInput={<CustomInput />}
                />

            </div>
        </div>
    }

    const render_species = () => {
        return <div>
            {worklotmanagestore.crickettypelist.map((item, index) => {
                return <div className='d-flex flex-row justify-content-between mx-3 mb-3'>
                    <div className='d-flex flex-row align-items-center'>
                        <input
                            name="isGoing"
                            type="checkbox"
                            className='mr-3'
                            style={{
                                width: 30,
                                height: 30,
                            }}
                            checked={item.selected}
                            onChange={(event) => {

                                if (item.selected === false) {
                                    worklotmanagestore.addcrickettypelist(item.id)
                                    item.selected = event.target.checked
                                } else {
                                    worklotmanagestore.removecrickettypelist(index)
                                    item.selected = event.target.checked
                                }
                                console.log(worklotmanagestore.option);

                            }}
                        />

                        <span className="prompt_XL text_black">
                            {item.name}
                        </span>
                    </div>

                    <div>
                        <img src={bug_sading} alt="" className='crickket_img' />
                    </div>
                </div>
            })}

            {/* <div className='d-flex flex-row justify-content-between mx-3 mb-3'>
                <div className='d-flex flex-row align-items-center'>
                    <input
                        name="isGoing"
                        type="checkbox"
                        className='mr-3'
                        style={{
                            width: 30,
                            height: 30,
                        }}
                        checked={select_species}
                        onChange={(event) => {
                            setselect_species(event.target.checked)
                        }}
                    />

                    <span className="prompt_XL text_black">
                        จิ้งหรีดทองดำ
                    </span>
                </div>

                <div>
                    <img src={bug_sading} alt="" className='crickket_img' />
                </div>
            </div>
            <div className='d-flex flex-row justify-content-between mx-3 mb-3'>
                <div className='d-flex flex-row align-items-center'>
                    <input
                        name="isGoing"
                        type="checkbox"
                        className='mr-3'
                        style={{
                            width: 30,
                            height: 30,
                        }}
                        checked={select_species}
                        onChange={(event) => {
                            setselect_species(event.target.checked)
                        }}
                    />

                    <span className="prompt_XL text_black">
                        จิ้งหรีดทองแดง
                    </span>
                </div>

                <div>
                    <img src={bug_sading} alt="" className='crickket_img' />
                </div>
            </div> */}
        </div>



    }

    const render_harvestdate = () => {
        return <div className='d-flex flex-column w-100 mb-2'>
            <div className='list-harvest-filter'>
                <div onClick={() => {
                    // createworkstore.Setmodal_splitlot(!createworkstore.modal_splitlot)
                }}
                    className='btn-add-worklot mr-4'>

                </div>

                <div
                    onClick={() => {
                        var condi_option = worklotmanagestore.option
                        condi_option.havestdate = 'custom'
                        worklotmanagestore.setoption(condi_option)
                    }}
                    className={`mr-4 ${worklotmanagestore.option.havestdate === 'custom' ? "status-blueteal" : "status-paleturquoise"}`}>
                    <span className={`prompt_L ${worklotmanagestore.option.havestdate === 'custom' ? "text_white" : "text_dodgerblue"}`}>กำหนดเอง</span>
                </div>

                <div onClick={() => {

                    var condi_option = worklotmanagestore.option
                    condi_option.havestdate = '3lm'
                    worklotmanagestore.setoption(condi_option)
                }} className={`mr-4 ${worklotmanagestore.option.havestdate === '3lm' ? "status-blueteal" : "status-paleturquoise"}`}>
                    <span className={`prompt_L ${worklotmanagestore.option.havestdate === '3lm' ? "text_white" : "text_dodgerblue"}`}>3เดือนล่าสุด</span>
                </div>
                <div onClick={() => {
                    var condi_option = worklotmanagestore.option
                    condi_option.havestdate = 'ly'
                    worklotmanagestore.setoption(condi_option)
                }} className={`mr-4 ${worklotmanagestore.option.havestdate === 'ly' ? "status-blueteal" : "status-paleturquoise"}`}>
                    <span className={`prompt_L ${worklotmanagestore.option.havestdate === 'ly' ? "text_white" : "text_dodgerblue"}`}>ปีนี้</span>
                </div>

            </div>


            {worklotmanagestore.option.havestdate === 'custom' ?
                <div className='d-flex flex-row'>
                    <div className='w-50 mx-3'>
                        {/* <DatePicker
                            selected={new Date(worklotmanagestore.option.startdate)}
                            onChange={(date) => {
                                console.log('date', date.toISOString);
                                var condi_option = worklotmanagestore.option
                                condi_option.startdate = date
                                worklotmanagestore.Setoption(condi_option)
                            }}
                            customInput={<CustomInput />}
                        /> */}
                    </div>

                    <div className='w-50 mx-3'>
                        {/* <DatePicker
                            selected={new Date(worklotmanagestore.option.enddate)}
                            onChange={(date) => {
                                var condi_option = worklotmanagestore.option
                                condi_option.enddate = date
                                worklotmanagestore.Setoption(condi_option)
                            }}
                            minDate={worklotmanagestore.option.startdate}
                            customInput={<CustomInput />}
                        /> */}

                    </div>
                </div> : null
            }
        </div>
    }



    return (

        <div className="bg-white p-0" >

            <div className='d-flex flex-row w-100 align-items-center justify-content-between px-3 py-3'>
                <div
                    onClick={() => {
                        history.goBack()
                    }}
                    className="d-flex flex-row justify-content-center align-items-center"
                >
                    <img src={icon_leftarrow} alt='' className='icon-mini' />
                    <span className='sarabun_L_100 text_black'>
                        BACK
                    </span>
                </div>

                <div
                    onClick={() => {

                    }}
                    className="d-flex flex-row justify-content-center align-items-center"
                >
                    <img src={icon_search} className='icon-minismall' />
                </div>
                {/*  */}

            </div>

            <div className='w-100 d-flex flex-row px-3  filter-lotmanage'>
                <div onClick={() => {
                    worklotmanagestore.setfilterhistory(true)
                }}>
                    <img src={icon_filter} className='icon-mini' />
                </div>
                <div className='line-dash-vertical ml-3' />
                <div className='w-100 d-flex flex-row align-items-center ml-2'>
                    <span className='prompt_L_300 text_dimgray' >เดือนล่าสุด</span>
                </div>
                <img src={icon_downarrow} className='icon-mini' />
            </div>


            <div className='cricket-listmanage mt-4' >

                <div className='cricket-card1 d-flex flex-column justify-content-center '>
                    <span className='prompt_XL text_white'>
                        จิ้งหรีดสะดิ้ง
                    </span>
                    <span className='sarabun_L text_blueteal'>
                        30 %
                    </span>
                </div>

                <div className='cricket-card2 d-flex flex-column d-flex flex-column justify-content-center'>
                    <span className='prompt_XL text_white'>
                        จิ้งหรีดทองดำ
                    </span>
                    <span className='sarabun_L text_blueteal'>
                        30 %
                    </span>
                </div>


            </div>


            <div className='d-flex flex-row align-items-center justify-content-between mt-4 px-3 pb-3'>
                <span className='prompt_XXL text_blueteal'>
                    ประวัติ
                </span>
                <span className='sarabun_M_300 text_dimgray'>
                    {worklotmanagestore.history_arr.lotcount} ลอตเลี้ยง
                </span>
            </div>

            <div className='worklot-history'>

                {_.get(worklotmanagestore.history_arr, 'lotlist', []).map((item, index) => {
                    return <div className='body-worklot-tabel'>

                        <div className='card-lotmanage'>

                            <div
                                onClick={() => {
                                    let newArr = _.get(worklotmanagestore.history_arr, 'lotlist', []).map((item, i) => {
                                        if (index === i) {
                                            item.active = !item.active

                                            return item;
                                        } else {
                                            return item;
                                        }
                                    });

                                }}

                                className='d-flex flex-row align-items-center justify-content-between'>
                                <div className='d-flex flex-row px-3'>

                                    <div className='d-flex flex-column'>
                                        <span className='prompt_XL text_blueteal'>
                                            {item.id}
                                        </span>

                                        <span className='sarabun_M_300 text_dimgray'>
                                            {item.name}
                                        </span>
                                    </div>
                                </div>

                                <div className='box-weight-cricket mr-3'>

                                    <span className="prompt_XL text_dodgerblue mr-1">
                                        {item.weight}
                                    </span>

                                    <span className='sarabun_M_300 text_dimgray'>
                                        กก.
                                    </span>


                                    <img src={bug_sading} alt="" className='crickket_img position-absolute end-0' />

                                </div>



                            </div>

                            {/* */}

                        </div>

                        <div className='d-flex flex-column bg-white m-0' >

                            <Collapse
                                isActive={item.active}
                                render_children={render_detail(item, index)}
                            />

                        </div>

                    </div>
                })}


            </div>
            <Appmodal
                visible={worklotmanagestore.filterhistory}
                nocontainer={false}
                className={'d-flex justify-content-center align-items-center'}
            >
                <div
                    className='d-flex flex-column  py-4 position-relative'
                    style={{
                        width: '100%',
                        minHeight: '100vh',
                        backgroundColor: '#fff',
                    }}>
                    <div
                        onClick={() => {
                            worklotmanagestore.setfilterhistory(false)
                        }}
                        className="d-flex flex-row justify-content-start align-items-center px-3"
                        style={{}}>
                        <img src={icon_leftarrow} alt='' className='icon-mini' />
                        <span className='sarabun_L_100 text_dimgray'>
                            BACK
                        </span>
                    </div>


                    <div

                    >
                        <div className='px-4 mt-3' onClick={() => setExpanded_filterdate((x) => !x)}>
                            <span className='prompt_XXL text_blueteal'>
                                ตัวกรอง
                            </span>


                            <div className='d-flex flex-row justify-content-between align-items-center py-3'>

                                <span className='sarabun_L text_dimgray'>
                                    วันที่เริ่มต้น
                                </span>

                                <div className='d-flex flex-row align-items-center'>
                                    <span className='prompt_L text_black mr-2'>ทั้งหมด</span>

                                    <img src={icon_downarrow} className='icon-mini' />
                                </div>

                            </div>

                        </div>



                        <Collapse
                            isActive={isExpanded_filterdate}
                            render_children={render_date()}
                        />

                        <div className="position-relative mt-1 ">
                            <div className=" line-solid not-absolute" />
                        </div>


                    </div>


                    <div>
                        <div className='px-4'
                            onClick={() => setExpanded_filterharvestdate((x) => !x)}
                        >
                            <div className='d-flex flex-row justify-content-between align-items-center py-4'>

                                <span className='sarabun_L text_dimgray'>
                                    วันที่เก็บเกี่ยว
                                </span>

                                <div className='d-flex flex-row align-items-center'>
                                    <span className='prompt_L text_black mr-2'>ทั้งหมด</span>

                                    <img src={icon_downarrow} className='icon-mini' />
                                </div>

                            </div>

                        </div>

                        <Collapse
                            isActive={isExpanded_filterharvestdate}
                            render_children={render_harvestdate()}
                        />
                        <div className="position-relative mt-1 ">
                            <div className=" line-solid not-absolute" />
                        </div>
                    </div>


                    <div className='mb-5'>
                        <div className='px-4'
                            onClick={() => setExpanded_filterspecies((x) => !x)}
                        >
                            <div className='d-flex flex-row justify-content-between align-items-center py-4'>

                                <span className='sarabun_L text_dimgray'>
                                    สายพันธ์จิ้งหรีด
                                </span>

                                <div className='d-flex flex-row align-items-center'>
                                    <span className='prompt_L text_black mr-2'>ทั้งหมด</span>

                                    <img src={icon_downarrow} className='icon-mini' />
                                </div>

                            </div>

                        </div>


                        <Collapse
                            isActive={isExpanded_filterspecies}
                            render_children={render_species()}
                        />
                        <div className="position-relative mt-1 ">
                            <div className=" line-solid not-absolute" />
                        </div>
                    </div>


                    <div className='bottom-filter'>

                        <div onClick={() => {
                            worklotmanagestore.gethistory()
                            worklotmanagestore.setfilterhistory(false)
                        }} className='btn-confirm-100 d-flex justify-content-center align-items-center'>
                            <span className="prompt_XL text_white">
                                กรองเลย
                            </span>
                        </div>
                    </div>


                </div>
            </Appmodal>


        </div>
    );
})


// const HomeDash = () => {

// };

export default Worklothistory;
