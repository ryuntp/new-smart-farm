
import React, { useContext, useEffect, useState } from "react";
import axios from 'axios'
import { observer } from 'mobx-react-lite'
import useCollapse from 'react-collapsed'
import { Dropdown } from "react-bootstrap";
import Switch from "react-switch";
import sading from "../../Assets/img/bug-sading-01.png";
import icon_water from '../../Assets/img/icon-dashboard-water.png'
import icon_food from '../../Assets/img/icon-dashboard-food.png'
import icon_leftarrowwhite from '../../Assets/img/dropdown-left-white.png'
import icon_dots from '../../Assets/img/dots.png'
import checkIcon from "../../Assets/img/fb-check-a@2x.png";
import icon_downarrow from '../../Assets/img/dropdown-down.png'
import icon_Love from '../../Assets/img/icon-status-02-beed-orange.png'
import Collapse from '../../Components/Collapsed/Collapsed'
import Worklotmanage from "../../Stores/Worklotmanagestore";
import HatcheryDetail from "../Hatchery/HatcheryDetail";
import Appmodal from "../../Components/Modals/Appmodal";
import { useHistory } from "react-router-dom";
import axiosInstance from "../../Lib/AxiosInterceptor";
import moment from "moment";
import UIstore from "../../Stores/UIstore";
import CalendarsworkerStore from "../../Stores/CalendarsworkerStore";

// const stores = useContext(StoreContext);



export const Worklotdetail = observer(() => {

    const worklotmanagestore = useContext(Worklotmanage);
    const calendarsworkerStore = useContext(CalendarsworkerStore);
    const uistore = useContext(UIstore);
    const [insectariumAPI, setAPI] = useState(null);
    const [tabSelect, settabSelect] = useState('homedash');
    const [notiList, setNotiList] = useState([{ item: '' }, { item: '' }]);
    const history = useHistory();

    useEffect(() => {
        Allinsect()
        getnotification()
        calendarsworkerStore.Getlist_workdata()
    }, []);


    const Allinsect = () => {
        axiosInstance.get(`admin/dashboard/allinsectarium/`)
            .then((res) => {
                setAPI(res.data);
            })
            .catch((err) => {
                console.log(err);
            })
    };

    const getnotification = () => {
        axiosInstance.get(`all/problem/allproblem/1`)
            .then((res) => {
                setNotiList(res.data);
            })
            .catch((err) => {
                console.log(err);
            })
    };

    const renderHatcheryCards = (cards) => {
        if (!cards) {
            return;
        }
        var array_data = cards[0].array;
        return array_data.map((card, index) => (
            <div key={index} className="px-3">
                <HatcheryDetail
                    percentage1={0}
                    percentage2={0}
                    card={card}
                    index={index}
                // id={hatcheriesAPI[index].egg_id}
                />
            </div>
        ));
    };

    const calPoundList = (arr) => {
        let ans = [];
        let firstval;

        for (let [i, num] of arr.entries()) {
            if (num + 1 === arr[i + 1]) {
                if (!firstval) firstval = num;
            } else {
                if (firstval) {
                    if (firstval !== num) {
                        ans.push(`${firstval}-${num}`);
                    }
                } else {
                    ans.push(num.toString());
                }

                firstval = undefined;
            }
        }

        return ans;
    };
    const handlePoundList = (arr) => {
        if (!arr) return;
        return arr.map((pound) => {
            return (
                <span className="sarabun_M_300 text_dimgray">
                    {pound.insectarium_name}: บ่อที่ {calPoundList(pound.pound_name)}
                </span>
            );
        });
    };
    const sumPounds = (arr) => {
        if (!arr) return;
        let sum = 0;

        for (const pound of arr) {
            sum += pound.pound_name.length;
        }

        return sum;
    };

    const handleDisplayTime = (time) => {
        if (!time) return;
        return time.split("T")[1].slice(0, 5);
    };

    const renderoverall = () => {
        if (tabSelect !== 'homedash') {
            return
        }
        return <div>
            <div className="card-report-cricket mx-3">
                <img src={sading} alt="" className="icon-ExLarge absolute" />
                <div className="d-flex flex-column align-items-center " style={{
                    marginTop: '160px'
                }}>
                    <span className="prompt_XXL text_blueteal">จิ้งหรีดสะดิ้ง</span>
                    <span className="sarabun_M_300 text_dimgray">SADING</span>
                </div>
                <div className='d-flex flex-row justify-content-between align-items-center w-100 mt-5'>
                    <div className='water-level'>
                        <span className='sarabun_L text_dimgray'>น้ำสะสม</span>
                        <div className='d-flex justify-content-center align-items-center'>
                            <span className='prompt_SL text_dodgerblue'>
                                12
                            </span>
                            <img src={icon_water} className='icon-default ml-1' />
                        </div>

                        <span className='sarabun_L_300 text_dimgray'>
                            กก.
                        </span>
                    </div>
                    <div className='food-level '>
                        <span className='sarabun_L text_dimgray'>อาหารสะสม</span>


                        <div className='d-flex justify-content-center align-items-center'>
                            <span className='prompt_SL text_darkturquoise'>
                                12
                            </span>
                            <img src={icon_food} className='icon-default ml-2' />
                        </div>

                        <span className='sarabun_L_300 text_dimgray'>
                            กก.
                        </span>

                    </div>
                </div>

                <div className="FCR-status">
                    <div className="d-flex flex-row align-items-center">
                        <div className="mr-2" style={{ width: 30, height: 30, backgroundColor: '#ff0', borderRadius: 100 }}></div>
                        <span className="sarabun_L text_dimgray">FCR</span>
                    </div>

                    <span className="prompt_XXL text_darkturquoise">
                        2 : 1
                    </span>


                </div>

                <div className='d-flex flex-row align-items-center justify-content-between w-100 mt-3'>
                    <div className="d-flex flex-row align-items-center">
                        <img src={sading} alt="" className="crickket_img mr-3" />

                        <div className="d-flex flex-column">
                            <span className="prompt_L">เก็บเกี่ยว</span>
                            <span className="sarabun_M_300 text_dimgray mt-1">10/05/2021</span>
                        </div>
                    </div>

                    <div className="d-flex flex-column align-items-center">
                        <span className="prompt_SL text_blueteal">45</span>
                        <span className="sarabun_L_300" style={{
                            marginTop: '-12px'
                        }}>กก.</span>
                    </div>
                </div>
            </div>

            {renderHatcheryCards(insectariumAPI)}
        </div>
    }
    const rendernoti = () => {
        if (tabSelect !== 'noti') {
            return
        }
        return <div className="mt-3">

            <div className='w-100 d-flex flex-row px-3 align-items-center' >
                <div className='w-100 d-flex flex-row align-items-center ml-2'>
                    <span className='prompt_L_300 text_dimgray ml-2' >ซ่อนงานที่ทำแล้ว </span>
                </div>

                <Switch uncheckedIcon={false} checkedIcon={false}
                    //  onChange={handleChangeAll} 
                    onColor={'#1e76fe'} offColor="#e2e2e2"
                //  checked={hidework} 
                />

            </div>
            <div className="px-4 border-bottom">
                {notiList.map((data, index) => {
                    return (
                        <div
                            key={index}
                            className={
                                data.status === "0"
                                    ? "d-flex flex-column w-100 pb-3 card-notihis-red my-4"
                                    : "d-flex flex-column w-100 pb-3 card-notihis-green my-4"
                            }
                        // onClick={() => handleOnclickNoti(data.id, "urgent")}
                        >
                            <div className="my-3 d-flex flex-column px-4">
                                <div className="w-100 d-flex flex-row justify-content-between align-items-center">
                                    <span
                                        className={
                                            data.status === "0"
                                                ? "prompt_L text_orangefatbg"
                                                : "prompt_L text_blueteal"
                                        }
                                    >
                                        {" "}
                                        {data.name}{" "}
                                    </span>
                                    <div className="d-flex flex-row justify-content-center align-items-center">
                                        <span
                                            className={
                                                data.status === "0"
                                                    ? "prompt_L text_orangefatbg"
                                                    : "prompt_L text_blueteal"
                                            }
                                        >
                                            X {sumPounds(data.poundlist)} บ่อ
                                        </span>
                                    </div>
                                </div>
                                {handlePoundList(data.poundlist)}
                            </div>
                            <div className="d-flex flex-row justify-content-between align-items-center w-100 px-4">
                                <div className="d-flex flex-row align-items-center">
                                    <div
                                        className="mr-3"
                                        style={{
                                            width: 30,
                                            height: 30,
                                            borderRadius: 100,
                                            backgroundColor: "red",
                                        }}
                                    ></div>
                                    <span className="sarabun_M_300 ">
                                        {data.worker_name}
                                    </span>
                                </div>
                                <span className="sarabun_M_300 text_dimgray">
                                    {handleDisplayTime(data.time)}
                                </span>
                            </div>
                        </div>
                    );
                })}
            </div>

        </div>
    }

    const renderWorker = () => {
        if(tabSelect === 'worker'){
            return
        }
        return <div>
            {calendarsworkerStore.list_workdata.map((item, index) => {
                return <div key={index} className="d-flex flex-column">
                    <div className="d-flex flex-row justify-content-between align-items-center w-100 px-3 mt-4">
                        <div className="d-flex flex-row align-items-center" >
                            <span className="prompt_XXL text_blueteal">{moment(item.time).format("DD")}</span>
                            <span className="sarabun_L_300 text_blueteal ml-2">{moment(item.time).format("ddd")}</span>
                        </div>
                        <span className="sarabun_L_300 text_gray">{item.worklist.length} รายการ</span>
                    </div>

                    {item.worklist.map((item, index) => {
                        return <div
                            onClick={() => {
                                calendarsworkerStore.Getoneworkdata(item.id)
                                    .then(() => {
                                        uistore.Setupdateworkcalendarmodal(true)
                                    })
                                    .catch((err) => {
                                        console.log(err);
                                    })
                            }}
                            key={index}
                            className="card-workdate-detail">
                            <div className="d-flex flex-row align-items-center justify-content-between w-100">
                                <div className="d-flex flex-column">
                                    <span className="prompt_XL text_blueteal">{item.title}</span>
                                    <div className="d-flex flex-row align-items-center">
                                        <span className="sarabun_M text_dimgray">ลอต {item.lot_name} :</span>
                                        {item.poundlist.map((itemin, index) => {
                                            return <span className="sarabun_M_300 text_dimgray">
                                                &nbsp; {calPoundList(itemin.pound_name)}
                                            </span>
                                        })}

                                    </div>
                                </div>
                                {item.workstatus === 0 ? <div className="d-flex flex-row justify-content-center align-items-center">
                                    <img
                                        src={checkIcon}
                                        alt=""
                                        width={30}
                                        height={30}
                                        className="mr-2"
                                    />
                                </div> : null}
                            </div>

                            <div className="d-flex flex-row w-100 align-items-center justify-content-between mt-3 mb-3">
                                <div className="d-flex flex-row ">
                                    {/* <div className="img-person-mini mr-2">

                                </div> */}
                                    <span className="sarabun_M_300 text_black">
                                        {item.name}
                                    </span>
                                </div>
                                <span className="sarabun_M_300 text_gray mr-2">{moment(item.start).format("HH:mm")}</span>

                            </div>
                            <hr className="m-0" />
                        </div>
                    })}

                </div>
            })}
        </div>
    }



    return (

        <div className="bg-white p-0 content" >


            <div className='head-workdetail' >
                <div className='d-flex flex-row w-100 align-items-center justify-content-between px-3 py-3'
                    style={{
                        zIndex: 88,
                        position: 'absolute',
                        top: 0
                    }}>
                    <div
                        onClick={() => {
                            history.goBack()
                        }}
                        className="d-flex flex-row justify-content-center align-items-center"
                    >
                        <img src={icon_leftarrowwhite} alt='' className='icon-mini' />
                        <span className='sarabun_L_100 text_white'>
                            BACK
                        </span>
                    </div>

                    <div
                        onClick={() => { }}
                        className="d-flex flex-row justify-content-center align-items-center"
                    >
                        <i className="far fa-bell white fa-2x mr-5"></i>

                        <img src={icon_dots} className='icon-mini position-absolute ml-5 ' />
                    </div>
                </div>
                <div className="ml-4 mt-5 d-flex flex-column justify-content-start">
                    <span className="prompt_XXL text_white">
                        ลอตเลี้ยง 4
                    </span>
                    <span className="sarabun_XL text_blueteal">
                        ORD20200001
                    </span>
                    <div className="mt-2 status-darkturquoise-tag d-flex justify-content-center align-items-center">
                        <span className="sarabun_L_300 text_white">รอผสมพันธ์</span>
                    </div>
                </div>

                <div className="status-circle absolute d-flex justify-content-center align-items-center">
                    <img src={icon_Love} className='icon-default' />
                </div>
            </div>

            <div
                className='tab-status'>
                <div
                    onClick={() => {
                        settabSelect('homedash')
                    }}
                    className={`w-100 d-flex align-items-center justify-content-center ${tabSelect === 'homedash' ? 'tab-active' : 'tab-unactive '}`}
                >
                    <span
                        className={`prompt_L ${tabSelect === 'homedash' ? 'text_dodgerblue' : 'text_dimgray'}`}
                    >ภาพรวม</span>
                </div>

                <div style={{
                    width: 1,
                    height: 15,
                    border: "solid 1px #cdd9e9",
                }}>
                </div>

                <div
                    onClick={() => {
                        settabSelect('worker')
                    }}
                    className={`w-100 d-flex align-items-center justify-content-center ${tabSelect === 'worker' ? 'tab-active' : 'tab-unactive'} `}>
                    <span
                        className={` prompt_L ${tabSelect === 'worker' ? 'text_dodgerblue' : 'text_dimgray'}`}
                    >งานที่ต้องทำ</span>
                </div>

                <div style={{
                    width: 1,
                    height: 15,
                    border: "solid 1px #cdd9e9",
                }}>
                </div>
                <div
                    onClick={() => {
                        settabSelect('noti')
                    }}
                    className={`w-100 d-flex align-items-center justify-content-center ${tabSelect === 'noti' ? 'tab-active' : 'tab-unactive'}  `}>
                    <span
                        className={` prompt_L ${tabSelect === 'noti' ? 'text_dodgerblue' : 'text_dimgray'}`}
                    >การแจ้งเตือน</span>
                </div>

            </div>

            <div className={`body-workdetail ${tabSelect === 'homedash' ? 'bg-dimgray' : 'bg-white'}`}>
                {renderoverall()}
                {rendernoti()}
                {renderWorker()}
            </div>




        </div>
    );
})


// const HomeDash = () => {

// };

export default Worklotdetail;
