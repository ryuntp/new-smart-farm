import React, { useContext, useEffect, useState } from "react";
import Rootstore from "../../Stores/Rootstore";
import axios from "axios";
import axiosInstance from "../../Lib/AxiosInterceptor";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Label,
  FormGroup,
  Form,
  Input,
  FormText,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Col,
  Row,
  Alert,
} from "reactstrap";
import { observer } from "mobx-react-lite";
import group3 from "../../Assets/img/group-3@2x.png";
import { Redirect } from "react-router-dom";
const URL = "https://notwork-env.eba-isvimfp4.us-east-1.elasticbeanstalk.com";

const workAPI = {
  id: 1,
  name: "Leanne Graham",
  username: "Bret",
  email: "Sincere@april.biz",
};

export const Login = observer(() => {
  const [isOpen, setIsOpen] = useState(false);
  const [active, setActive] = useState(null);

  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [falseInput, setFalseInput] = useState(false);

  const [token, setToken] = useState(null);
  const [role, setRole] = useState(null);
  useEffect(() => {}, [role, token, falseInput]);

  const getInsectarium = async () => {
    let req = await axiosInstance.get("/all/place/allinsectarium");
    console.log("reqLogin ", req);
    return req?.data[0]?.array || [];
  };

  const handleClick = async () => {
    let login = await axios({
      method: "post",
      url: "http://acethainodev2-env.eba-a6yxvpca.us-east-2.elasticbeanstalk.com/login",
      data: {
        name: email,
        password,
      },
    });

    if (login.status === 200) {
      localStorage.setItem("id", login.data.id);
      localStorage.setItem("role", login.data.role);
      localStorage.setItem("token", login.data.token);
      localStorage.setItem("farm_id", login.data.farm_id);

      setRole(login.data.role);
      setToken(login.data.token);
      let getInsect = await getInsectarium();
      localStorage.setItem(
        "insectariums",
        JSON.stringify(getInsect) || JSON.stringify([])
      );
    } else {
      setFalseInput(true);
    }
  };

  let localRole = localStorage.getItem("role");
  if ((token || localStorage.getItem("token")) && localRole === "0") {
    return <Redirect to="/admin/homedash" />;
  } else if ((token || localStorage.getItem("token")) && localRole === "1") {
    return <Redirect to="/admin/HomeMenu" />;
  }

  return (
    <div className="d-flex login">
      <img
        className="logo-login"
        src={group3}
        alt="react-logo"
        width="74"
        height="68"
      />
      <span class="ACHETHAI">ACHETHAI</span>
      <span class="Cricket-Farming-Mana">Cricket Farming Management</span>
      <Container className="login-container">
        <Row className="">
          <Col className="ml-auto mr-auto" lg="4" md="6">
            <CardBody>
              <div className="mt-3">
                <Row>
                  <p className="prompt_XL text_black">Username</p>
                </Row>
                <Row>
                  <Input
                    name="email"
                    value={email}
                    placeholder="ชื่อผู้ใช้งาน"
                    type="text"
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </Row>
              </div>
              <div>
                <Row className="mt-3">
                  <p className="prompt_XL text_black">Password</p>
                </Row>
                <Row>
                  <Input
                    name="password"
                    value={password}
                    placeholder="รหัสผ่าน"
                    type="password"
                    autoComplete="off"
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </Row>
              </div>
              <Alert
                color="danger"
                style={{ display: falseInput ? "flex" : "none" }}
              >
                ชื่อผู้ใช้งาน/รหัสผ่านผิด
              </Alert>
            </CardBody>
            <div className="text-center">
              <button
                type="button"
                className="btn btn-primary w-75 prompt_XL text_white"
                onClick={() => handleClick()}
              >
                เข้าสู่ระบบ
              </button>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
});

export default Login;
