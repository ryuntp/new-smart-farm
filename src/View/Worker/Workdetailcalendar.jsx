
import React, { useContext, useEffect, useState } from "react";
import Appmodal from "../../Components/Modals/Appmodal";
import view_icon from '../../Assets/img/icon-general-view.png'
import checkIcon from "../../Assets/img/fb-check-a@2x.png";
import Addworkcalendar from "./Addworkcalendar";
import { Dropdown } from "react-bootstrap";
import icon_downarrow from '../../Assets/img/dropdown-down.png'
import { observer } from "mobx-react";
import UIstore from "../../Stores/UIstore";
import _ from 'lodash'
import CalendarsworkerStore from '../../Stores/CalendarsworkerStore'

import moment from "moment";
// import icon_leftarrowwhite from '../../Assets/img/dropdown-left-white.png'



// const stores = useContext(StoreContext);



export const Workdetailcalendar = observer((props) => {

    const uistore = useContext(UIstore);
    const calendarsworkerStore = useContext(CalendarsworkerStore);

    const calPoundList = (arr) => {
        let ans = [];
        let firstval;

        for (let [i, num] of arr.entries()) {
            if (num + 1 === arr[i + 1]) {
                if (!firstval) firstval = num;
            } else {
                if (firstval) {
                    if (firstval !== num) {
                        ans.push(`${firstval}-${num}`);
                    }
                } else {
                    ans.push(num.toString());
                }

                firstval = undefined;
            }
        }

        return ans;
    };

    useEffect(() => {

    }, []);

    return (

        <div className="bg-white p-0 w-100" >

            <Addworkcalendar />
            <Appmodal
                visible={uistore.updateworkcalendarmodal}
                nocontainer={true}
                toggle_click={() => {
                    uistore.Setupdateworkcalendarmodal(false)
                }}
            >
                <div
                    className='d-flex flex-column workdetail-modal'
                >

                    <div className="d-flex flex-row justify-content-between w-100 head-banner " style={{
                        backgroundColor: '#359bf4',
                    }}>
                        <div onClick={() => {
                            console.log(calendarsworkerStore.updateworkDateSelect);
                        }} className="d-flex flex-column justify-content-center ">
                            <span className="prompt_XXL text_white">
                                {calendarsworkerStore.updateworkDateSelect.worktemplatetype_name}
                            </span>
                            <span className="sarabun_L text_blueteal">
                                {moment(calendarsworkerStore.updateworkDateSelect.time).format('HH:mm')} น.

                            </span>
                        </div>

                        <div>
                            <div className="card-opacity">
                                <span className="prompt_XXL text_blueteal">
                                    {moment(calendarsworkerStore.updateworkDateSelect.time).format('DD')}
                                </span>
                                <span className="sarabun_M_300 text_blueteal">
                                    {moment(calendarsworkerStore.updateworkDateSelect.time).format('ddd')}
                                </span>
                            </div>
                        </div>



                    </div>


                    <div className="p-3">
                        <span className="sarabun_M text_dimgray">
                            รายละเอียดงานที่ให้
                        </span>
                        {_.get(calendarsworkerStore.updateworkDateSelect, 'workdetail', []).map((item, index) => {
                            return <div className="d-flex flex-row justify-content-between align-items-center">
                                <span className="prompt_L py-2 single-ellipsis w-75">• {item.question}</span>
                                <div className="d-flex flex-row align-items-center justify-content-center">
                                    <span className="prompt_L text_blueteal mr-2">
                                        {item.answer}
                                    </span>
                                </div>
                            </div>
                        })}

                    </div>


                    <hr className="m-0 p-0" />


                    <div className="p-3">
                        <span className="prompt_L mb-3">
                            ลอตผลิต {calendarsworkerStore.updateworkDateSelect.lot_name}
                        </span>
                        {calendarsworkerStore.updateworkDateSelect.poundlist !== '' ?
                            _.get(calendarsworkerStore.updateworkDateSelect, "poundlist", []).map((item, index) => {
                                return <div className="d-flex flex-column justify-content-center ">
                                    <span className="sarabun_M text_dimgray mt-3">
                                        {item.insectarium_name} : {calPoundList(item.pound_name)}
                                    </span>

                                </div>
                            })
                            : null}

                    </div>

                    <div className="p-3">
                        <div className="card-note">
                            <span className="text_orange prompt_M pb-2">
                                Note :
                            </span>
                            <span className="text_dimgray sarabun_M_300">
                                {calendarsworkerStore.updateworkDateSelect.note}
                            </span>
                        </div>
                    </div>


                    <div className="d-flex flex-row w-100 align-items-center justify-content-center mt-3">
                        <span className="sarabun_L_300 text_black">
                            ผู้ดูแล:
                        </span>
                        <div className="img-person-mini mx-3 mr-2">

                        </div>
                        <span className="sarabun_M_300 text_black">
                            {calendarsworkerStore.updateworkDateSelect.worker_name}
                        </span>
                    </div>


                    {Number(calendarsworkerStore.updateworkDateSelect.workstatus_id) === 1
                        ? <div>
                            <div className='position-relative  mt-4'>

                                <div className='line-dash not-absolute' />

                            </div>

                            <div className="d-flex flex-row align-items-center justify-content-between p-3">
                                <div className="d-flex flex-row align-items-center">
                                    <img
                                        src={checkIcon}
                                        alt=""
                                        width={30}
                                        height={30}
                                        className="mr-3"
                                    />
                                    <div className="d-flex flex-column justify-content-center">
                                        <span className="sarabun_M text_dimgray">
                                            เสร็จสิ้น
                                        </span>
                                        <span className="sarabun_S_300 text_dimgray">
                                            {moment(calendarsworkerStore.updateworkDateSelect.time).format("DD/MM/YY , HH:mm")}
                                        </span>
                                    </div>
                                </div>

                                <div
                                    onClick={() => {
                                        calendarsworkerStore.SetcardinfoSendapi(calendarsworkerStore.updateworkDateSelect.cardinfo)
                                        uistore.SetaddworkCalendar(true)
                                    }}
                                    className="card-watch-detail">
                                    <img src={view_icon} alt="" width={30} height={30} className="mr-2" />
                                    <span className="prompt_M text_dodgerblue">
                                        รายละเอียด
                                    </span>
                                </div>
                            </div>
                        </div>
                        : <div className="p-3  mt-3">
                            <div onClick={() => {
                                uistore.SetaddworkCalendar(true)
                            }} className="btn-confirm-100 d-flex flex-row justify-content-center align-items-center position-relative">

                                <span className="prompt_XL text_white"> เริ่มงานเลย </span>
                                <div className="box-righticon">

                                </div>
                            </div>


                        </div>}




                </div>
            </Appmodal>


        </div>
    );
})


// const HomeDash = () => {

// };

export default Workdetailcalendar;
