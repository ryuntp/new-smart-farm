
import React, { useContext, useEffect, useState } from "react";
import UIstore from "../../Stores/UIstore";
import icon_filter from '../../Assets/img/icon-general-filter.png'
import icon_leftarrow from '../../Assets/img/dropdown-left.png'
import icon_rightarrow from "../../Assets/img/dropdown-right.png";
import checkIcon from "../../Assets/img/fb-check-a@2x.png";
import Switch from "react-switch";
import CalendarsworkerStore from '../../Stores/CalendarsworkerStore'
import Filterworkcalendar from "./Filterworkcalendar";
import moment from "moment";

import Workdetailcalendar from "./Workdetailcalendar";
import axiosInstance from "../../Lib/AxiosInterceptor";
import { observer } from "mobx-react";
// const stores = useContext(StoreContext);



export const Calendardetail = observer((props) => {
    const uistore = useContext(UIstore);
    const calendarsworkerStore = useContext(CalendarsworkerStore);
    const [Dateselect, setDateselect] = useState({})
    const [hidework, sethidework] = useState(false);

    const handleChangeAll = (checked) => {
        sethidework(checked)
    }


    useEffect(() => {
        uistore.SetIsloading(true)
        const search = props.location.search;
        const Params = new URLSearchParams(search)
        var condi_option = calendarsworkerStore.option
        condi_option.startdate = Params.get('dateselect')
        condi_option.enddate = moment(new Date(new Date(Params.get('dateselect')).setDate(new Date(Params.get('dateselect')).getDate() + 30))).format("YYYY-MM-DD")
        setDateselect(Params.get('dateselect'))
        uistore.SetEnableHeader(true)
        uistore.SetEnablesubHeader(false)
        calendarsworkerStore.Setoption(condi_option)
        calendarsworkerStore.Getlist_workdata()
            .then(() => {
                uistore.SetIsloading(false)
            })
    }, [calendarsworkerStore, props, uistore]);



    const calPoundList = (arr) => {
        let ans = [];
        let firstval;
        for (let [i, num] of arr.entries()) {
            if ((Number(num) + 1) === Number(arr[i + 1])) {
                if (!firstval) firstval = num;
            } else {
                if (firstval) {
                    if (firstval !== num) {
                        ans.push(`${firstval}-${num}`);
                    }
                } else {
                    ans.push(num.toString());
                }

                firstval = undefined;
            }

        }

        return ans;
    };



    return (
        <div className="bg-white p-0" >
            <Filterworkcalendar
                Type={'detail'}
            />


            <Workdetailcalendar />

            <div className='d-flex flex-row justify-content-between align-items-center w-100 px-3'
                style={{
                    marginTop: "90px"
                }}>

                <div
                    onClick={() => {

                    }}
                    className="d-flex flex-row justify-content-center align-items-center"
                >

                    <div className="d-flex flex-row align-items-center">
                        <span className='prompt_XXL text_blueteal'>
                            {moment(Dateselect).format("MMMM")}
                        </span>
                        <span className='prompt_XL text_blueteal mt-1 ml-3'>
                            {moment(Dateselect).format("YYYY")}
                        </span>
                    </div>
                </div>

                <div className="box-changedate">
                    <div onClick={() => {
                        uistore.SetIsloading(true)
                        var condi_option = calendarsworkerStore.option
                        var prev_month = new Date(condi_option.startdate).getMonth()
                        var get_year = new Date(condi_option.startdate).getFullYear()
                        var lastDayOfMonth = new Date(get_year, prev_month, 0);
                        condi_option.startdate = `${get_year}-${prev_month}-01`
                        condi_option.enddate = `${get_year}-${prev_month}-${new Date(lastDayOfMonth).getDate()}`
                        setDateselect(condi_option.startdate)
                        calendarsworkerStore.Setoption(condi_option)
                        calendarsworkerStore.Getlist_workdata()
                            .then(() => {
                                uistore.SetIsloading(false)
                            })
                    }}>
                        <img src={icon_leftarrow} alt="" className="icon-mini" />
                    </div>

                    <span className="sarabun_L text_dimgray">วันนี้</span>
                    <div onClick={() => {
                        uistore.SetIsloading(true)
                        var condi_option = calendarsworkerStore.option
                        var next_month = new Date(condi_option.startdate).getMonth() + 2
                        var get_year = new Date(condi_option.startdate).getFullYear()
                        var lastDayOfMonth = new Date(get_year, next_month, 0);
                        condi_option.startdate = `${get_year}-${next_month}-01`
                        condi_option.enddate = `${get_year}-${next_month}-${new Date(lastDayOfMonth).getDate()}`
                        setDateselect(condi_option.startdate)
                        calendarsworkerStore.Setoption(condi_option)
                        calendarsworkerStore.Getlist_workdata()
                            .then(() => {
                                uistore.SetIsloading(false)
                            })
                    }}>
                        <img src={icon_rightarrow} alt="" className="icon-mini" />
                    </div>

                </div>

            </div>



            <div className='w-100 d-flex flex-row px-3 mt-3 filter-bar align-items-center' >

                <div onClick={() => { uistore.SetFilterworker(true) }}>
                    <img src={icon_filter} className='icon-mini' />
                </div>

                <div className='line-dash-vertical ml-3' />

                <div className='w-100 d-flex flex-row align-items-center ml-2'>
                    <span className='prompt_L_300 text_dimgray ml-2' >ซ่อนงานประจำวัน </span>
                   
                </div>

                <Switch uncheckedIcon={false} checkedIcon={false} onChange={handleChangeAll} onColor={'#1e76fe'} offColor="#e2e2e2" checked={hidework} />

            </div>

            <div className='list-production-lot'>
                <div
                    onClick={() => {
                        // createworkstore.Setmodal_splitlot(!createworkstore.modal_splitlot)
                    }}
                    className='btn-add-worklot mr-4'>

                </div>

                <div className='status-blueteal mr-4'>

                </div>

                <div className='status-paleturquoise mr-4'>

                </div>
                <div className='status-blueteal mr-4'>

                </div>

            </div>

            {calendarsworkerStore.list_workdata.map((item, index) => {
                return <div key={index} className="d-flex flex-column">
                    <div className="d-flex flex-row justify-content-between align-items-center w-100 px-3 mt-4">
                        <div className="d-flex flex-row align-items-center" >
                            <span className="prompt_XXL text_blueteal">{moment(item.time).format("DD")}</span>
                            <span className="sarabun_L_300 text_blueteal ml-2">{moment(item.time).format("ddd")}</span>
                        </div>
                        <span className="sarabun_L_300 text_gray">{item.worklist.length} รายการ</span>
                    </div>

                    {item.worklist.map((item, index) => {
                        return <div
                            onClick={() => {
                                calendarsworkerStore.Getoneworkdata(item.id)
                                    .then(() => {
                                        uistore.Setupdateworkcalendarmodal(true)
                                    })
                                    .catch((err) => {
                                        console.log(err);
                                    })
                            }}
                            key={index}
                            className="card-workdate-detail">
                            <div className="d-flex flex-row align-items-center justify-content-between w-100">
                                <div className="d-flex flex-column">
                                    <span className="prompt_XL text_blueteal">{item.title}</span>
                                    <div className="d-flex flex-row align-items-center">
                                        <span className="sarabun_M text_dimgray">ลอต {item.lot_name} :</span>
                                        {item.poundlist.map((itemin, index) => {
                                            return <span className="sarabun_M_300 text_dimgray">
                                                &nbsp; {calPoundList(itemin.pound_name)}
                                            </span>
                                        })}

                                    </div>
                                </div>
                                {item.workstatus === 0 ? <div className="d-flex flex-row justify-content-center align-items-center">
                                    <img
                                        src={checkIcon}
                                        alt=""
                                        width={30}
                                        height={30}
                                        className="mr-2"
                                    />
                                </div> : null}
                            </div>

                            <div className="d-flex flex-row w-100 align-items-center justify-content-between mt-3 mb-3">
                                <div className="d-flex flex-row ">
                                    {/* <div className="img-person-mini mr-2">

                                </div> */}
                                    <span className="sarabun_M_300 text_black">
                                        {item.name}
                                    </span>
                                </div>
                                <span className="sarabun_M_300 text_gray mr-2">{moment(item.start).format("HH:mm")}</span>

                            </div>
                            <hr className="m-0" />
                        </div>
                    })}

                </div>
            })}



        </div>
    );
})



export default Calendardetail;
