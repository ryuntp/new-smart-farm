
import React, { useContext, useEffect, useState } from "react";
import Appmodal from "../../Components/Modals/Appmodal";
import icon_leftarrowwhite from '../../Assets/img/dropdown-left-white.png'
import icon_leftarrow from '../../Assets/img/dropdown-left.png'
import icon_midelete from "../../Assets/img/mi-delete.png";
import icon_view3x from '../../Assets/img/ico-view3x.png'
import icon_edit3x from '../../Assets/img/ico-css-edit3x.png'
import checkIcon from "../../Assets/img/fb-check-a@2x.png";
import cancelIcon from "../../Assets/img/cancel.png";
import { Dropdown } from "react-bootstrap";
import { handleSelectPounds } from "../../util/handleInput";
import CalendarsworkerStore from '../../Stores/CalendarsworkerStore'
import _ from 'lodash'
import Switch from "react-switch";
import { Input, } from "reactstrap";
// import icon_leftarrowwhite from '../../Assets/img/dropdown-left-white.png'
import icon_downarrow from '../../Assets/img/dropdown-down.png'
import { observer } from "mobx-react";
import UIstore from "../../Stores/UIstore";
import moment from "moment";
import Poundlist from "../../Components/Poundlist";
import Rootstore from "../../Stores/Rootstore";






export const Addworkcalendar = observer((props) => {

    const uistore = useContext(UIstore);
    const calendarsworkerStore = useContext(CalendarsworkerStore);
    const rootstore = useContext(Rootstore);
    const [notemodal, setnotemodal] = useState(false);
    const [checkedAll, setcheck] = useState(false);
    const [note, setnote] = useState('')
    const [splitpoundmodal, setsplitpoundmodal] = useState(false)
    const [isUpdate, SetisUpdate] = useState(false)
    const [PoundlistCardinfo, setPoundlistCardinfo] = useState([{
        insectarium_name: "",
        pound_name: [],
        Error_pound: false,
    }])

    const [poundlistSplitlot, setpoundlistSplitlot] = useState([
        {
            label: 'ทุกวัน',
            value: 'ทุกวัน',
        },
    ])

    const handleChange = (checked) => {
        if (checked === false) {
            return
        }
        setcheck(checked)
        setnote('')
        SetisUpdate(false)
        var poundlist = _.get(calendarsworkerStore.updateworkDateSelect, "poundlist", [])

        var insectarium_name_list = [{}]
        for (const key in poundlist) {
            if (Object.hasOwnProperty.call(poundlist, key)) {
                const element = poundlist[key];
                var dateElement = {
                    label: element.insectarium_name,
                    value: element.insectarium_name,
                }

                insectarium_name_list.push(dateElement)
            }
        }
        setpoundlistSplitlot(insectarium_name_list)
        setsplitpoundmodal(true)
        calendarsworkerStore.SetworkdetailBymodal(_.cloneDeep(_.get(calendarsworkerStore.updateworkDateSelect, "workdetail", [])))
    }

    const calPoundList = (arr) => {
        let ans = [];
        let firstval;
        for (let [i, num] of arr.entries()) {
            if ((Number(num) + 1) === Number(arr[i + 1])) {
                if (!firstval) firstval = num;
            } else {
                if (firstval) {
                    if (firstval !== num) {
                        ans.push(`${firstval}-${num}`);
                    }
                } else {
                    ans.push(num.toString());
                }

                firstval = undefined;
            }

        }

        return ans;
    };



    const render_listTaskworker = () => {

        if (calendarsworkerStore.cardinfoSendapi.length === 0) {
            return;
        }
        return <div>
            {Number(calendarsworkerStore.updateworkDateSelect.workstatus_id) === 0
                ? <div>
                    <div className="d-flex flex-row justify-content-between p-3">
                        <span className="prompt_L_300 text_dimgray">ระบุรายละเอียดรายบ่อ</span>
                        <Switch uncheckedIcon={false} checkedIcon={false} onChange={handleChange} onColor={'#1e76fe'} offColor="#e2e2e2" checked={checkedAll} />
                    </div>
                    <div className='position-relative mx-3 mt-0 px-0 '>

                        <div className='line-dash not-absolute' />

                    </div>
                </div> : null
            }

            <div className="d-flex justify-content-center align-items-center d-flex flex-column mt-4 px-3">

                <div className="d-flex flex-row justify-content-between w-100 align-items-center ">

                    <span className="prompt_L">
                        รายการที่ทำ
                    </span>

                    {Number(calendarsworkerStore.updateworkDateSelect.workstatus_id) === 0
                        ? <div onClick={() => {
                            calendarsworkerStore.SetcardinfoSendapi([])
                            setcheck(false)
                            calendarsworkerStore.SetpoundlistForcheck(_.get(calendarsworkerStore.updateworkDateSelect, "poundlist", []))
                        }} className="d-flex flex-row align-items-center justify-content-center">


                            <span className="prompt_M_200 text_orangefat">
                                <u> ล้างข้อมูล</u>
                            </span>
                            <img src={icon_midelete} alt="" className='icon-minismall ml-2' />
                        </div> : null
                    }



                    {/* <div onClick={() => {
                        setsplitpoundmodal(true);
                        SetisUpdate(true)
                    }} className="d-flex flex-row">
                        <img src={icon_edit3x} alt="" className='icon-mini mr-2' />

                        <span className="prompt_M_200 text_dodgerblue">
                            <u> แก้ไขรายการ</u>
                        </span>
                    </div> */}

                </div>




                {calendarsworkerStore.cardinfoSendapi.map((item, index) => {
                    return <div className="card-task-worker mt-3 mb-3">
                        <div className="d-flex flex-row justify-content-between align-items-center p-3 py-2">
                            <div className="d-flex flex-row  align-items-center">
                                {item.poundlist.map((itemin, index) => {
                                    return <span className="prompt_XL text_blueteal">
                                        {itemin.insectarium_name}
                                        &nbsp;
                                        {index >= 0 && (index + 1) !== item.poundlist.length ? ',' : null}
                                        &nbsp;
                                    </span>
                                })} :{item.poundlist.map((itemin, index) => {
                                    return <span className="prompt_XL_300 text_blueteal">
                                        &nbsp; {calPoundList(itemin.pound_name)}
                                    </span>
                                })}
                            </div>


                            <div onClick={() => {
                                calendarsworkerStore.SetworkdetailBymodal(_.cloneDeep(_.get(calendarsworkerStore.cardinfoSendapi[index], "workdetail", [])))
                                calendarsworkerStore.SetcardinfoSendapi_index(index)
                                setPoundlistCardinfo(item.poundlist)

                                setsplitpoundmodal(true)
                                SetisUpdate(true)
                            }}>
                                <img src={icon_edit3x} alt="" className='icon-minismall ml-2' />
                            </div>
                        </div>


                        <div className='position-relative mx-3 mt-0 px-0 '>

                            <div className='line-dash not-absolute' />

                        </div>

                        {_.get(item, 'workdetail', []).map((item, index) => {
                            return <div className="d-flex flex-row justify-content-between align-items-center px-3 p-2">
                                <span className="prompt_L py-2 single-ellipsis w-75">• {item.question}</span>
                                <div className="d-flex flex-row align-items-center justify-content-center">
                                    <span className="prompt_L text_blueteal mr-2">
                                        {item.answer}
                                    </span>
                                </div>
                            </div>
                        })}



                        {item.note ? <div>
                            <hr className="m-0" />

                            <div className=" m-3 d-flex flex-column">
                                <span className="prompt_L text_blueteal"> Note : </span>
                                <span className="sarabun_M_300 text_dimgray">
                                    {item.note}
                                </span>
                            </div>
                        </div> : null}

                        <hr className="m-0" />

                        <div className="d-flex flex-row align-items-center justify-content-between p-3 py-2">
                            <div className="d-flex flex-row">

                                <div className="icon-mini rounded-circle bg-success mr-2">

                                </div>

                                <span className="sarabun_M_300">{item.worker_name}</span>
                            </div>

                            <div className="sarabun_M_300 text_dimgray">
                                {moment(item.time).format("DD/MM/YY , HH:mm")}
                            </div>
                        </div>


                    </div>
                })}


            </div>


            {Number(calendarsworkerStore.updateworkDateSelect.workstatus_id) === 1
                ? <div className="px-3">
                    <div className="d-flex flex-row align-items-center justify-content-between">

                        <div className="d-flex flex-row align-items-center ">
                            <img
                                src={checkIcon}
                                alt=""
                                width={30}
                                height={30}
                                className="mr-3"
                            />
                            <span className="sarabun_M text_dimgray">
                                เสร็จสิ้น
                            </span>
                        </div>

                        <span className="sarabun_S_300 text_dimgray">
                            {moment(calendarsworkerStore.updateworkDateSelect.time).format("DD/MM/YY , HH:mm")}
                        </span>
                    </div>
                </div>
                :
                <div>
                    {calendarsworkerStore.poundlistForcheck.length !== 0
                        ? <div onClick={() => {
                            calendarsworkerStore.SetworkdetailBymodal(_.cloneDeep(_.get(calendarsworkerStore.updateworkDateSelect, "workdetail", [])))
                            setsplitpoundmodal(true)
                            SetisUpdate(false)
                        }} className="d-flex flex-row align-item-center mt-4 px-3">
                            <div className="add-circle-darkturquoise mr-3 " />
                            <span className="prompt_XL">ระบุงานที่ทำเพิ่ม</span>
                        </div>
                        : <div className="d-flex flex-row px-3 pb-3 align-items-center justify-content-between">
                            <div className="d-flex flex-row align-items-center">
                                <img
                                    src={checkIcon}
                                    alt=""
                                    width={30}
                                    height={30}
                                    className="mr-2"
                                />
                                <span className="sarabun_M text_dimgray">
                                    ระบุครบแล้ว
                                </span>
                            </div>
                        </div>}

                    < div className="px-3 d-flex flex-column w-100">

                        <div
                            onClick={() => {
                                calendarsworkerStore.Postupdatework()
                            }}
                            className="btn-bigconfirm d-flex justify-content-center align-items-center mt-5"
                        >
                            <span className="prompt_XL text_white">บันทึก</span>
                        </div>
                    </div>
                </div>
            }





        </div >
    }


    const render_detailTaskworker = () => {
        if (calendarsworkerStore.cardinfoSendapi.length !== 0) {
            return;
        }
        return <div style={{ paddingBottom: '10rem' }}>
            <div className="d-flex flex-row justify-content-between p-3">
                <span className="prompt_L_300 text_dimgray">ระบุรายละเอียดรายบ่อ</span>
                <Switch uncheckedIcon={false} checkedIcon={false} onChange={handleChange} onColor={'#1e76fe'} offColor="#e2e2e2" checked={checkedAll} />
            </div>
            <div className='position-relative mx-3 mt-0 px-0 '>

                <div className='line-dash not-absolute' />

            </div>

            {_.get(calendarsworkerStore.updateworkDateSelect, 'workdetail', []).map((item, index) => {
                if (Number(item.type) === 1) {
                    return <div className="p-3 ">
                        <span className="prompt_XL ">{item.question}</span>
                        <input
                            type="text"
                            value={item.answer}
                            onChange={(e) => {
                                item.answer = e.target.value
                            }}
                            className="drop-downwork-blue mt-3" />

                    </div>
                } else {
                    return <div className="p-3 pt-3">
                        <span className="prompt_XL ">{item.question}</span>

                        <div className="d-flex flex-row ">
                            <div className='checklist-box mt-3'>
                                <div onClick={() => {
                                    item.answer = true
                                }} className='d-flex flex-row align-items-center mr-3'>

                                    {item.answer ? <img
                                        src={checkIcon}
                                        alt=""
                                        width={30}
                                        height={30}
                                        className="mr-2"
                                    /> : null}
                                    <span className='sarabun_L '>Yes</span>
                                </div>

                                <div className='mx-5' style={{
                                    width: 2,
                                    height: 15,
                                    backgroundColor: "#cdd9e9"
                                }} />

                                <div
                                    onClick={() => {
                                        item.answer = false
                                    }} className='d-flex flex-row align-items-center justify-content-center ml-3'>
                                    {!item.answer ? <img
                                        src={cancelIcon}
                                        alt=""
                                        width={30}
                                        height={30}
                                        className="mr-2"
                                    /> : null}
                                    <span className='sarabun_L'>No</span>
                                </div>
                            </div>


                        </div>
                    </div>
                }

            })}


            <div style={{
                position: 'fixed',
                bottom: 0,
                width: '100%'
            }}>

                <div className='position-relative mx-3 mt-0 px-0 '>
                    <div className='line-dash not-absolute' />
                </div>


                <div className="d-flex flex-row justify-content-between align-items-center p-3">
                    <span className="prompt_XL ">
                        หมายเหตุ
                    </span>
                    {
                        note === '' ? <div onClick={() => {
                            setnotemodal(true)
                        }} className='add-circle-darkturquoise'>
                            <span className="text_white"></span>
                        </div> :
                            <div onClick={() => {
                                setnotemodal(true)
                            }} className='d-flex flex-row align-items-center'>
                                <span className=" text_black prompt_L mr-3 ">{note}</span>
                                <div className='add-circle-darkturquoise'>
                                    <span className="text_white"></span>
                                </div>
                            </div>
                    }
                </div>


                <div className="mx-3 mb-3">
                    <div onClick={() => {
                        var answer = []
                        var workdetail = _.get(calendarsworkerStore.updateworkDateSelect, "workdetail", [])
                        for (const key in workdetail) {
                            if (Object.hasOwnProperty.call(workdetail, key)) {
                                const element = workdetail[key];
                                answer.push(element.answer)
                            }
                        }
                        var poundlist = _.get(calendarsworkerStore.updateworkDateSelect, "poundlist", [])
                        var cardinfo = []
                        var packDatasend = {
                            poundlist: [],
                            answer: answer,
                            workdetail: workdetail,
                            worker_id: calendarsworkerStore.updateworkDateSelect.worker_id,
                            worker_name: calendarsworkerStore.updateworkDateSelect.worker_name,
                            time: new Date(),
                            note: note
                        }
                        for (const key in poundlist) {
                            if (Object.hasOwnProperty.call(poundlist, key)) {
                                const element = poundlist[key];
                                packDatasend.poundlist.push(element)
                                cardinfo.push(packDatasend)
                            }
                        }

                        calendarsworkerStore.SetcardinfoSendapi(cardinfo)
                        calendarsworkerStore.Checkpoundlist()


                    }} className='btn-confirm-100 d-flex justify-content-center align-items-center  '>
                        <span className='prompt_XL text_white'>
                            บันทึก
                        </span>
                    </div>
                </div>


            </div>


        </div>
    }
    const renderAnswer = (Type, item, index) => {
        if (Number(item.type) === 1) {
            return <div className="p-3 ">
                <span className="prompt_XL ">{item.question}</span>
                <input
                    type="text"
                    value={item.answer}
                    onChange={(e) => {
                        item.answer = e.target.value
                        console.log("item.answer", item.answer);
                    }}
                    className="drop-downwork-blue mt-3" />

            </div>
        } else {
            return <div className="p-3 pt-3">
                <span className="prompt_XL ">{item.question}</span>

                <div className="d-flex flex-row ">
                    <div className='checklist-box mt-3'>
                        <div onClick={() => {
                            item.answer = true
                        }} className='d-flex flex-row align-items-center mr-3'>

                            {item.answer ? <img
                                src={checkIcon}
                                alt=""
                                width={30}
                                height={30}
                                className="mr-2"
                            /> : null}
                            <span className='sarabun_L '>Yes</span>
                        </div>

                        <div className='mx-5' style={{
                            width: 2,
                            height: 15,
                            backgroundColor: "#cdd9e9"
                        }} />

                        <div
                            onClick={() => {
                                item.answer = false
                            }} className='d-flex flex-row align-items-center justify-content-center ml-3'>
                            {!item.answer ? <img
                                src={cancelIcon}
                                alt=""
                                width={30}
                                height={30}
                                className="mr-2"
                            /> : null}
                            <span className='sarabun_L'>No</span>
                        </div>
                    </div>


                </div>
            </div>
        }
    }

    return (

        <div className="bg-white p-0" >
            <Appmodal
                visible={uistore.addworkCalendar}
                nocontainer={false}
            >
                <div className='addwork-modal'>
                    <div className='head-content'>
                        <div
                            onClick={() => {
                                uistore.SetaddworkCalendar(false)
                            }}
                            className="d-flex flex-row justify-content-center align-items-center"
                            style={{
                                position: 'absolute',
                                top: 20,
                                left: 15
                            }}>
                            <img src={icon_leftarrowwhite} alt='' className='icon-mini' />
                            <span className='sarabun_L_100 text_white'>
                                BACK
                            </span>
                        </div>

                        <div className='text-content'>
                            <span className='prompt_XXL text_white'>
                                ให้อาหารจิ้งหรีด
                            </span>

                            <span className='sarabun_L text_blueteal'>
                                ลอตผลิต 04-1A
                            </span>

                            <div className='view-tag mt-2 d-flex flex-row justify-content-center align-items-center'>
                                <img src={icon_view3x} alt="" className='icon-mini mr-2' />

                                <span className='prompt_M text_orange'>รายละเอียด</span>
                            </div>
                        </div>


                    </div>

                    {render_listTaskworker()}

                    {render_detailTaskworker()}

                </div>
            </Appmodal>


            <Appmodal
                visible={splitpoundmodal}
                nocontainer={true}
            >
                <div
                    className='d-flex flex-column adddetail-modal'
                >
                    {calendarsworkerStore.workdetailBymodal.map((item, index) => {
                        return renderAnswer(Number(item.type), item, index)
                    })}

                    <Poundlist
                        poundlistSplitlot={poundlistSplitlot}
                        title={'บ่อที่ทำ'}
                        isUpdate={isUpdate}
                        poundlistForcheck={calendarsworkerStore.poundlistForcheck}
                        cardinfo={PoundlistCardinfo}
                        provider={rootstore}
                        checked={true}
                    />

                    <div className="py-4" style={{
                        position: 'fixed',
                        bottom: 10,
                        width: '94%'
                    }}>

                        <div className='position-relative mx-3 mt-0 px-0 '>
                            <div className='line-dash not-absolute' />
                        </div>


                        <div onClick={() => {
                            setnotemodal(true)
                        }} className="d-flex flex-row justify-content-between align-items-center p-3 mt-2">
                            <span className="prompt_XL ">
                                หมายเหตุ
                            </span>

                            {
                                note === '' ? <div onClick={() => {
                                    setnotemodal(true)
                                }} className='add-circle-darkturquoise'>
                                    <span className="text_white"></span>
                                </div> :
                                    <div onClick={() => {
                                        setnotemodal(true)
                                    }} className='d-flex flex-row align-items-center'>
                                        <span className=" text_black prompt_L mr-3 ">{note}</span>
                                        <div className='add-circle-darkturquoise'>
                                            <span className="text_white"></span>
                                        </div>
                                    </div>
                            }
                        </div>


                        <div className="w-100">
                            <div className="d-flex flex-row justify-content-between w-100 px-3">
                                <div
                                    onClick={() => {
                                        setsplitpoundmodal(false)
                                        setcheck(false)
                                        setnote('')
                                    }}
                                    className="btn-minicancle d-flex justify-content-center align-items-center"
                                >
                                    <span className="prompt_XL text_dodgerblue">ยกเลิก</span>
                                </div>
                                <div
                                    onClick={() => {
                                        setsplitpoundmodal(false)
                                        setnote('')
                                        var answer = []
                                        var workdetail = []
                                        workdetail = calendarsworkerStore.workdetailBymodal
                                        for (const key in workdetail) {
                                            if (Object.hasOwnProperty.call(workdetail, key)) {
                                                const element = workdetail[key];
                                                answer.push(element.answer)
                                            }
                                        }
                                        var packDatasend = {
                                            poundlist: [],
                                            answer: answer,
                                            workdetail: workdetail,
                                            worker_id: calendarsworkerStore.updateworkDateSelect.worker_id,
                                            worker_name: calendarsworkerStore.updateworkDateSelect.worker_name,
                                            time: new Date(),
                                            note: note
                                        }
                                        for (const key in rootstore.datacardinfo) {
                                            if (rootstore.datacardinfo.hasOwnProperty.call(rootstore.datacardinfo, key)) {
                                                const element = rootstore.datacardinfo[key];
                                                packDatasend.poundlist.push(element)
                                            }
                                        }

                                        if (isUpdate === false) {
                                            calendarsworkerStore.AddcardinfoSendapi(packDatasend)
                                        } else {
                                            calendarsworkerStore.SetcardinfoSendapi_Followindex(packDatasend, calendarsworkerStore.cardinfoSendapi_index)
                                        }
                                        calendarsworkerStore.Checkpoundlist()
                                        calendarsworkerStore.SetworkdetailBymodal(_.cloneDeep(_.get(calendarsworkerStore.updateworkDateSelect, "workdetail", [])))
                                    }}
                                    className="btn-miniconfirm d-flex justify-content-center align-items-center"
                                >
                                    <span className="prompt_XL text_white">ยืนยัน</span>
                                </div>
                            </div>
                        </div>


                    </div>



                </div>
            </Appmodal>

            <Appmodal
                visible={notemodal}

                toggle_click={() => {
                    setnotemodal(false)
                }}
                nocontainer={true}
                className={'d-flex justify-content-center align-items-center'}
            >


                <div
                    className='d-flex flex-column '
                    style={{
                        width: '100%',
                        minHeight: 600,
                        backgroundColor: '#fff',
                    }}>

                    <div className='note-headmodal d-flex align-items-center w-100'>
                        <div
                            onClick={() => {
                                setnotemodal(false)
                            }}
                            className="d-flex flex-row justify-content-center align-items-center"
                        >
                            <img src={icon_leftarrow} alt='' className='icon-mini' />
                            <span className='sarabun_L_100 text_black'>
                                BACK
                            </span>
                        </div>
                    </div>

                    <div className="px-4 mt-4">

                        <div className='d-flex flex-row justify-content-between align-items-center'>

                            <span className='prompt_XL text_black'>หมายเหตุ</span>

                            <div>
                                <img src={icon_midelete} className='icon-minismall mr-3' />
                                <span className='prompt_M_200 text-decoration-underline text_tomato'>ลบ</span>
                            </div>

                        </div>

                    </div>


                    <div className='mx-4 mt-4'>
                        <textarea
                            value={note}
                            onChange={(e) => {
                                setnote(e.target.value)
                            }}
                            className='input-area-addnote'
                            id="w3review"
                            name="w3review"
                            rows="4"
                            cols="50"
                        ></textarea>
                    </div>

                    <div className='d-flex flex-row mx-4 justify-content-between mt-4'>
                        <div
                            onClick={() => {
                                setnotemodal(false)
                            }}
                            className='btn-minicancle d-flex justify-content-center align-items-center'>
                            <span className="prompt_XL text_dodgerblue">ยกเลิก</span>
                        </div>
                        <div
                            onClick={() => {
                                // breedstore.Setnotebreed(note)
                                setnotemodal(false)
                            }}
                            className='btn-miniconfirm d-flex justify-content-center align-items-center'>
                            <span className="prompt_XL text_white">ยืนยัน</span>
                        </div>
                    </div>



                </div>
            </Appmodal>
            {/*  */}

        </div>
    );
})


// const HomeDash = () => {

// };

export default Addworkcalendar;
