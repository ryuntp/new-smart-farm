
import React, { forwardRef, useContext, useEffect, useState } from "react";
import axios from 'axios'
import icon_search from '../../Assets/img/ico-search.png'
import { observer } from 'mobx-react-lite'
import icon_leftarrow from '../../Assets/img/dropdown-left.png'
import icon_downarrow from '../../Assets/img/dropdown-down.png'
import icon_calendar from '../../Assets/img/icon-nav-calendar-blue.png'
import icon_close from '../../Assets/img/close.png'
import view_icon from '../../Assets/img/icon-general-view.png'
import Collapse from '../../Components/Collapsed/Collapsed'
import Appmodal from "../../Components/Modals/Appmodal";
import DatePicker from "react-datepicker";
import { Input, } from "reactstrap";
import UIstore from "../../Stores/UIstore";
import CalendarsworkerStore from "../../Stores/CalendarsworkerStore";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import Workersearchmodal from "../../Components/Modals/Workersearchmodal";


// const stores = useContext(StoreContext);



export const Filterworkcalendar = observer((props) => {
    const uistore = useContext(UIstore);
    const calendarsworkerStore = useContext(CalendarsworkerStore);
    const [isExpanded_filterdate, setExpanded_filterdate] = useState(false)
    const [isExpanded_filterharvestdate, setExpanded_filterharvestdate] = useState(false)
    const [isExpanded_filterspecies, setExpanded_filterspecies] = useState(false)
    useEffect(() => {
        calendarsworkerStore.Getworktemplete()
    }, [calendarsworkerStore, props]);


    const render_work = () => {
        return <div className='d-flex flex-row w-100 mb-2'>

        </div>
    }
    const submitWorker = () => {
        if (props.Type === 'detail') {
            var condi_option = calendarsworkerStore.option
            condi_option.workerlist = []
            for (const key in calendarsworkerStore.workerselectdata) {
                if (Object.hasOwnProperty.call(calendarsworkerStore.workerselectdata, key)) {
                    const element = calendarsworkerStore.workerselectdata[key];
                    condi_option.workerlist.push(Number(element.id))
                }
            }
            calendarsworkerStore.Setoption(condi_option)
        } else {
            var condi_optioncalendars = calendarsworkerStore.optioncalendars
            condi_optioncalendars.workerlist = []
            for (const key in calendarsworkerStore.workerselectdatacalendar) {
                if (Object.hasOwnProperty.call(calendarsworkerStore.workerselectdatacalendar, key)) {
                    const element = calendarsworkerStore.workerselectdatacalendar[key];
                    condi_optioncalendars.workerlist.push(Number(element.id))
                }
            }
            calendarsworkerStore.Setoption(condi_optioncalendars)
        }

    }

    const selectWorker = (item) => {
        calendarsworkerStore.addworkerSelectdata(item)
    }


    const renderBodyworkerlist = () => {
        if (props.Type === 'detail') {
            return <div>
                <div className='d-flex flex-row justify-content-start mx-3 mb-3'>

                    {calendarsworkerStore.workerselectdata.map((item, index) => {
                        return <div key={index} className="tag-paleturquoise d-flex flex-row align-items-center justify-content-around pr-3 mr-3">
                            <div className="img-person-mini mr-1">

                            </div>

                            <span className="sarabun_M text_black">{item.name}</span>
                            <div onClick={() => {
                                calendarsworkerStore.removeworkerSelectdata(index)
                            }}>
                                <img src={icon_close} className='ml-3' alt="" style={{ width: 9, height: 9 }} />
                            </div>
                        </div>
                    })}
                </div>
            </div>
        }
        else {
            return <div>
                <div className='d-flex flex-row justify-content-start mx-3 mb-3'>

                    {calendarsworkerStore.workerselectdatacalendar.map((item, index) => {
                        return <div key={index} className="tag-paleturquoise d-flex flex-row align-items-center justify-content-around pr-3 mr-3">
                            <div className="img-person-mini mr-1">

                            </div>

                            <span className="sarabun_M text_black">{item.name}</span>
                            <div onClick={() => {
                                calendarsworkerStore.removeworkerSelectdatacalendar(index)
                            }}>
                                <img src={icon_close} className='ml-3' alt="" style={{ width: 9, height: 9 }} />
                            </div>
                        </div>
                    })}
                </div>
            </div>
        }


    }

    const renderBodyWorkdate = () => {
        if (props.Type === 'detail') {
            return <div className='d-flex flex-column w-100 mb-2'>
                <div className='list-harvest-filter'>
                    <div onClick={() => {
                        // createworkstore.Setmodal_splitlot(!createworkstore.modal_splitlot)
                    }}
                        className='btn-add-worklot mr-4'>

                    </div>

                    <div
                        onClick={() => {
                            var condi_option = calendarsworkerStore.option
                            condi_option.dateoption = 'custom'
                            calendarsworkerStore.Setoption(condi_option)
                        }}
                        className={`mr-4 ${calendarsworkerStore.option.dateoption === 'custom' ? "status-blueteal" : "status-paleturquoise"}`}>
                        <span className={`prompt_L ${calendarsworkerStore.option.dateoption === 'custom' ? "text_white" : "text_dodgerblue"}`}>กำหนดเอง</span>
                    </div>

                    <div onClick={() => {

                        var condi_option = calendarsworkerStore.option
                        condi_option.dateoption = '3lm'
                        calendarsworkerStore.Setoption(condi_option)
                    }} className={`mr-4 ${calendarsworkerStore.option.dateoption === '3lm' ? "status-blueteal" : "status-paleturquoise"}`}>
                        <span className={`prompt_L ${calendarsworkerStore.option.dateoption === '3lm' ? "text_white" : "text_dodgerblue"}`}>3เดือนล่าสุด</span>
                    </div>
                    <div onClick={() => {
                        var condi_option = calendarsworkerStore.option
                        condi_option.dateoption = 'ly'
                        calendarsworkerStore.Setoption(condi_option)
                    }} className={`mr-4 ${calendarsworkerStore.option.dateoption === 'ly' ? "status-blueteal" : "status-paleturquoise"}`}>
                        <span className={`prompt_L ${calendarsworkerStore.option.dateoption === 'ly' ? "text_white" : "text_dodgerblue"}`}>ปีนี้</span>
                    </div>

                </div>


                {calendarsworkerStore.option.dateoption === 'custom' ?
                    <div className='d-flex flex-row'>
                        <div className='w-50 mx-3'>
                            <DatePicker
                                selected={new Date(calendarsworkerStore.option.startdate)}
                                onChange={(date) => {
                                    console.log('date', date.toISOString);
                                    var condi_option = calendarsworkerStore.option
                                    condi_option.startdate = date
                                    calendarsworkerStore.Setoption(condi_option)
                                }}
                                customInput={<CustomInput />}
                            />
                        </div>

                        <div className='w-50 mx-3'>
                            <DatePicker
                                selected={new Date(calendarsworkerStore.option.enddate)}
                                onChange={(date) => {
                                    var condi_option = calendarsworkerStore.option
                                    condi_option.enddate = date
                                    calendarsworkerStore.Setoption(condi_option)
                                }}
                                minDate={calendarsworkerStore.option.startdate}
                                customInput={<CustomInput />}
                            />

                        </div>
                    </div> : null
                }
            </div>
        } else {
            return <div className='d-flex flex-column w-100 mb-2'>
                <div className='list-harvest-filter'>
                    <div onClick={() => {
                        // createworkstore.Setmodal_splitlot(!createworkstore.modal_splitlot)
                    }}
                        className='btn-add-worklot mr-4'>

                    </div>

                    <div
                        onClick={() => {
                            var condi_option = calendarsworkerStore.optioncalendars
                            condi_option.dateoption = 'custom'
                            calendarsworkerStore.Setoptioncalendars(condi_option)
                        }}
                        className={`mr-4 ${calendarsworkerStore.optioncalendars.dateoption === 'custom' ? "status-blueteal" : "status-paleturquoise"}`}>
                        <span className={`prompt_L ${calendarsworkerStore.optioncalendars.dateoption === 'custom' ? "text_white" : "text_dodgerblue"}`}>กำหนดเอง</span>
                    </div>

                    <div onClick={() => {

                        var condi_option = calendarsworkerStore.optioncalendars
                        condi_option.dateoption = '3lm'
                        calendarsworkerStore.Setoptioncalendars(condi_option)
                    }} className={`mr-4 ${calendarsworkerStore.optioncalendars.dateoption === '3lm' ? "status-blueteal" : "status-paleturquoise"}`}>
                        <span className={`prompt_L ${calendarsworkerStore.optioncalendars.dateoption === '3lm' ? "text_white" : "text_dodgerblue"}`}>3เดือนล่าสุด</span>
                    </div>
                    <div onClick={() => {
                        var condi_option = calendarsworkerStore.optioncalendars
                        condi_option.dateoption = 'ly'
                        calendarsworkerStore.Setoptioncalendars(condi_option)
                    }} className={`mr-4 ${calendarsworkerStore.optioncalendars.dateoption === 'ly' ? "status-blueteal" : "status-paleturquoise"}`}>
                        <span className={`prompt_L ${calendarsworkerStore.optioncalendars.dateoption === 'ly' ? "text_white" : "text_dodgerblue"}`}>ปีนี้</span>
                    </div>

                </div>


                {calendarsworkerStore.optioncalendars.dateoption === 'custom' ?
                    <div className='d-flex flex-row'>
                        <div className='w-50 mx-3'>
                            <DatePicker
                                selected={new Date(calendarsworkerStore.optioncalendars.startdate)}
                                onChange={(date) => {
                                    console.log('date', date.toISOString);
                                    var condi_option = calendarsworkerStore.optioncalendars
                                    condi_option.startdate = date
                                    calendarsworkerStore.Setoptioncalendars(condi_option)
                                }}
                                customInput={<CustomInput />}
                            />
                        </div>

                        <div className='w-50 mx-3'>
                            <DatePicker
                                selected={new Date(calendarsworkerStore.optioncalendars.enddate)}
                                onChange={(date) => {
                                    var condi_option = calendarsworkerStore.optioncalendars
                                    condi_option.enddate = date
                                    calendarsworkerStore.Setoptioncalendars(condi_option)
                                }}
                                minDate={calendarsworkerStore.optioncalendars.startdate}
                                customInput={<CustomInput />}
                            />

                        </div>
                    </div> : null
                }
            </div>
        }
    }

    const CustomInput = forwardRef(({ value, onClick }, ref) => (
        <div className="drop-downwork-blue d-flex flex-row align-items-center justify-content-between mt-3" onClick={onClick} ref={ref}>
            <span>
                {value}
            </span>

            <img src={icon_calendar} alt="" className="icon-mini" />
        </div>
    ));

    const Renderclearfilter = () => {
        if (props.Type === 'detail') {
            return <div className="w-100 d-flex flex-row justify-content-between">
                <span className='prompt_XXL text_blueteal '>
                    ตัวกรอง
                </span>

                {calendarsworkerStore.option.dateoption !== ''
                    || calendarsworkerStore.option.workerlist === ''
                    || calendarsworkerStore.option.worklist === ''
                    || calendarsworkerStore.option.worktype === '' ?
                    <div className="clear-filter">
                        <span className="sarabun_L_300 text_dimgray mr-2">Clear filter</span>
                        <div onClick={() => {
                            calendarsworkerStore.SetworktemplateDisplay('')
                            var option = {
                                "worktype": [],
                                "worklist": [],
                                "workerlist": [],
                                "dateoption": "",
                                "startdate": "",
                                "enddate": ""

                            }
                            calendarsworkerStore.Setoption(option)
                        }} className="cancel_btn">X</div>
                    </div> : null}


            </div>
        } else {
            return <div className="w-100 d-flex flex-row justify-content-between">
                <span className='prompt_XXL text_blueteal '>
                    ตัวกรอง
                </span>

                {calendarsworkerStore.optioncalendars.dateoption !== ''
                    || calendarsworkerStore.optioncalendars.workerlist === ''
                    || calendarsworkerStore.optioncalendars.worklist === ''
                    || calendarsworkerStore.optioncalendars.worktype === '' ?
                    <div className="clear-filter">
                        <span className="sarabun_L_300 text_dimgray mr-2">Clear filter</span>
                        <div onClick={() => {
                            calendarsworkerStore.SetworktemplateDisplayCalendar('')
                            var optioncalendars = {
                                "worktype": [],
                                "worklist": [],
                                "workerlist": [],
                                "dateoption": "",
                                "startdate": "",
                                "enddate": ""

                            }
                            calendarsworkerStore.Setoptioncalendars(optioncalendars)
                        }} className="cancel_btn">X</div>
                    </div> : null}


            </div>
        }
    }

    const renderListjob = () => {
        if (props.Type === 'detail') {
            return <div className='d-flex flex-row list-job'>
                <div
                    onClick={() => {
                        var condi_option = calendarsworkerStore.option
                        var arrWorktype = condi_option.worktype
                        const found = arrWorktype.findIndex(element => element === 0);

                        if (found === -1) {
                            arrWorktype.push(0)
                        } else {
                            arrWorktype.splice(found, 1)
                        }

                        condi_option.worktype = arrWorktype
                        calendarsworkerStore.Setoption(condi_option)
                    }}
                    className={`mr-4 ${calendarsworkerStore.option.worktype.findIndex(element => element === 0) === -1 ? "status-paleturquoise" : "status-blueteal"}`}
                >
                    <span
                        className={`prompt_XL ${calendarsworkerStore.option.worktype.findIndex(element => element === 0) === -1 ? "text_dodgerblue" : "text_white"} `}

                    >งานเพาะเลี้ยง</span>
                </div>

                <div
                    onClick={() => {
                        var condi_option = calendarsworkerStore.option
                        var arrWorktype = condi_option.worktype
                        var found = arrWorktype.findIndex(element => element === 1);
                        if (found === -1) {
                            arrWorktype.push(1)
                        } else {
                            arrWorktype.splice(found, 1)
                        }
                        condi_option.worktype = arrWorktype
                        calendarsworkerStore.Setoption(condi_option)
                    }}
                    className={`mr-4 ${calendarsworkerStore.option.worktype.findIndex(element => element === 1) === -1 ? "status-paleturquoise" : "status-blueteal"}`}
                >
                    <span
                        className={`prompt_XL ${calendarsworkerStore.option.worktype.findIndex(element => element === 1) === -1 ? "text_dodgerblue" : "text_white"} `}
                    >งานประจำวัน</span>
                </div>

                <div
                    onClick={() => {

                    }}
                    className={`status-paleturquoise`}
                >
                    <span
                        className={`prompt_XL  text_dodgerblue`}
                    >งานโครงสร้าง</span>
                </div>

            </div>
        }
        else {
            return <div className='d-flex flex-row list-job'>
                <div
                    onClick={() => {
                        var condi_option = calendarsworkerStore.optioncalendars
                        var arrWorktype = condi_option.worktype
                        const found = arrWorktype.findIndex(element => element === 0);

                        if (found === -1) {
                            arrWorktype.push(0)
                        } else {
                            arrWorktype.splice(found, 1)
                        }

                        condi_option.worktype = arrWorktype
                        calendarsworkerStore.Setoption(condi_option)
                    }}
                    className={`mr-4 ${calendarsworkerStore.optioncalendars.worktype.findIndex(element => element === 0) === -1 ? "status-paleturquoise" : "status-blueteal"}`}
                >
                    <span
                        className={`prompt_XL ${calendarsworkerStore.optioncalendars.worktype.findIndex(element => element === 0) === -1 ? "text_dodgerblue" : "text_white"} `}

                    >งานเพาะเลี้ยง</span>
                </div>

                <div
                    onClick={() => {
                        var condi_option = calendarsworkerStore.optioncalendars
                        var arrWorktype = condi_option.worktype
                        var found = arrWorktype.findIndex(element => element === 1);
                        if (found === -1) {
                            arrWorktype.push(1)
                        } else {
                            arrWorktype.splice(found, 1)
                        }
                        condi_option.worktype = arrWorktype
                        calendarsworkerStore.Setoption(condi_option)
                    }}
                    className={`mr-4 ${calendarsworkerStore.optioncalendars.worktype.findIndex(element => element === 1) === -1 ? "status-paleturquoise" : "status-blueteal"}`}
                >
                    <span
                        className={`prompt_XL ${calendarsworkerStore.optioncalendars.worktype.findIndex(element => element === 1) === -1 ? "text_dodgerblue" : "text_white"} `}
                    >งานประจำวัน</span>
                </div>

                <div
                    onClick={() => {

                    }}
                    className={`status-paleturquoise`}
                >
                    <span
                        className={`prompt_XL  text_dodgerblue`}
                    >งานโครงสร้าง</span>
                </div>

            </div>
        }

    }

    const renderHeadworklist = () => {
        if (props.Type === 'detail') {
            return <div className='d-flex flex-row justify-content-between align-items-center py-3'>
                <span className='sarabun_L text_dimgray'>
                    งาน
                </span>
                {calendarsworkerStore.worktemplateDisplay === ''
                    ? <div onClick={() => {
                        calendarsworkerStore.Setworkselectmodal(true)
                    }} className='d-flex flex-row align-items-center'>
                        <span className='prompt_L text_black mr-2'>ทั้งหมด</span>
                        <img src={icon_downarrow} className='icon-mini' />
                    </div>
                    : <div className="d-flex flex-row ">
                        {calendarsworkerStore.worktemplateDisplay.map((item, index) => {
                            var rendermore = []
                            if (index === 1 && (calendarsworkerStore.worktemplateDisplay.length - 2) !== 0) {
                                rendermore.push(<div className="">
                                    <span> +{calendarsworkerStore.worktemplateDisplay.length - 2}</span>
                                </div>)
                            }
                            if (index <= 1) {
                                return <div className="d-flex flex-row" style={{
                                    width: '55px'
                                }} >
                                    <div className="single-ellipsis">
                                        <span className="prompt_M ">{item.name}</span>
                                    </div>
                                    <span> ,</span>
                                    {rendermore}
                                </div>
                            }
                        })}

                        <div onClick={() => {
                            calendarsworkerStore.SetworktemplateDisplay('')
                        }} className="cancel_btn ml-2">X</div>
                    </div>
                }

            </div>
        } else {
            return <div className='d-flex flex-row justify-content-between align-items-center py-3'>
                <span className='sarabun_L text_dimgray'>
                    งาน
                </span>
                {calendarsworkerStore.worktemplateDisplayCalendar === ''
                    ? <div onClick={() => {
                        calendarsworkerStore.Setworkselectmodal(true)
                    }} className='d-flex flex-row align-items-center'>
                        <span className='prompt_L text_black mr-2'>ทั้งหมด</span>
                        <img src={icon_downarrow} className='icon-mini' />
                    </div>
                    : <div className="d-flex flex-row ">
                        {calendarsworkerStore.worktemplateDisplayCalendar.map((item, index) => {
                            var rendermore = []
                            if (index === 1 && (calendarsworkerStore.worktemplateDisplayCalendar.length - 2) !== 0) {
                                rendermore.push(<div className="">
                                    <span> +{calendarsworkerStore.worktemplateDisplayCalendar.length - 2}</span>
                                </div>)
                            }
                            if (index <= 1) {
                                return <div className="d-flex flex-row" style={{
                                    width: '55px'
                                }} >
                                    <div className="single-ellipsis">
                                        <span className="prompt_M ">{item.name}</span>
                                    </div>
                                    <span> ,</span>
                                    {rendermore}
                                </div>
                            }
                        })}

                        <div onClick={() => {
                            calendarsworkerStore.SetworktemplateDisplayCalendar('')
                        }} className="cancel_btn ml-2">X</div>
                    </div>
                }

            </div>
        }

    }

    const renderBodyWorklist = () => {
        if (props.Type === 'detail') {
            return <div
                className='d-flex flex-column  position-relative pb-5'
                style={{
                    width: '100%',
                    minHeight: '75vh',
                    backgroundColor: '#fff',
                    padding: '21px 18px'
                }}>
                {calendarsworkerStore.worktemplate_list.map((item, index) => {
                    return <div key={index} className='d-flex flex-column'>
                        <div className='d-flex flex-row justify-content-start align-items-end'>
                            <Input
                                type="checkbox"
                                className="checkboxes-xl mr-2"
                                id={index}
                                // checked={item.checked}
                                defaultChecked={false}
                                onChange={(e) => {
                                    calendarsworkerStore.Changeallchecked(item, index)
                                }}
                            />
                        </div>
                        <div className='list-filterwork'>
                            {item[0].worklist.map((item, index) => {
                                return <div key={index}>
                                    <div className='d-flex flex-row justify-content-start align-items-center'>
                                        <Input
                                            type="checkbox"
                                            className="checkboxes-xl mr-2"
                                            checked={item.checked}
                                            defaultChecked={false}
                                            onChange={(e) => {
                                                // console.log(e);
                                                item.checked = !item.checked
                                            }}
                                            id={index}
                                        />
                                        <span className='ml-1 prompt_XL text-black'>{item.name}</span>
                                    </div>

                                    <hr />
                                </div>
                            })}
                        </div>
                    </div>
                })}

                <div className='fixed-bottom'>
                    <div className='d-flex flex-row w-100 px-3 mt-3 mb-4 '>
                        <div onClick={() => {
                            calendarsworkerStore.Setworkselectmodal(false)
                            var arrWorklist = []
                            var arrDisplay = []
                            for (let index = 0; index < calendarsworkerStore.worktemplate_list.length; index++) {
                                const element = calendarsworkerStore.worktemplate_list[index];
                                for (const key in element[0].worklist) {
                                    if (Object.hasOwnProperty.call(element[0].worklist, key)) {
                                        const worklist = element[0].worklist[key];
                                        if (worklist.checked === true) {
                                            arrWorklist.push(Number(worklist.id))
                                            arrDisplay.push(worklist)
                                        }

                                    }
                                }
                            }
                            var condi_option = calendarsworkerStore.option
                            condi_option.worklist = arrWorklist
                            calendarsworkerStore.Setoption(condi_option)
                            calendarsworkerStore.SetworktemplateDisplay(arrDisplay)

                        }} className='btn-confirm-100 d-flex justify-content-center align-items-center mr-3'>
                            <span className='prompt_XL text_white'>
                                ยืนยัน
                            </span>
                        </div>
                        <div onClick={() => { calendarsworkerStore.Setworkselectmodal(false) }} className='btn-cancel-100 d-flex justify-content-center align-items-center ml-3'>
                            <span className='prompt_XL text_dodgerblue'>
                                ยกเลิก
                            </span>
                        </div>

                    </div>
                </div>
            </div>
        }
        else {
            return <div
                className='d-flex flex-column  position-relative pb-5'
                style={{
                    width: '100%',
                    minHeight: '75vh',
                    backgroundColor: '#fff',
                    padding: '21px 18px'
                }}>
                {calendarsworkerStore.worktemplate_listCalendar.map((item, index) => {
                    return <div key={index} className='d-flex flex-column'>
                        <div className='d-flex flex-row justify-content-start align-items-end'>
                            <Input
                                type="checkbox"
                                className="checkboxes-xl mr-2"
                                id={index}
                                // checked={item.checked}
                                defaultChecked={false}
                                onChange={(e) => {
                                    calendarsworkerStore.ChangeallcheckedCalendar(item, index)
                                }}
                            />
                        </div>
                        <div className='list-filterwork'>
                            {item[0].worklist.map((item, index) => {
                                return <div key={index}>
                                    <div className='d-flex flex-row justify-content-start align-items-center'>
                                        <Input
                                            type="checkbox"
                                            className="checkboxes-xl mr-2"
                                            checked={item.checked}
                                            defaultChecked={false}
                                            onChange={(e) => {
                                                // console.log(e);
                                                item.checked = !item.checked
                                            }}
                                            id={index}
                                        />
                                        <span className='ml-1 prompt_XL text-black'>{item.name}</span>
                                    </div>

                                    <hr />
                                </div>
                            })}
                        </div>
                    </div>
                })}

                <div className='fixed-bottom'>
                    <div className='d-flex flex-row w-100 px-3 mt-3 mb-4 '>
                        <div onClick={() => {
                            calendarsworkerStore.Setworkselectmodal(false)
                            var arrWorklist = []
                            var arrDisplay = []
                            for (let index = 0; index < calendarsworkerStore.worktemplate_listCalendar.length; index++) {
                                const element = calendarsworkerStore.worktemplate_listCalendar[index];
                                for (const key in element[0].worklist) {
                                    if (Object.hasOwnProperty.call(element[0].worklist, key)) {
                                        const worklist = element[0].worklist[key];
                                        if (worklist.checked === true) {
                                            arrWorklist.push(Number(worklist.id))
                                            arrDisplay.push(worklist)
                                        }

                                    }
                                }
                            }
                            var condi_option = calendarsworkerStore.optioncalendars
                            condi_option.worklist = arrWorklist
                            calendarsworkerStore.Setoptioncalendars(condi_option)
                            calendarsworkerStore.SetworktemplateDisplayCalendar(arrDisplay)

                        }} className='btn-confirm-100 d-flex justify-content-center align-items-center mr-3'>
                            <span className='prompt_XL text_white'>
                                ยืนยัน
                            </span>
                        </div>
                        <div onClick={() => { calendarsworkerStore.Setworkselectmodal(false) }} className='btn-cancel-100 d-flex justify-content-center align-items-center ml-3'>
                            <span className='prompt_XL text_dodgerblue'>
                                ยกเลิก
                            </span>
                        </div>

                    </div>
                </div>
            </div>
        }

    }

    const renderHeadWorkdate = () => {
        if (props.Type === 'detail') {
            return <div className='d-flex flex-row justify-content-between align-items-center py-4'>

                <span className='sarabun_L text_dimgray'>
                    ช่วงวันที่
                </span>

                {calendarsworkerStore.option.dateoption !== '' ? <div className='d-flex flex-row align-items-center'>
                    {calendarsworkerStore.option.dateoption === 'custom'
                        ? <span className='prompt_L text_black mr-2'>{moment(calendarsworkerStore.option.startdate).format("DD/MM/YYYY")} - {moment(calendarsworkerStore.option.enddate).format("DD/MM/YYYY")}</span>
                        : <span className='prompt_L text_black mr-2'>{calendarsworkerStore.option.dateoption}</span>}
                    <div onClick={() => {
                        var condi_option = calendarsworkerStore.option
                        condi_option.dateoption = ''
                        calendarsworkerStore.Setoption(condi_option)
                    }} className="cancel_btn">X</div>
                </div> : <div className='d-flex flex-row align-items-center'>
                    <span className='prompt_L text_black mr-2'>ทั้งหมด</span>

                    <img src={icon_downarrow} className='icon-mini' />
                </div>}

            </div>
        }
        else {
            return <div className='d-flex flex-row justify-content-between align-items-center py-4'>

                <span className='sarabun_L text_dimgray'>
                    ช่วงวันที่
                </span>

                {calendarsworkerStore.optioncalendars.dateoption !== '' ? <div className='d-flex flex-row align-items-center'>
                    {calendarsworkerStore.optioncalendars.dateoption === 'custom'
                        ? <span className='prompt_L text_black mr-2'>{moment(calendarsworkerStore.optioncalendars.startdate).format("DD/MM/YYYY")} - {moment(calendarsworkerStore.optioncalendars.enddate).format("DD/MM/YYYY")}</span>
                        : <span className='prompt_L text_black mr-2'>{calendarsworkerStore.optioncalendars.dateoption}</span>}
                    <div onClick={() => {
                        var condi_option = calendarsworkerStore.optioncalendars
                        condi_option.dateoption = ''
                        calendarsworkerStore.Setoptioncalendars(condi_option)
                    }} className="cancel_btn">X</div>
                </div> : <div className='d-flex flex-row align-items-center'>
                    <span className='prompt_L text_black mr-2'>ทั้งหมด</span>

                    <img src={icon_downarrow} className='icon-mini' />
                </div>}

            </div>
        }

    }

    const renderHeadWorkerlist = () => {
        if (props.Type === 'detail') {
            return <div> {calendarsworkerStore.workerselectdata.length !== 0 ? <div className='d-flex flex-row align-items-center'>

                <span className='prompt_L text_black mr-2'>{calendarsworkerStore.workerselectdata.length} คน</span>
                <div onClick={() => {
                    calendarsworkerStore.removeallworkerSelectdata()
                }} className="cancel_btn">X</div>
            </div>
                : <div onClick={() => {
                    calendarsworkerStore.Setworkerselectmodal(true)
                }} className='d-flex flex-row align-items-center'>

                    <span className='prompt_L text_black mr-2'>ทั้งหมด</span>
                    <img src={icon_downarrow} className='icon-mini' />
                </div>}
            </div>
        }
        else {
            return <div> {calendarsworkerStore.workerselectdatacalendar.length !== 0 ? <div className='d-flex flex-row align-items-center'>

                <span className='prompt_L text_black mr-2'>{calendarsworkerStore.workerselectdatacalendar.length} คน</span>
                <div onClick={() => {
                    calendarsworkerStore.removeallworkerSelectdatacalendar()
                }} className="cancel_btn">X</div>
            </div>
                : <div onClick={() => {
                    calendarsworkerStore.Setworkerselectmodal(true)
                }} className='d-flex flex-row align-items-center'>

                    <span className='prompt_L text_black mr-2'>ทั้งหมด</span>
                    <img src={icon_downarrow} className='icon-mini' />
                </div>}
            </div>
        }

    }




    return (

        <div className="bg-white p-0" >



            <Appmodal
                visible={uistore.Filterworker}
                nocontainer={false}
                className={'d-flex justify-content-center align-items-center'}
            >
                <div
                    className='d-flex flex-column  py-4 position-relative'
                    style={{
                        width: '100%',
                        minHeight: '100vh',
                        backgroundColor: '#fff',
                    }}>
                    <div
                        onClick={() => {
                            uistore.SetFilterworker(false)
                        }}
                        className="d-flex flex-row justify-content-start align-items-center px-3"
                        style={{}}>
                        <img src={icon_leftarrow} alt='' className='icon-mini' />
                        <span className='sarabun_L_100 text_dimgray'>
                            BACK
                        </span>
                    </div>


                    <div>

                        <div className='px-4 mt-3' onClick={() => setExpanded_filterdate((x) => !x)}>

                            {Renderclearfilter()}

                            {renderListjob()}

                            {renderHeadworklist()}

                        </div>


                        <Collapse
                            isActive={isExpanded_filterdate}
                            render_children={render_work()}
                        />
                        <div className="position-relative mt-1 ">
                            <div className=" line-solid not-absolute" />
                        </div>
                    </div>


                    <div>
                        <div className='px-4'
                            onClick={() => setExpanded_filterharvestdate((x) => !x)}
                        >
                            {renderHeadWorkdate()}
                        </div>
                        <Collapse
                            isActive={isExpanded_filterharvestdate}
                            render_children={renderBodyWorkdate()}
                        />
                        <div className="position-relative mt-1 ">
                            <div className=" line-solid not-absolute" />
                        </div>
                    </div>

                    <div className='mb-5'>
                        <div className='px-4'
                            onClick={() => setExpanded_filterspecies((x) => !x)}
                        >
                            <div className='d-flex flex-row justify-content-between align-items-center py-4'>

                                <span className='sarabun_L text_dimgray'>
                                    คนงาน
                                </span>
                                {renderHeadWorkerlist()}
                            </div>
                        </div>
                        <Collapse
                            isActive={isExpanded_filterspecies}
                            render_children={renderBodyworkerlist()}
                        />
                        <div className="position-relative mt-1 ">
                            <div className=" line-solid not-absolute" />
                        </div>
                    </div>


                    <div className='bottom-filter'>
                        <div onClick={() => {
                            if (props.Type === 'calendar') {
                                uistore.SetEnableFooter(true)
                                calendarsworkerStore.Getinitial_events()
                            } else {
                                calendarsworkerStore.Getlist_workdata()
                            }

                            uistore.SetFilterworker(false)

                        }} className='btn-confirm-100 d-flex justify-content-center align-items-center'>
                            <span className="prompt_XL text_white">
                                กรองเลย
                            </span>
                        </div>
                    </div>


                </div>
            </Appmodal>



            <Appmodal
                visible={calendarsworkerStore.workselectmodal}
                nocontainer={true}
                className={'d-flex justify-content-center align-items-center'}
            >
                {renderBodyWorklist()}
            </Appmodal>



            <Workersearchmodal
                Type={props.Type}
                submitWorker={submitWorker}
                selectedWorker={selectWorker}
            />

            {/* <Appmodal
                visible={calendarsworkerStore.workerselectmodal}
                nocontainer={false}
                className={'d-flex justify-content-center align-items-center'}
            >
                {renderModalWorkerlist()}
            </Appmodal> */}
        </div>
    );
})


// const HomeDash = () => {

// };

export default Filterworkcalendar;
