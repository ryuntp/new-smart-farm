
import React, { useContext, useEffect, useState } from "react";
import FullCalendar, { formatDate } from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import timeGridPlugin from '@fullcalendar/timegrid'
import interactionPlugin from '@fullcalendar/interaction'
import icon_filter from '../../Assets/img/icon-general-filter.png'
import { INITIAL_EVENTS, createEventId } from '../../utils/event-utils'

import Filterworkcalendar from "./Filterworkcalendar";
import CalendarsworkerStore from '../../Stores/CalendarsworkerStore'
import UIstore from "../../Stores/UIstore";
import Switch from "react-switch";
import moment from 'moment'
import 'moment/locale/th';
import icon_leftarrow from '../../Assets/img/dropdown-left.png'
import { useHistory } from "react-router-dom";
import axiosInstance from "../../Lib/AxiosInterceptor";
import { observer } from "mobx-react";
// const stores = useContext(StoreContext);



export const Calendarworker = observer((props) => {
  const uistore = useContext(UIstore);
  const history = useHistory();
  const calendarsworkerStore = useContext(CalendarsworkerStore);

  const [weekendsVisible, setweekendsVisible] = useState(true);
  const [currentEvents, setcurrentEvents] = useState([]);
  const [hidework, sethidework] = useState(false);

  const handleChangeAll = (checked) => {
    sethidework(checked)
  }

  const handleDateSelect = (selectInfo) => {

    // let title = prompt('Please enter a new title for your event')
    // let calendarApi = selectInfo.view.calendar

    // calendarApi.unselect() // clear date selection

    // if (title) {
    //   calendarApi.addEvent({
    //     id: createEventId(),
    //     title,
    //     start: selectInfo.startStr,
    //     end: selectInfo.endStr,
    //     allDay: selectInfo.allDay
    //   })
    // }
  }

  const ClickDate = (clickInfo) => {
    // console.log('clickInfo', clickInfo);
    history.push({ pathname: `/admin/Calendardetail`, search: `dateselect=${clickInfo.dateStr}` });
    history.go('d')
  }

  const handleEventClick = (clickInfo) => {

  }

  const handleEvents = (events) => {
    setcurrentEvents(events)
  }


  const renderEventContent = (eventInfo) => {
    return (
      <div style={{
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        color: '#000'
      }}>
        <b>{eventInfo.timeText}</b>
        <span style={{
          color: '#000',
          whiteSpace: 'nowrap'
        }}
        >{eventInfo.event.title}</span>
      </div>
    )
  }

 
  const handleMonthChange = (payload) => {
    var condi_option = calendarsworkerStore.optioncalendars
    var formatdate = moment(payload.startStr).format("YYYY-MM-DD")
    condi_option.startdate = formatdate
    condi_option.enddate = moment(new Date(new Date(formatdate).setDate(new Date(formatdate).getDate() + 32))).format("YYYY-MM-DD")
    console.log(condi_option);
    calendarsworkerStore.Setoptioncalendars(condi_option)
    calendarsworkerStore.Getinitial_events()

  }


  useEffect(() => {
    uistore.SetEnableHeader(true)
    uistore.SetEnablesubHeader(false)
    uistore.SetEnableFooter(true)
    // alert('test')
    calendarsworkerStore.Getinitial_events()
  }, [calendarsworkerStore, uistore]);


  return (

    <div className="bg-white p-0" >



      <Filterworkcalendar
        Type={'calendar'}
      />
      <div className='' style={{
        flexGrow: 1,
        marginTop: 90
      }}>

        <div className='w-100 d-flex flex-row px-3 mb-3 filter-bar align-items-center' >

          <div onClick={() => {
            uistore.SetFilterworker(true)
            uistore.SetEnableFooter(false)
          }}>
            <img src={icon_filter} className='icon-mini' />
          </div>


          <div className='line-dash-vertical ml-3' />

          <div className='w-100 d-flex flex-row align-items-center ml-2'>
            <span className='prompt_L_300 text_dimgray ml-2' >ซ่อนงานประจำวัน </span>
          </div>

          <Switch uncheckedIcon={false} checkedIcon={false} onChange={handleChangeAll} onColor={'#1e76fe'} offColor="#e2e2e2" checked={hidework} />

        </div>
        <FullCalendar
          plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
          height={"auto"}
          datesSet={handleMonthChange}
          // initialDate={"2020-02-02"}
          headerToolbar={{
            left: 'title',
            center: '',
            right: 'prev,today,next'
          }}
          initialView='dayGridMonth'
          editable={true}
          selectable={true}
          selectMirror={true}
          dayMaxEvents={2}
          dateClick={ClickDate}
          fixedWeekCount={false}
          dayHeaderFormat={(args) => {
            return moment(args.date).format('dd')
          }}
          displayEventTime={false}
          weekends={weekendsVisible}
          events={calendarsworkerStore.eventData}
          // initialEvents={INITIAL_EVENTS}
          eventClick={handleEventClick}// alternatively, use the `events` setting to fetch from a feed
          select={handleDateSelect}
          eventContent={renderEventContent} // custom render function
          eventDisplay={'list-item'}    
          moreLinkClassNames={'sarabun_M_300 text_gray'}
          moreLinkContent={(args) => {
            return `+ ${args.num} `;
          }}


        />
      </div >

    </div >
  );
})


// const HomeDash = () => {

// };

export default Calendarworker;
