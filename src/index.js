import React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory } from "history";
import 'bootstrap/dist/css/bootstrap.css';
import './index.css';
import Auth from './Layout/Auth';
import Admin from "./Layout/Admin";
import reportWebVitals from './reportWebVitals';
import AdminNavBar from "./Components/AdminNavbar/AdminNavbar"
import AdminHeader from "./Components/AdminNabberHeader/AdminHeader"
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { StoreProvider } from "./Stores/Rootstore";
import "./Assets/scss/App.scss";
import Loader from './Components/Loader';
import Footer from './Components/Footer/Footer';
const hist = createBrowserHistory();


ReactDOM.render(
  <React.Fragment>
    {/* <StoreProvider> */}

    <Router history={hist}>

      <Loader />
      <AdminNavBar
        pageName={"Dashboard"}
      />
      <AdminHeader />

      <Switch>
        <Route path="/admin" render={props => <Admin {...props} />} />
        <Route path="/auth" render={props => <Auth {...props} />} />
        <Redirect to="/auth/login" />
        {/* <App /> */}
      </Switch>

     
      <Footer />
    </Router>
    {/* </StoreProvider> */}
  </React.Fragment>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
